-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 17, 2021 at 06:34 PM
-- Server version: 5.7.35-log
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demof4hc_sultana`
--

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `id` int(11) NOT NULL,
  `name` text,
  `name_en` text,
  `branch_numb` int(11) DEFAULT NULL,
  `phone` text,
  `city` varchar(100) DEFAULT NULL,
  `address` text,
  `tax_number` varchar(250) DEFAULT NULL,
  `comment` text,
  `user_id` int(11) DEFAULT NULL,
  `created_date` text,
  `is_active` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `name`, `name_en`, `branch_numb`, `phone`, `city`, `address`, `tax_number`, `comment`, `user_id`, `created_date`, `is_active`) VALUES
(3, 'الفرع الرئيسي', 'Main Branch', 3, '1234567', 'شرق الرياض', 'riyadh', '1234', '0', 9, NULL, 1),
(6, 'فرع مكة المكرمة', 'Makkah', 2, '123456', 'مكة المكرمة', 'مكة المكرمة', NULL, '0', 11, '2020-09-08 08:50:22', 1),
(7, 'نجران', 'Nagran', 1, '565656', 'نجران', 'نجران', NULL, '0', 12, '2020-09-08 08:52:45', 1),
(8, 'مخزون عرض رمضان', '', 10, '0135868863', 'الأحساء', 'الفيصل', 'لايوجد', '0', 14, '2021-02-21 14:00:45', 1),
(9, 'رامز', 'RAMEZ', 1, '0540052287', 'الأحساء', 'الأحساء', '', '0', 15, '2021-03-01 14:06:01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `device_id` text NOT NULL,
  `mount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `pro_id`, `device_id`, `mount`) VALUES
(13, 5, '231321234', 4),
(16, 4, '231321234', 1),
(18, 8, '231321234', 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `title`) VALUES
(19, 'الأحساء'),
(20, 'رفحاء'),
(21, 'حفر الباطن'),
(22, 'حائل'),
(23, 'القريات'),
(24, 'عرعر'),
(25, 'الجوف (سكاكا)'),
(26, 'تبوك'),
(27, 'شرق الرياض'),
(28, 'غرب الرياض'),
(29, 'وسط الرياض'),
(30, 'شمال الرياض'),
(31, 'جنوب الرياض'),
(32, 'محافظة الخرج والدلم'),
(33, 'محافظات منطقة القصيم'),
(34, 'محافظات منطقة الرياض'),
(35, 'رابغ'),
(36, 'ينبع'),
(37, 'مكة المكرمة'),
(38, 'جدة'),
(39, 'القنفدة'),
(40, 'الطائف'),
(41, 'المدينة المنورة'),
(42, 'بقيق'),
(43, 'النعيرية'),
(44, 'الخفجي'),
(45, 'الجبيل'),
(46, 'رأس تنورة'),
(47, 'الخبر'),
(48, 'القطيف'),
(49, 'حفر الباطن'),
(50, 'سيهات'),
(51, 'الدمام'),
(52, 'الظهران'),
(53, 'طريف'),
(54, 'صفوى'),
(55, 'محايل عسير'),
(56, 'خميس مشيط'),
(57, 'الباحة'),
(58, 'شرورة'),
(59, 'جازان'),
(60, 'أبها'),
(61, 'نجران'),
(62, 'بيشة');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `phone` text NOT NULL,
  `national_id` text,
  `client_barcode` text,
  `start_special_date` datetime DEFAULT NULL,
  `discount_percentage` float NOT NULL,
  `sum_sales_cost` float NOT NULL DEFAULT '0',
  `points_start_fristyear` float NOT NULL DEFAULT '0',
  `points` decimal(10,0) DEFAULT '0',
  `points_to_test` float NOT NULL DEFAULT '0',
  `special_year_totest` float NOT NULL DEFAULT '0',
  `notification_check` int(11) NOT NULL DEFAULT '1',
  `address` text,
  `email` varchar(350) DEFAULT NULL,
  `is_special` int(11) DEFAULT '0',
  `birth_date` text,
  `city` text,
  `state` text,
  `profile` text,
  `ref_code` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `user_group` int(11) NOT NULL,
  `created_date` text NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `vip_card_status` int(11) DEFAULT '0',
  `vip_card_active` int(11) NOT NULL DEFAULT '1',
  `branch_give_client` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `name`, `phone`, `national_id`, `client_barcode`, `start_special_date`, `discount_percentage`, `sum_sales_cost`, `points_start_fristyear`, `points`, `points_to_test`, `special_year_totest`, `notification_check`, `address`, `email`, `is_special`, `birth_date`, `city`, `state`, `profile`, `ref_code`, `type`, `user_group`, `created_date`, `is_active`, `vip_card_status`, `vip_card_active`, `branch_give_client`) VALUES
(7, 'فانتاستك', '966539200166', '98754', '382777471617', '2020-09-08 09:52:35', 50, 0, 0, 101, 0, 1, 0, 'الرياض', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-09-08 09:35:24', 1, 22, 1, 6),
(8, 'تقنية', '345345345', '', '613199177637', '2021-01-31 21:55:14', 50, 0, 0, 42, 0, 1, 0, '', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-09-08 09:35:47', 1, 1, 1, 0),
(9, 'المعلومات', '236790', '', '459513296586', '2021-01-10 22:19:05', 50, 0, 0, 92, 0, 1, 1, '', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-09-08 09:36:11', 1, 3, 1, 6),
(10, 'الأتصالات', '34545545', NULL, '206639168876', '2021-01-10 22:34:30', 50, 0, 0, 52, 0, 0, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-09-08 10:46:38', 0, 3, 1, 6),
(11, 'احمد', '0541774999', '', '378492805595', '2021-01-17 14:12:02', 50, 0, 0, 120, 0, 1, 1, '', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-11-01 15:24:07', 1, 3, 1, NULL),
(12, 'رضا', '0555555555', '', '6287008590090', '2020-11-15 13:26:14', 50, 0, 0, 160, 0, 1, 0, '', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-11-01 15:27:14', 1, 1, 1, 0),
(13, 'حسن', '0539200166', '101112131415', '299597676886', '2021-01-10 22:26:34', 50, 0, 0, 384, 0, 1, 1, '', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-11-02 06:30:34', 1, 2, 1, NULL),
(14, 'علي', '0510203040', NULL, '160597673123', '2021-01-10 22:17:36', 50, 0, 0, 333, 0, 1, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-11-02 09:12:43', 0, 2, 1, NULL),
(15, 'حسن السلطان', '966508958421', '', '492585583458', '2021-01-10 22:31:36', 50, 0, 0, 125, 0, 1, 0, '', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-11-15 13:27:00', 1, 3, 1, NULL),
(16, 'بدر', '0508948421', '1001001000', '1001001000', '2021-03-28 20:20:43', 50, 0, 0, 127, 0, 1, 1, 'الهفوف', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-02 12:31:08', 1, 2, 1, 7),
(17, 'حبيب السلطان', '0531888198', NULL, '100', NULL, 0, 0, 0, 40, 0, 0, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-21 13:51:49', 0, 1, 1, 0),
(18, 'علي', '0512345678', '', '123456789', '2021-05-01 08:49:58', 50, 0, 0, 40, 48, 51, 1, '', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-03-03 11:36:34', 1, 3, 1, 7),
(19, 'أحمد الحاجي', '0501234587', '', '571365316814', '2021-03-03 12:10:37', 50, 0, 0, 54, 0, 1, 1, '', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-03-03 11:45:23', 0, 22, 1, 6),
(20, 'عميل جديد', '1234567812345678', '1231231234', '342310689602', NULL, 0, 0, 0, 40, 0, 0, 1, '', '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-03-28 21:08:08', 1, 2, 1, 6),
(27, 'عميل جديد2', '12345612122', '22336655', '1000002206', '2021-04-06 11:14:58', 50, 0, 0, 68, 0, 1, 1, '', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-03-31 06:22:32', 1, 22, 1, 6),
(28, 'احمد الحاج', '966540052287', '', '1000000999', '2021-04-28 04:03:25', 50, 0, 0, 56, 0, 1, 1, '', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-04-05 19:21:00', 1, 1, 1, 0),
(29, 'عميل جديد', '966553919623', '966548270331', '161436334998', '2018-03-21 18:37:27', 50, 0, 0, 60, 4, 3, 1, '', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-04-21 18:20:05', 1, 1, 1, 0),
(30, 'test', '966541774999', '4321', '752008830959', '2020-04-23 16:38:29', 10, 0, 0, 42, 56, 2, 1, '', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-04-24 13:57:56', 1, 3, 1, 9),
(31, 'بندر السلمان', '96655155555500000', '', '1000002265', NULL, 0, 0, 0, 40, 0, 0, 1, '', '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-05-06 19:31:29', 1, 3, 1, 9),
(34, 'حسن السلطان ', '966595012300', '', '1000002030', NULL, 0, 0, 0, 10, 0, 0, 1, '', '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-06-26 14:42:32', 1, 3, 1, 9),
(35, 'محمد', '0121212121', '', '7622100765189', NULL, 0, 0, 0, 40, 0, 0, 1, '', '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-07-04 07:13:02', 1, 0, 1, NULL),
(37, 'عميل سلطانة', '966546780336', '112233', '943554992622', '2021-07-27 19:57:25', 50, 0, 172, 172, 0, 0, 1, '', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-07-27 18:44:54', 1, 0, 1, NULL),
(38, 'test101', '0539200166', '0123456789', '912293893240', NULL, 0, 0, 0, 0, 0, 0, 1, '', '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-08-10 06:43:43', 1, 0, 1, NULL),
(39, 'test101', '0539200166', '0123456789', '912293893240', NULL, 0, 0, 0, 0, 0, 0, 1, '', '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-08-10 06:43:44', 1, 0, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--

CREATE TABLE `complaints` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `phone` varchar(150) NOT NULL,
  `subject` text NOT NULL,
  `comment` text NOT NULL,
  `status` int(11) NOT NULL,
  `date` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `complaints`
--

INSERT INTO `complaints` (`id`, `name`, `phone`, `subject`, `comment`, `status`, `date`) VALUES
(5, 'tarek mandour', '010062873', 'تجربه', ' نص  نص نص نصنص', 0, '2020-05-18 01:25:55');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `phone` text NOT NULL,
  `address` text NOT NULL,
  `email` varchar(350) NOT NULL,
  `user_hash` varchar(200) NOT NULL,
  `gendar` text NOT NULL,
  `birth_date` text NOT NULL,
  `city` text NOT NULL,
  `state` text NOT NULL,
  `forgot_password` text,
  `ref_code` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `user_group` int(11) NOT NULL,
  `created_date` text NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fav`
--

CREATE TABLE `fav` (
  `id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fav`
--

INSERT INTO `fav` (`id`, `pro_id`, `user_id`) VALUES
(1, 4, 23),
(12, 9, 23);

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `address` text NOT NULL,
  `phone1` text NOT NULL,
  `phone2` text NOT NULL,
  `email` text NOT NULL,
  `tax_num` int(11) NOT NULL,
  `type` text NOT NULL,
  `currency` text NOT NULL,
  `start_invoice` int(11) NOT NULL,
  `photo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`id`, `title`, `address`, `phone1`, `phone2`, `email`, `tax_num`, `type`, `currency`, `start_invoice`, `photo`) VALUES
(2, 'ميدو ماركت', 'omr bn khatab', '01006287376', '1774999', 'tarek.mandourr@gmail.com', 36257, 'بصريات', 'ريال سعودي', 0, 'http://localhost/urampro/images/c2.jpg'),
(3, 'ميدو ماركت', 'omr bn khatab', '01006287376', '1774999', 'pilot@gmail.com', 177557575, 'صيدليات', 'ريال سعودي', 0, 'http://localhost/urampro/images/c5.jpg,http://localhost/urampro/images/c6.jpg,http://localhost/urampro/images/c6.png'),
(4, 'الرياض', 'omr bn khatab', '01006287376', '369369', 'pilot@gmail.com', 335553, 'مبيعات عام', 'ريال سعودي', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `masrofat_receipt`
--

CREATE TABLE `masrofat_receipt` (
  `id` int(11) NOT NULL,
  `byuser` float NOT NULL,
  `branch_id` int(11) NOT NULL,
  `sum_productsquantity` float NOT NULL,
  `tax` float DEFAULT NULL,
  `sum_productspricetax` float DEFAULT NULL,
  `sum_productspricewithouttax` float DEFAULT NULL,
  `receipt_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `receipt_state` int(11) DEFAULT NULL,
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `masrofat_receipt`
--

INSERT INTO `masrofat_receipt` (`id`, `byuser`, `branch_id`, `sum_productsquantity`, `tax`, `sum_productspricetax`, `sum_productspricewithouttax`, `receipt_date`, `receipt_state`, `comment`) VALUES
(1, 11, 3, 2, 0.9, 6.9, 6, '2021-01-30 18:48:33', 0, '0'),
(2, 11, 3, 7, 6.3, 48.3, 42, '2021-01-30 18:48:46', 0, '0'),
(3, 11, 6, 7, 6.3, 48.3, 42, '2021-01-30 18:48:50', 0, '0'),
(4, 11, 6, 7, 6.3, 48.3, 42, '2021-01-30 18:48:54', 0, '0'),
(5, 9, 3, 6, 4.5, 34.5, 30, '2021-01-30 19:24:24', 0, '0'),
(6, 9, 3, 0, 0, 0, 0, '2021-01-31 16:08:16', 0, '0'),
(7, 9, 3, 7, 0, 0, 0, '2021-01-31 16:09:44', 0, '0'),
(8, 9, 3, 1, 34.5, 264.5, 230, '2021-02-02 12:51:14', 0, '0'),
(9, 9, 3, 1, 30, 230, 200, '2021-02-02 12:51:37', 0, '0'),
(10, 9, 3, 0, 0, 0, 0, '2021-02-21 14:08:34', 0, '0'),
(11, 9, 3, 1, 75, 575, 500, '2021-03-03 11:32:36', 1, '0');

-- --------------------------------------------------------

--
-- Table structure for table `masrofat_receiptdetails`
--

CREATE TABLE `masrofat_receiptdetails` (
  `id` int(11) NOT NULL,
  `receipt_id` int(11) NOT NULL,
  `product_name` text,
  `product_price` float NOT NULL,
  `product_quantity` float NOT NULL,
  `tax` float NOT NULL,
  `tax_description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `masrofat_receiptdetails`
--

INSERT INTO `masrofat_receiptdetails` (`id`, `receipt_id`, `product_name`, `product_price`, `product_quantity`, `tax`, `tax_description`) VALUES
(1, 1, 'سقبس', 3, 2, 0.9, NULL),
(2, 2, '12صقثص', 6, 7, 6.3, NULL),
(3, 3, '12صقثص', 6, 7, 6.3, NULL),
(4, 4, '12صقثص', 6, 7, 6.3, NULL),
(5, 5, '33333', 5, 6, 4.5, NULL),
(6, 6, 'xzgvxc', 0, 0, 0, NULL),
(7, 7, 'fvgdfgdf', 0, 7, 0, NULL),
(8, 8, 'كهرباء', 200, 1, 34.5, NULL),
(9, 9, 'كهرباء', 200, 1, 30, NULL),
(10, 10, 'الكهرباء', 400, 0, 0, NULL),
(11, 11, 'كهرباء', 500, 1, 75, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `name` varchar(350) NOT NULL,
  `phone` varchar(125) NOT NULL,
  `country` text NOT NULL,
  `city` text NOT NULL,
  `pre_num` text NOT NULL,
  `national_id` int(11) NOT NULL,
  `email` text NOT NULL,
  `user_hash` text NOT NULL,
  `comp_name` text NOT NULL,
  `comp_type` text NOT NULL,
  `comp_tele` varchar(100) NOT NULL,
  `comp_phone` varchar(100) NOT NULL,
  `comp_city` varchar(150) NOT NULL,
  `comp_state` varchar(150) NOT NULL,
  `address` text NOT NULL,
  `store_ar` text NOT NULL,
  `store_en` text NOT NULL,
  `car_num` text NOT NULL,
  `photo1` text NOT NULL,
  `photo2` text NOT NULL,
  `photo3` text NOT NULL,
  `low_delivery_cost` int(11) NOT NULL,
  `store_num` int(11) NOT NULL,
  `store_bransh` int(11) NOT NULL,
  `confirm` int(11) NOT NULL,
  `ref_code` int(11) NOT NULL,
  `opcl` int(11) NOT NULL,
  `mem_status` int(11) NOT NULL,
  `created_date` text NOT NULL COMMENT 'timestamp',
  `is_active` varchar(3) NOT NULL COMMENT 'yes or no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `name`, `phone`, `country`, `city`, `pre_num`, `national_id`, `email`, `user_hash`, `comp_name`, `comp_type`, `comp_tele`, `comp_phone`, `comp_city`, `comp_state`, `address`, `store_ar`, `store_en`, `car_num`, `photo1`, `photo2`, `photo3`, `low_delivery_cost`, `store_num`, `store_bransh`, `confirm`, `ref_code`, `opcl`, `mem_status`, `created_date`, `is_active`) VALUES
(434, '', '0541774999', '', 'الاحساء', '', 0, 'redha@f4h.com.sa', '', '', '', '', '', '', '', 'الهفةف', 'فانتاستك للذهب والمجوهرات', 'Fantastic for Gold', '', '', '', '', 0, 203487942, 2, 0, 0, 0, 2, '2020-05-06 13:42:32', '0'),
(435, '', '0544068323', '', 'الاحساء', '', 0, 'busjad_jewellery@hotmail.com', '', '', '', '', '', '', '', 'الاحساء سوق المبرز', 'مجوهرات بوسجاد', 'busjadjewellery', '', 'http://igoldapp.com/images/8615355922314506d591cc715f37014e.jpeg', 'http://igoldapp.com/images/8935545f6a97b9baf4c49754b2e9537d.jpeg', '', 0, 2147483647, 1, 0, 0, 0, 2, '2020-05-06 15:09:08', '0'),
(438, '', '0593905769', '', 'الاحساء', '', 0, 'alhotali@gmail.com', '', '', '', '', '', '', '', 'المبرز', 'مجوهرات حسين', 'Hussain - jewellery', '', '', 'http://igoldapp.com/images/b42ff51a1ccaed570003b5bacb95d941.jpeg', 'http://igoldapp.com/images/571943d934e3e1c344e8be1dd4c82b11.jpeg', 0, 2147483647, 1, 0, 0, 0, 2, '2020-05-06 15:52:25', '0'),
(440, '', '0545738040', '', 'الاحساء', '', 0, 'mjb_mj@hotmail.com', '', '', '', '', '', '', '', 'الاحساء الهفوف', 'جواهر الباذر', 'Jwaher Albather', '', 'http://igoldapp.com/images/58340061b7d670c4f937d1c3db4ef1bf.jpeg', 'http://igoldapp.com/images/d4dca59720b5370646d2a582b7360dbc.png', 'http://igoldapp.com/images/dfdd6d84c2b83308ab29d0a43dfbc3b8.png', 0, 2147483647, 1, 0, 0, 0, 2, '2020-05-07 17:10:32', '0'),
(441, '', '0566995990', '', 'الدمام ', '', 0, 'f16520@gmail.com', '', '', '', '', '', '', '', 'مجمع الشاطئ', 'احمد النمر للذهب ولمجوهرات وساعات الثمينه ', 'Ahmed a number', '', 'http://igoldapp.com/images/06632547aa2e039d659f0aad7e31ddaf.jpeg', 'http://igoldapp.com/images/79862bce04bbdfc026a3aef66b19c741.jpeg', 'http://igoldapp.com/images/d9d6d315d0d388c844329c7d96765541.jpeg', 0, 2050036983, 7, 0, 0, 0, 2, '2020-05-07 17:20:57', '0'),
(442, '', '0506902429', '', 'الاحساء', '', 0, 'asir2006123@gmail.com', '', '', '', '', '', '', '', 'المبرز', 'مصاغات الرضا', 'Alreda jewellary', '', 'http://igoldapp.com/images/03aad3d4f71154d5a5dd17619f236457.png', 'http://igoldapp.com/images/213f2d2dde0cf8091fd2b6bbd57c17f7.jpeg', '', 0, 2147483647, 1, 0, 0, 0, 2, '2020-05-07 18:43:15', '0'),
(443, '', '0557577501', '', 'الهفوف', '', 0, 'bo50as@hotmail.com', '', '', '', '', '', '', '', 'حي وسط الهفوف التاريخي', 'مؤسسه ماسه نبراس للالماس والمجوهرات', 'maast nibras', '', 'http://igoldapp.com/images/9f7036249380e6e990c5150655b671c7.jpg', 'http://igoldapp.com/images/9e5de3d1f79b3bc8226ade4514dad5a3.jpg', '', 0, 2147483647, 1, 0, 0, 0, 2, '2020-05-07 19:51:44', '0'),
(445, '', '0545808003', '', 'الدمام', '', 0, 'ali.latyf@gmail.com', '', '', '', '', '', '', '', 'الدمام شارع الملك سعود ', 'مجوهرات الصبايا ', 'Sabaya jewllery', '', 'http://igoldapp.com/images/cddb084887e79e90ccf65e4be9558aab.jpeg', 'http://igoldapp.com/images/e8c346d3d306591d5b2692344fa8baca.png', 'http://igoldapp.com/images/c10452b896e0e476f08393b0c28b48d8.png', 0, 2147483647, 3, 0, 0, 0, 2, '2020-05-07 23:02:23', '0'),
(446, '', '0590005444', '', 'الدمام', '', 0, 'msagold5@gmail.com', '', '', '', '', '', '', '', 'سوق الدمام المركزي', 'مؤسسة موسى عبدالله محمد النمر', 'Mosa alnemer', '', '', '', '', 0, 2050119141, 1, 0, 0, 0, 2, '2020-05-07 23:19:15', '0'),
(447, '', '0590771740', '', 'الاحساء', '', 0, 'abbasjewelary@gmail.com', '', '', '', '', '', '', '', 'الأحساء-المبرز-سوق الذهب بمدينة المبرز', 'عباس بن صالح للذهب', 'Abbas binsaleh gold', '', 'http://igoldapp.com/images/b7523775cdbb5b75847cfff41dbc1b3b.jpg', 'http://igoldapp.com/images/375d64a483e2436d54f14d65a1d1c05a.jpg', 'http://igoldapp.com/images/67a5858e1274c4700859798d0c911b81.jpg', 0, 2147483647, 1, 0, 0, 0, 2, '2020-05-08 10:19:13', '0'),
(448, '', '0544465424', '', 'Dammam', '', 0, 'jaffar.a.alnasser@gmail.com', '', '', '', '', '', '', '', 'الدمام', 'مصنع جعفر الناصر', 'Jaffar Alnasser Factory ', '', '', '', '', 0, 2050119821, 1, 0, 0, 0, 2, '2020-05-08 10:37:52', '0'),
(449, '', '0536873782', '', 'الدمام ', '', 0, 'alimax77@gmail.com', '', '', '', '', '', '', '', 'الدمام - سوق الحب - سما الاشراق للمجوهرات', 'مجوهرات سما الاشراق', 'Sama AlEsharaq jewelry ', '', 'http://igoldapp.com/images/85db61fbc3af510d41c251eb5439e478.jpeg', 'http://igoldapp.com/images/a227d7e175609b66dc59848fb467c663.jpg', 'http://igoldapp.com/images/2baba3506cfac8610eef3cf93719f060.jpg', 0, 2050049628, 1, 0, 0, 0, 2, '2020-05-08 11:10:51', '0'),
(450, '', '0555457888', '', 'الدمام', '', 0, 'ajayb.al.hasna1984@gmail.com', '', '', '', '', '', '', '', 'حي الدواسر - شارع ابو ليلى المازني', 'جوهرة عجائب الحسناء للمجوهرات', 'ajaib al hasna Jewellery', '', 'http://igoldapp.com/images/ca0354ebd25d0cf0332b035caa7d988f.jpeg', 'http://igoldapp.com/images/cd7132f7bdbb4873087144cd03d79de1.jpg', 'http://igoldapp.com/images/66467bd38000c203aa44a255a5744e25.jpg', 0, 2050129047, 2, 0, 0, 0, 2, '2020-05-08 11:34:31', '0'),
(451, '', '0566557429', '', 'الاحساء', '', 0, 'alhotali@gmail.com', '', '', '', '', '', '', '', 'المبرز', 'مجوهرات لؤلؤة المهناء', 'Lualuat Almuhanna', '', 'http://igoldapp.com/images/96cc79f4b33b3ea8554535fcf500840b.jpg', 'http://igoldapp.com/images/fd3421499f9bed3b6fcea8cd867ad670.jpg', 'http://igoldapp.com/images/263649c37af0720edf0c274e02aac34a.jpg', 0, 2147483647, 1, 0, 0, 0, 2, '2020-05-08 11:54:05', '0'),
(452, '', '0562443499', '', 'الاحساء', '', 0, 'Jawahir.almuhanna@gmail.com', '', '', '', '', '', '', '', 'المبرز', 'مجوهرات جواهر المهناء', 'Jawahir Almuhanna', '', 'http://igoldapp.com/images/3b42df2c54807c168a822e8e06ef5419.jpg', 'http://igoldapp.com/images/5408f5b3a40e7f6ffc1d539ecd55b801.jpg', 'http://igoldapp.com/images/caf441f6a2b00bb1bf43330de25a2ac6.jpg', 0, 2147483647, 1, 0, 0, 0, 2, '2020-05-08 12:31:02', '0'),
(453, '', '0548039330', '', 'الاحساء', '', 0, 'kz_906@hotmail.com', '', '', '', '', '', '', '', 'المبرز', 'الهوانم للمجوهرات', 'Alhawanim ', '', 'http://igoldapp.com/images/857ee0b5b9682de012ace62bb24e35ea.jpg', 'http://igoldapp.com/images/6fd38ca4a2e002e6c364fa82fefc6c91.jpg', 'http://igoldapp.com/images/8cfb5ec54b14c18451c2115488fe05d1.jpg', 0, 2147483647, 1, 0, 0, 0, 2, '2020-05-08 13:00:43', '0'),
(454, '', '0536342419', '', 'الدمام', '', 0, 'alnemer@hotmail.co.uk', '', '', '', '', '', '', '', 'سوق الحب', 'علي النمر للمجوهرات', 'Ali Alnemer Jewelleries ', '', 'https://igoldapp.com/images/f0f8817ece5b42390f83f5729932a691.jpeg', 'https://igoldapp.com/images/cb31a4e8373b95262989f5676a045f2f.png', 'https://igoldapp.com/images/e5c724419eb9c40c3a97b71f5515b811.png', 0, 2147483647, 2, 0, 0, 0, 2, '2020-05-08 19:44:39', '0'),
(455, '', '0563506660', '', 'الأحساء', '', 0, 'a-a-ww@hotmail.com', '', '', '', '', '', '', '', 'المبرز ', 'لؤلؤة الوايل ', 'Loaloat Alwayil', '', 'https://igoldapp.com/images/583fb4162ee9d5e028fb5babebcf4729.jpeg', 'https://igoldapp.com/images/07fadbc7012e4b1905cda728911095b1.jpeg', 'https://igoldapp.com/images/59ee48108c8ac3ea6845c47dbb486b69.jpeg', 0, 2147483647, 1, 0, 0, 0, 2, '2020-05-09 07:24:09', '0'),
(456, '', '0501502405', '', 'الاحساء', '', 0, 'alwayil.gold@gmail.com', '', '', '', '', '', '', '', 'الهفوف - سوق الذهب', 'دانه فاطمة للذهب و المجوهرات', 'Dana Fatimah', '', '', 'https://igoldapp.com/images/fadafad068022c71ebf3dffa34eef8d5.jpg', 'https://igoldapp.com/images/a9886858d4b20b2fcb213094650c38b3.jpg', 0, 2147483647, 1, 0, 0, 0, 2, '2020-05-10 12:26:31', '0'),
(457, '', '0507922763', '', 'الاحساء الهفوف', '', 0, 'hossainalbather@gmail.com', '', '', '', '', '', '', '', 'الاحساء-الهفوف-الرفعه-سوق الذهب', 'دار الباذر  ذهب ومجوهرات', 'DAR ALBATHER', '', 'https://igoldapp.com/images/70e1e850ccf9ecfd8c674c0d44f4fe0b.png', 'https://igoldapp.com/images/86c30911fa546b61569bfa55c7dfc92d.png', 'https://igoldapp.com/images/55515231b25ef4bdb2dcfefb8b0c94c6.png', 0, 2147483647, 1, 0, 0, 0, 2, '2020-05-10 19:01:54', '0'),
(460, '', '0502666232', '', 'الاحساء', '', 0, 'oww-@hotmail.com', '', '', '', '', '', '', '', 'الاحساء المبرز', 'مصنع محمد علي عبدالله المهناء للتصنيع', 'Mohammed Ali Abdullah Al-Muhanna Factory for Manufacturing', '', '', '', '', 0, 2147483647, 1, 0, 0, 0, 2, '2020-05-11 10:40:30', '0'),
(461, '', '0555815080', '', 'الدمام', '', 0, 'qsr.gold@gmail.com', '', '', '', '', '', '', '', 'سوق الحب', 'جواهر قصر العز', 'Jewels palace of ezz', '', '', '', '', 0, 2050115626, 2, 0, 0, 0, 2, '2020-05-11 20:07:48', '0');

-- --------------------------------------------------------

--
-- Table structure for table `offer`
--

CREATE TABLE `offer` (
  `id` int(11) NOT NULL,
  `name` text,
  `offer_type` varchar(150) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `percentage` decimal(10,0) DEFAULT '0',
  `items_number` float DEFAULT NULL,
  `items_number_sub` float DEFAULT NULL,
  `comment` text,
  `created_date` text,
  `is_active` int(11) DEFAULT NULL,
  `branches_which_active` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offer`
--

INSERT INTO `offer` (`id`, `name`, `offer_type`, `price`, `percentage`, `items_number`, `items_number_sub`, `comment`, `created_date`, `is_active`, `branches_which_active`) VALUES
(3, 'نوع 3 قطعة وسعر', '3', 0, 0, 0, 0, '0', '2020-07-27 13:55:37', 1, '3,7,8,9'),
(4, 'الحبة والدرزن', '8', 0, 0, 0, 0, '0', '2020-07-27 13:55:37', 0, '3,6,7,8'),
(9, '2 سعر 2 مجانا', '9', 0, 0, 0, 0, '0', '2020-07-27 13:55:37', 1, '3'),
(59, 'عميل مميز', '7', 0, 0, 0, 0, '0', '2020-08-18 07:23:37', 1, '3,6,7,8,9'),
(60, 'عرض رمضان', '1', 20, 0, 0, 0, '0', '2020-08-23 15:57:33', 0, ''),
(61, 'عرض العيد الوطني', '1', 52.17, 0, 0, 0, '0', '2020-08-23 15:57:57', 1, '3'),
(64, 'عرض خصم بنسبة', '6', 0, 25, 0, 0, '0', '2020-08-23 16:00:42', 1, '3'),
(65, 'عرض 4 و 4', '2', 0, 0, 3, 0, '0', '2020-09-07 15:13:22', 1, ''),
(66, 'فانتاستك', '1', 69.57, 0, 0, 0, '0', '2020-11-01 16:38:59', 0, ''),
(67, 'علي', '2', 0, 0, 4, 0, '0', '2020-11-01 16:52:38', 0, ''),
(68, '1+1', '2', 0, 0, 1, 0, '0', '2020-11-01 17:01:22', 0, ''),
(71, 'العرض الاسبوعي', '2', 0, 0, 1, 0, '0', '2020-11-16 09:14:06', 1, '3,9'),
(72, ' العرض اليومي', '1', 200, 0, 0, 0, '0', '2020-11-16 09:15:41', 0, '3,6,7'),
(73, 'ww', '9', 33, 0, 0, 0, '0', '2021-03-08 06:09:53', 0, NULL),
(83, 'عرض 1+2', '5', 0, 0, 1, 2, '0', '2021-04-04 14:38:02', 0, '3,6,7,8,9'),
(84, '2+3', '5', 0, 0, 2, 3, '0', '2021-04-15 18:23:12', 1, '3,6,7,8,9');

-- --------------------------------------------------------

--
-- Table structure for table `offers_pieaceprice`
--

CREATE TABLE `offers_pieaceprice` (
  `id` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL,
  `offer_type` int(11) DEFAULT NULL,
  `main_product_id` int(11) DEFAULT NULL,
  `number_mainproduct` float DEFAULT NULL,
  `main_productprice` float NOT NULL,
  `sub_mainproduct_id` int(11) DEFAULT NULL,
  `sub_productprice` float NOT NULL,
  `is_active` int(11) DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offers_pieaceprice`
--

INSERT INTO `offers_pieaceprice` (`id`, `offer_id`, `offer_type`, `main_product_id`, `number_mainproduct`, `main_productprice`, `sub_mainproduct_id`, `sub_productprice`, `is_active`, `date_added`) VALUES
(9, 3, 3, 12, 2, 10.2222, 15, 50.1111, 0, '2021-04-24 13:54:11'),
(10, 3, 3, 2, 1, 50, 3, 100, 0, '2021-04-28 00:16:45');

-- --------------------------------------------------------

--
-- Table structure for table `offers_products`
--

CREATE TABLE `offers_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` text,
  `type3_main_offer` int(11) NOT NULL DEFAULT '0',
  `type3_sub_offer` int(11) NOT NULL DEFAULT '0',
  `3_offer` int(11) NOT NULL DEFAULT '0',
  `4_offer` int(11) NOT NULL DEFAULT '0',
  `4_offer_darzanprice` float NOT NULL DEFAULT '0',
  `9_offer` int(11) NOT NULL DEFAULT '0',
  `type9_price1` int(11) NOT NULL DEFAULT '0',
  `type9_price2` int(11) NOT NULL DEFAULT '0',
  `59_offer` float DEFAULT '0',
  `60_offer` float DEFAULT '0',
  `61_offer` float DEFAULT '0',
  `64_offer` float DEFAULT '0',
  `65_offer` float DEFAULT '0',
  `66_offer` float DEFAULT '0',
  `67_offer` float DEFAULT '0',
  `68_offer` float DEFAULT '0',
  `71_offer` float DEFAULT '0',
  `72_offer` float DEFAULT '0',
  `73_offer` float DEFAULT '0',
  `83_type5_main_offer` float DEFAULT '0',
  `83_type5_sub_offer` float DEFAULT '0',
  `83_type5_main_price` float DEFAULT '0',
  `84_type5_main_offer` float DEFAULT '0',
  `84_type5_sub_offer` float DEFAULT '0',
  `84_type5_main_price` float DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offers_products`
--

INSERT INTO `offers_products` (`id`, `product_id`, `product_name`, `type3_main_offer`, `type3_sub_offer`, `3_offer`, `4_offer`, `4_offer_darzanprice`, `9_offer`, `type9_price1`, `type9_price2`, `59_offer`, `60_offer`, `61_offer`, `64_offer`, `65_offer`, `66_offer`, `67_offer`, `68_offer`, `71_offer`, `72_offer`, `73_offer`, `83_type5_main_offer`, `83_type5_sub_offer`, `83_type5_main_price`, `84_type5_main_offer`, `84_type5_sub_offer`, `84_type5_main_price`) VALUES
(22, 1, 'لوحة مفاتيح', 0, 0, 0, 0, 100, 0, 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 200, 0, 0, 0),
(23, 2, 'ماوس', 0, 0, 0, 0, 0, 0, 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20),
(24, 3, 'عطر Joy Dior', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0),
(25, 4, 'عطر الورود', 0, 0, 0, 0, 200, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0),
(26, 5, 'عطر الذهبي', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0),
(27, 6, 'كريك', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(28, 7, 'برويطة', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(29, 8, 'ماء اكوافينا', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(30, 9, 'ماء اكوافينا 1', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(31, 10, 'تجربة 5', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(32, 11, 'مربع', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(33, 12, 'عطر حسن', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(34, 13, 'محمد', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(35, 14, 'امنتي', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0),
(36, 15, 'سيارة', 0, 0, 0, 0, 0, 0, 5, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(37, 16, 'sajed', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(38, 17, 'dggdfhfg', 0, 0, 0, 0, 0, 0, 7, 6, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(39, 18, 'e', 0, 0, 0, 0, 0, 0, 100, 80, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(40, 19, 'تجربة', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(41, 20, '5تي اش', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0),
(42, 21, 'هاي كلاس نوير', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0),
(43, 22, 'عرض افتتاح القارة', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(44, 23, 'فلورز', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 200, 0, 0, 0),
(45, 24, 'تجربة 60', 0, 0, 0, 0, 150, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0),
(46, 25, 'ee', 0, 0, 0, 0, 50, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 500, 0, 0, 0),
(47, 26, 'ايس', 0, 0, 0, 0, 108.7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 100, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `offer_groups`
--

CREATE TABLE `offer_groups` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `products_in_group` text,
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offer_groups`
--

INSERT INTO `offer_groups` (`id`, `title`, `products_in_group`, `date_add`) VALUES
(3, 'الحديقة 3', '12,11,10,8,7,4,3,2,1', '2020-09-08 08:58:43'),
(4, 'المجموعة 2', '18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1', '2021-01-08 09:37:36'),
(6, 'الكل', '26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1', '2021-03-08 11:31:24'),
(9, 'أهل الرياض', '21,20,19', '2021-03-08 11:43:33'),
(10, 'مجموعة المدرسين', '15,14', '2021-03-08 11:46:56'),
(11, 'مجموعة العطور', '15,14,13,12', '2021-03-08 11:49:18'),
(12, 'العطور', '16,15,14', '2021-03-08 11:53:46'),
(13, 'العطور الشرقية', '5,4,3', '2021-03-08 22:04:00');

-- --------------------------------------------------------

--
-- Table structure for table `offer_types`
--

CREATE TABLE `offer_types` (
  `id` int(11) NOT NULL,
  `name` text,
  `comment` text,
  `created_date` text,
  `is_active` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offer_types`
--

INSERT INTO `offer_types` (`id`, `name`, `comment`, `created_date`, `is_active`) VALUES
(1, 'سعر ثابت', 'يتم اضافة مجموعة منتجات الى العرض بسعر ثابت يحدده مدير لوحة التحكم ', NULL, 1),
(2, 'قطعة وأكثر', 'يتم اضافة مجموعة منتجات الى العرض وعند شراء عدد معين من منتج (أ) يمكنه الحصول على عدد  مشابه من نفس المنتج مثال اشترى بيتزا وتحصل على بيتزا مجانا او اشترى 2 بيبسي وتحصل على 2 من نفس المنتج بيبسي مجانا', NULL, 1),
(3, 'قطعة وسعر', 'لا يتم اضافة  قائمة منتجات الى  العرض ولكني عند شراء عدد معين من منتج اساسي (أ) يحصل على منتج (ب) بسعر معين وممكن ان يكون السعر صفر أى مجانا', NULL, 1),
(4, 'قطعة وأكثر منتجات مختلفة (نفس القائمة)', 'يتم اضافة قائمة منتجات الى العرض وعند شراء عدد معين من اى منتج داخل القائمة يمكنهالحصول على عدد معين أخر من أى منتج أخر شرط ان يكون داخل قائمة منتجات العرض\nمثال:  منتجات أ ب ج د عند شراء قطعة من  أ يمكنه الحصول على قطعة من ب وقطعة من ج مجانا', NULL, 0),
(5, 'قطعة وأكثر منتجات مختلفة (قائمتين)', 'يتم انشاء قائمة منتجات رئيسية وقائمة منتجات فرعيه وعند شراء عدد معين من منتج من القائمة الرئيسية يمكنه الحصول على عدد معين من منتجات محتلفة من القائمة الفرعية', NULL, 1),
(6, 'نسبة ثابتة', 'مثال خصم 50 % أو 25 % على كل المنتجات فى القائمة', NULL, 1),
(7, 'عميل مميز', 'قائم بالمنتجات عليها خصم بنسبة معينة تتغير وفقا لمدى تميز العميل', NULL, 1),
(8, 'حبة ودرزن', 'عرض البخور الحبة 11.55 والدرزن 115 ', NULL, 1),
(9, '2سعر و 2مجانا', 'قطعة بسعر والثانية بسعر والثالثة والرابعة مجانا', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `title` varchar(350) NOT NULL,
  `slug` varchar(350) NOT NULL,
  `content` text NOT NULL,
  `status` varchar(100) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `title`, `slug`, `content`, `status`, `meta_keywords`, `meta_description`) VALUES
(4, 'من نحن', 'من_نحن', '<p><span style=\"font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</span><br class=\"line-break\" style=\"margin-bottom: 27px; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\"><span style=\"font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.</span><br class=\"line-break\" style=\"margin-bottom: 27px; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\"><span style=\"font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</span><br class=\"line-break\" style=\"margin-bottom: 27px; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\"><span style=\"font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</span></p><p><span style=\"font-family: JannaLT-Regular; font-size: 18px; text-align: right;\"><br></span></p><p><span style=\"font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</span><br class=\"line-break\" style=\"margin-bottom: 27px; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\"><span style=\"font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.</span><br class=\"line-break\" style=\"margin-bottom: 27px; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\"><span style=\"font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</span><br class=\"line-break\" style=\"margin-bottom: 27px; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\"><span style=\"font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</span><span style=\"font-family: JannaLT-Regular; font-size: 18px; text-align: right;\"><br></span><br></p>', '1', 'koko , jpjp', ''),
(10, 'الشروط والاحكام', 'الشروط_والاحكام', '      \r\n                    <p dir=\"RTL\"><strong>سياسة الاستخدام</strong></p>\r\n\r\n<p dir=\"RTL\">الشروط والأحكام</p>\r\n\r\n\r\n\r\n<p dir=\"RTL\">يرجى قراءة الشروط والاحكام بالاسفل قبل استخدام التطبيق والقيام بأي عملية حيث يعد شراءك كمستخدم او بيعك كتاجر عن طريق التطبيق موافقة منك على جميع الشروط والاحكام. كما يرجى الاطلاع على سياسة الخصوصية الخاصة بنا التي يخضع لها موقعنا الالكتروني والتطبيق ايضا.</p>\r\n\r\n<p dir=\"RTL\">- جميع عمليات الشراء عن طريق التطبيق هي محط قبول او رفض من قبل مقدم الخدمة \"التاجر\" كما يحق لإدارة التطبيق برفض اي طلب شراء بناء على ماتراه مناسب.</p>\r\n\r\n<p dir=\"RTL\">- تفاصيل الطلبات والشراء وحالات القبول والرفض ستكون دائما متاحة في تفاصيل الطلب سواء في صفحات التاجر او صفحات المستخدم.</p>\r\n\r\n<p dir=\"RTL\">- إصدارك لأمر الشراء هو تفويض منك لنا أو لأي طرف ثالث متخصص في عمليات السداد الإلكتروني بخصم قيمة المشتريات من رصيد بطاقتك الائتمانية او بطاقة مدى أو بطاقة الخصم، علماً بأننا نقبل السداد بموجب<span dir=\"LTR\">: </span></p>\r\n\r\n<p dir=\"RTL\">بطاقة ائتمانية </p>\r\n\r\n<p dir=\"RTL\"> بطاقة مدى</p>\r\n\r\n<p dir=\"RTL\"> كوبونات خصم او باستخدام المحفظة الخاصة بك في التطبيق</p>\r\n\r\n<p dir=\"RTL\">- جميع التعاملات المالية بين الادارة والتجار تكون عن طريق حوالات بنكية وكما يمكن ان نقوم نطلب من التجار أن نطلب منك فتح حساب لدى شركات معالجة المدفوعات الأخرى الخاصة بنا لاجل تسهيل استخدام دفعات عبر البطاقات الائتمانية، بما في ذلك قبول الاحكام والشروط الخاصة بها وتقديم التفاصيل الخاصة بك نيابة عنك. وتخولنا أنت بموجبه القيام بذلك ولايتحمل آي قولد اي مسؤولية عن اي ضرار او خسارة جراء ذلك.</p>\r\n\r\n<p dir=\"RTL\">- جميع وسائل الدفع قابلة للتحديث والتغيير بناء على مايرى مناسب من قبل إدارة آي قولد.</p>\r\n\r\n<p dir=\"RTL\">- جميع طلبات الغاء الشراء مقبولة في حال لم يتم شحن المنتج المطلوب.س</p>\r\n\r\n<p dir=\"RTL\">- توصيل الطلبات للعملاء:</p>\r\n\r\n<ul>\r\n	<li dir=\"RTL\">لايتم احتساب اي رسوم اضافية لشحن الطلبات عند الشراء من تطبيق آي قولد،</li>\r\n	<li dir=\"RTL\">يتم تحديد من قبل شركات الشحن المتعاقدة مع التجار، وقد تختلف المدة من شركة الى شركة كما سيظهر للمشتري دائما تفاصيل رقم الشحنة والشركة لكي يمكنه تتبع شحنته. </li>\r\n	<li dir=\"RTL\">يقوم التاجر بتنسيق علمية الشحن وادخال تفاصيل الشحنة لكل طلب، كما سيكون هو المسؤول الاول عن كل مايخص الشحنة من صحة بيانات ورسوم والتأكد من عدم وجود اي مشاكل في ايصالها للمشتري.</li>\r\n	<li dir=\"RTL\">تطبق شروط وأحكام شركة الشحن في حال وجود اي تعثر في الوصول لعنوان المشتري او تعذر الاتصال به.</li>\r\n	<li dir=\"RTL\">تصبح المنتجات مملوكة منك بمجرد ان نوصلها إليك في عنوان التسليم وسدادك لقيمتها كاملة<span dir=\"LTR\">. </span></li>\r\n</ul>\r\n\r\n<p dir=\"RTL\"> </p>\r\n\r\n<p dir=\"RTL\"> </p>\r\n\r\n<ul>\r\n	<li dir=\"RTL\"> يتم اصدار فاتورة من قبل التطبيق باسم التاجر لكل علمية شراء.</li>\r\n	<li dir=\"RTL\"> يمكن للمشتري تنفيذ عملية شراء بمنتجات من تجار مختلفين، واتمام عملية دفع واحدة حسب طرق الدفع المتوفرة للتطبيق، كما سيتم آليا فصل الطلبات حسب التجار والتعامل مع كل طلب كما أنه طلب مستقل.</li>\r\n	<li dir=\"RTL\"> يوضح الجدول التالي سياسة الإرجاع والاستبدال الخاصة بنا حسب فئة كل منتج<span dir=\"LTR\">: </span></li>\r\n</ul>\r\n\r\n<p dir=\"RTL\"> </p>\r\n\r\n<p dir=\"RTL\"> </p>\r\n\r\n<p dir=\"RTL\"> </p>\r\n\r\n<p dir=\"RTL\"> </p>\r\n\r\n<p dir=\"RTL\"> </p>\r\n\r\n<p dir=\"RTL\"> </p>\r\n\r\n<p dir=\"RTL\"> </p>\r\n\r\n<p dir=\"RTL\"> </p>\r\n\r\n<table dir=\"rtl\" cellspacing=\"0\" cellpadding=\"0\" border=\"1\" align=\"right\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width:208px\">\r\n			<p dir=\"RTL\">سبب الاسترجاع</p>\r\n			</td>\r\n			<td style=\"width:208px\">\r\n			<p dir=\"RTL\">أمكانية الاسترجاع</p>\r\n			</td>\r\n			<td style=\"width:208px\">\r\n			<p dir=\"RTL\">شروط الارجاع</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:208px\">\r\n			<p dir=\"RTL\">استلام منتج خاطئ</p>\r\n			</td>\r\n			<td style=\"width:208px\">\r\n			<p dir=\"RTL\">يمكن استرجاعه</p>\r\n			</td>\r\n			<td style=\"width:208px\">\r\n			<ul>\r\n				<li dir=\"RTL\">يتم الارجاع خلال يوم واحد فقط بعد استلام الشحنة</li>\r\n				<li dir=\"RTL\">لم يسبق استخدام المنتج</li>\r\n				<li dir=\"RTL\">يكون المنتج في حالته الاصلية مع كل الاكسسوارات التابعة له مثل العلب والصنادق (ان وجدت)</li>\r\n			</ul>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:208px\">\r\n			<p dir=\"RTL\">استلام منتج ليس كما تم وصفه من قبل التاجر في التطبيق</p>\r\n			</td>\r\n			<td style=\"width:208px\">\r\n			<p dir=\"RTL\">يمكن استرجاعه</p>\r\n			</td>\r\n			<td style=\"width:208px\">\r\n			<ul>\r\n				<li dir=\"RTL\">يتم الارجاع خلال يوم واحد فقط بعد استلام الشحنة</li>\r\n				<li dir=\"RTL\">لم يسبق استخدام المنتج يكون المنتج في حالته الاصلية مع كل المحلقات التابعة له مثل العلب والصنادق (ان وجدت)</li>\r\n			</ul>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:208px\">\r\n			<p dir=\"RTL\">تغيير رغبة المشتري</p>\r\n			</td>\r\n			<td style=\"width:208px\">\r\n			<p dir=\"RTL\">يمكن استرجاعه</p>\r\n			</td>\r\n			<td style=\"width:208px\">\r\n			<ul>\r\n				<li dir=\"RTL\">يتم الارجاع خلال يوم واحد فقط بعد استلام الشحنة</li>\r\n				<li dir=\"RTL\">لم يسبق استخدام المنتج يكون المنتج في حالته الاصلية مع كل الاكسسوارات التابعة له مثل العلب والصنادق (ان وجدت)</li>\r\n				<li dir=\"RTL\">يتحمل المشتري تكلفة ارجاع المنتج وشحنه.</li>\r\n			</ul>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<div style=\"clear:both;\"> </div>\r\n\r\n<p dir=\"RTL\"> </p>\r\n\r\n<p dir=\"RTL\">- لايقبل بالاسترجاع ابدا في كل من الحالات التالية:</p>\r\n\r\n<ul>\r\n	<li dir=\"RTL\">التي سبق استخدامها أو تسببتَ أنت في الإضرار بها أو أصبحت على حال يختلف عما تسلَّمتها عليه<span dir=\"LTR\">. </span></li>\r\n	<li dir=\"RTL\">أي سلعة مر على تاريخ استلامها أكثر من يومين<span dir=\"LTR\">.</span></li>\r\n	<li dir=\"RTL\">أي منتجات خالية من الرقم المتسلسل الخاص بها أو تم التلاعب به<span dir=\"LTR\">. </span></li>\r\n</ul>\r\n\r\n<p dir=\"RTL\"> </p>\r\n\r\n<p dir=\"RTL\">- في حال تطلب استرجاع المبلغ للمشتري فيتم إعادة المبلغ بطريقة مماثلة لنفس طريقة السداد الاولى والتي قد تخضع لبعض الرسوم الادارية من قبل وسطاء الدفع والبنوك. كما تتم معالجة عميات استرجاع المبالغ خلال 30 يوم عمل من طلب الاسترجاع واستلام المنتجات المستردة.</p>\r\n\r\n<p dir=\"RTL\"> - تقع المسؤولية التامة على التاجر في شحن منتج سليم مطابق لطلب المشتري ومطابق للمواصفات المعروضة، ويحق للمشتري رفع شكوى في حال حدث غير ذلك على التاجر مباشرة، كما اننا في آي قولد سنقوم بتوفير جميع المعلومات والتفاصيل المطلوبة من اي جهات رسمية.</p>\r\n\r\n<p dir=\"RTL\"> </p>\r\n\r\n<p dir=\"RTL\">- تخضع شروط البيع والحقوق والالتزامات غير التعاقدية لقوانين المطبقة في المملكة العربية السعودية.</p>\r\n\r\n<p dir=\"RTL\">- إذا كنت غير راضٍ عن أي المنتجات التي اشتريتها عبر الموقع، يمكنك التواصل معنا عبر البريد الإلكتروني، أو عبر مواقع التواصل الاجتماعي الخاص بنا ، أو المحادثة المباشرة على الموقع، أو الاتصال بمركز الاتصال الخاص بنا.</p>\r\n\r\n<p dir=\"RTL\">- إذا لم يستطع المشتري التوصل الى حل خلال خمسة وأربعين (45) يوماً من إخطاره للبائع بمشكلته، يمكنه اللجوء إلى التحكيم وفقاً لقواعد التحكيم الخاصة بـ المملكة العربية السعودية</p>\r\n\r\n<p dir=\"RTL\">- شروط البيع هذه ملزمة لضمان مصلحة أطرافها وخلفائهم والمتنازل لهم المسموح لهم، وتوافق على عدم التنازل عن أو نقل صلاحية تلك الشروط أو أي من الحقوق أو الالتزامات التي تخصك بموجب شروط البيع الماثلة سواء مباشرة أو بطريقة غير مباشرة دون الحصول على موافقة مبدئية خطية من قبلنا على ألا نمتنع من جانبنا عن إصدار الموافقة دون إبداء سبب معقول<span dir=\"LTR\">. </span></p>\r\n\r\n<p dir=\"RTL\">-  لا يعتبر أي من أطراف الاتفاقية مسؤولاً عن أي خسائر أو أضرار أو تأخير أو إخفاق في الأداء بسبب أعمال خارجة عن إرادتهم سواء كانت تلك الأعمال أو الأحداث يمكن أو لا يمكن التنبؤ بها بشكل معقول (بما في ذلك أحداث القضاء والقدر أو الأحكام التشريعية أو أحكام القضاء أو القرارات الحكومية التنظيمية أو الصادرة عن الحكومات المحلية أو الحكومة الفيدرالية أو المحاكم أو هيئات حاكمة أو أعمال مقاولي الباطن أو أي طرف ثالث مورد للبضائع أو الخدمات لنا أو المقاطعة الاقتصادية أو انقطاع التيار الكهربائي أو الاضطرابات العمالية او اي من الكوارث الطبيعية)<span dir=\"LTR\">. </span></p>\r\n\r\n<p dir=\"RTL\">- لايجوز أن يُفسر تنازلنا عن أي من أحكام شروط البيع هذه على أنه تنازل عن أي أحكام أخرى واردة فيها (سواء كانت أحكاماً مشابهة أم مختلفة)، ولا يجوز أن يُفسر التنازل عن أحد الأحكام على أنه تنازل دائم عنه، إلا إذا عبرنا عن ذلك صراحة وبصورة خطية<span dir=\"LTR\">. </span></p>\r\n\r\n<p dir=\"RTL\"><span dir=\"LTR\"> </span><br>\r\n </p>\r\n\r\n<p dir=\"RTL\"> </p>\r\n\r\n<p dir=\"RTL\">- نحن آي قولد نؤمن بأهمية حفظ المعلومات وخصوصية التجار والمستخدمين ووهذا هو السبب في أننا وضعنا معايير عالية لمعاملاتنا المضمونة والالتزام بخصوصية معلومات العميل. كما يرجى ملاحظة أن سياسة الخصوصية لدينا سوف تخضع للتغييرات في أي وقت من الأوقات دون أي إشعار مسبق.</p>\r\n\r\n<p dir=\"RTL\">- المعلومات الشخصية التي يتم جمعها عادةً ما نجمع معلومات شخصية مثل الاسم ومعرف البريد الإلكتروني ورقم الاتصال ومثل هذه التفاصيل الأخرى أثناء عملية إنشاء حساب مع موقع وتطبيق آي قولد ستكون محل حفظ ولن تشارك مع اي جهة اخرى، كما يمكننا استخدام البيانات للتواصل على كلا الصعيدين الفردي والجماعي.</p>\r\n\r\n<p dir=\"RTL\">- نحن نستخدم البيانات الشخصية لتزويدك بطلبات الخدمة. لأغراض مختلفة مثل استكشاف الأخطاء وإصلاحها، وجمع الرسوم، والدراسات الاستقصائية، وتقديم معلومات عن العروض المختلفة، نحتاج إلى معلوماتك الشخصية. نحن نجمع ونحلل أيضًا البيانات السكانية وبيانات الملف الشخصي حول نشاط المستخدم في موقعنا. نحن نحرص على تحديد واستخدام عنوان<span dir=\"LTR\"> IP </span>لتشخيص المشاكل في موقعنا<span dir=\"LTR\">.</span></p>\r\n\r\n<p dir=\"RTL\"><strong>- </strong>يتميز موقعنا بتدابير أمنية صارمة وبسبب هذا، فإننا نساعد في حماية ضياع وتعديل وإساءة استخدام المعلومات الخاضعة لسيطرتنا. إذا كنت ترغب في تغيير وصولك إلى معلومات الحساب الشخصي، فيُرجى التأكد من توفير خادم آمن. بمجرد توفر المعلومات معنا، نتأكد من الالتزام بإرشادات الأمان الصارمة وحماية نفسها من الوصول غير المصرح به<span dir=\"LTR\">.</span></p>\r\n\r\n<p dir=\"RTL\"> <br>\r\n<br>\r\n<br>\r\n- تضمن آي قولد أن جميع المحلات المشتركة في التطبيق لديها سجلات رسمية تشمل سجل وزارة التجارة ورخصة معادن سارية المفعول وقت تسجيل المحل البائع في التطبيق. سيكون ضمان المنتج الذي تم شراءه عن طريق تطبيق آي قولد تحت مسؤولية المحل البائع للمنتج والذي يضمن خلو منتجك من أي عيوب في الخامات والمواد أو التصنيع أو أي نقص في الألماس أو المجوهرات المرصعة التي قد تحدث بعد الشراء. ستتم الإصلاحات من قبل المحل البائع وستطبق شروط ضمان هذا البائع<span dir=\"LTR\">. </span></p>\r\n\r\n<p dir=\"RTL\"><br>\r\n- لا ينطبق الضمان على أي حالات غير حالات عيوب الخامات والمواد أو التصنيع، ويكون الضمان محدود بإصلاح المنتج المعيب أو استبدال الجزء المعيب أو استبدال المنتج أو إعادة المبلغ بسعر الشراء من التطبيق للمنتج نفسه.</p>\r\n\r\n<p dir=\"RTL\"><br>\r\n- لا يشكل إصلاح المنتج أو إستبداله بموجب شروط هذا الضمان أي حق لتمديد أو تجديد الضمان، وتكون شروط الضمان وفقا للمحل البائع. في حالة عدم إمكانية إصلاح منتجك ولكن لا يزال قيد الضمان، سنقوم باستبدال المنتج، وإذا كان الاستبدال غير متوفر، سيتم رد المبلغ بالكامل<span dir=\"LTR\">. </span></p>\r\n\r\n<p dir=\"RTL\"><br>\r\n- يتوجب على العميل تسليم المنتج الواقع تحت الضمان بحالته الأصلية وبغلافه الأصلي في مدة أقصاها يوم من تاريخ الاستلام ويتضمن جميع الملحقات وبطاقة الضمان (إن وجدت)</p>\r\n\r\n<p dir=\"RTL\"><br>\r\n </p>\r\n\r\n<p dir=\"RTL\"> </p>\r\n', '1', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `title` varchar(350) NOT NULL,
  `title_en` varchar(100) DEFAULT NULL,
  `barcode_num` text,
  `cost` float DEFAULT NULL,
  `price` float DEFAULT NULL,
  `sale` float DEFAULT NULL,
  `price_wholesale` float DEFAULT NULL,
  `dep_id` int(11) NOT NULL,
  `photo` text,
  `comment` text,
  `notification_check` int(11) NOT NULL DEFAULT '0',
  `created_date` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `title`, `title_en`, `barcode_num`, `cost`, `price`, `sale`, `price_wholesale`, `dep_id`, `photo`, `comment`, `notification_check`, `created_date`) VALUES
(1, 'لوحة مفاتيح', 'KeyBoard', '826876480002', 391.3, 52.17, 1, 100, 2, 'http://demo.f4h.com.sa/sultana/images/cp/products/funct.jpg', '', 0, '2020-09-08 09:00:32'),
(2, 'ماوس', 'Mouse', '860366564006', 20, 40, 1, 25, 2, 'http://demo.f4h.com.sa/sultana/images/cp/products/HTB1adWlaLfsK1RjSszgq6yXzpXaX.jpg_350x350_.jpg', '', 0, '2020-09-08 09:01:12'),
(3, 'عطر Joy Dior', 'عطر Joy Dior', '149301951890', 200, 300, 1, 250.6, 1, 'http://demo.f4h.com.sa/sultana/images/cp/products/4460791-1230744024.jpg', '', 0, '2020-09-08 09:02:15'),
(4, 'عطر الورود', 'Rose perfumes', '521558286705', 400, 500, 1, 450, 1, 'http://demo.f4h.com.sa/sultana/images/cp/products/9999026109.jpg', '', 0, '2020-09-08 09:03:13'),
(5, 'عطر الذهبي', 'Golden Perfuim', '945767728896', 500, 10, 1, 9, 1, 'http://demo.f4h.com.sa/sultana/images/cp/products/emirates-voiceghgfdf.gif', '', 0, '2020-09-08 09:05:03'),
(6, 'كريك', 'كريك', '544485425584', 50, 10, 1, 9, 3, 'http://demo.f4h.com.sa/sultana/images/cp/products/YT-8865--1_776x591.jpg', '', 0, '2020-09-08 09:06:03'),
(7, 'برويطة', 'برويطة', '215665963906', 250, 300, 1, 275, 3, 'http://demo.f4h.com.sa/sultana/images/cp/products/bdvth3.jpg', '', 0, '2020-09-08 09:07:37'),
(8, 'ماء اكوافينا', 'water1', '012000014383', 0, 0, 1, 0, 1, '', '', 0, '2020-10-28 20:41:00'),
(9, 'ماء اكوافينا 1', 'water2', '6287008660014', 10, 20, 1, 15, 1, '', '', 0, '2020-10-28 20:46:23'),
(10, 'تجربة 5', 'test5', '102263932p', 100, 200, 1, 150, 2, '', '', 0, '2020-10-28 20:48:17'),
(11, 'مربع', 'Murabe3', '3850102323036', 5, 10, 1, 8, 3, '', '', 0, '2020-10-28 21:06:54'),
(12, 'عطر حسن', 'hasssan', '024300044168', 20, 40, 1, 30, 1, '', '', 0, '2020-11-01 15:07:29'),
(13, 'محمد', 'hasssan5', '6287008590090', 5, 8, 1, 6, 1, '', '', 0, '2020-11-01 15:40:07'),
(14, 'امنتي', 'AIMANTE', '6390902022861', 0, 0, 1, 0, 1, '', '', 0, '2020-11-19 17:45:36'),
(15, 'سيارة', 'car', 'jghdghkjdhgdhg', 35, 345, 1, 345, 3, '', '', 0, '2020-11-24 14:43:16'),
(16, 'sajed', '', '411458313199', 20, 30, 1, 10, 1, '', '', 0, '2021-01-21 17:41:48'),
(17, 'dggdfhfg', '', '986386337208', 0, 8, 1, 9, 1, '', '', 0, '2021-01-21 17:52:33'),
(18, 'e', 'we', '600806512854', 0, 10, 1, 10, 3, '', '', 0, '2021-01-21 17:53:14'),
(19, 'تجربة', 'test', '897396663941', 90, 120, 1, 100, 1, 'http://demo.f4h.com.sa/sultana/images/cp/products/س.png', 'vc', 0, '2021-02-05 08:55:44'),
(20, '5تي اش', '5th avenue', '6290845151577', 100, 191.3, 1, 200, 1, '', 'تكيز80% تاريخ الانتاج2019/05/1', 0, '2021-02-21 13:40:32'),
(21, 'هاي كلاس نوير', 'High Class noir', '6281074713957', 50, 130.43, 1, 100, 1, '', '', 0, '2021-02-21 13:48:14'),
(22, 'عرض افتتاح القارة', '', '564609899590', 0, 52.17, 1, 1, 4, '', '', 0, '2021-02-21 13:55:46'),
(23, 'فلورز', 'flowers', '6287006662294', 60, 134.78, 1, 70, 1, '', '', 0, '2021-03-01 14:16:14'),
(24, 'تجربة 60', 'test60', '339644344406', 10, 52.17, 1, 1.5, 1, '', '', 0, '2021-03-03 11:10:15'),
(25, 'ee', 'ee', '510750131949', 50, 90, 1, 60, 1, '', '', 0, '2021-03-03 11:13:59'),
(26, 'ايس', 'ice', '6287006662218', 5, 147.83, 1, 9, 6, '', '', 0, '2021-04-05 18:50:29');

-- --------------------------------------------------------

--
-- Table structure for table `products_deps`
--

CREATE TABLE `products_deps` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `photo` text NOT NULL,
  `content` text,
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products_deps`
--

INSERT INTO `products_deps` (`id`, `title`, `photo`, `content`, `date_add`) VALUES
(1, 'العطور الشرقية', 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/15772836555800.jpg', 'العطور الشرقية', '2020-09-08 08:55:18'),
(2, 'مستلزمات الحاسب الألي', 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/hqdefault.jpg', 'مستلزمات الحاسب الألي', '2020-09-08 08:57:07'),
(3, 'الحديقة', 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/720181413512197617633.jpg', 'الحديقة', '2020-09-08 08:58:43'),
(4, 'test', 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/Picture1.jpg', '', '2021-02-05 09:12:44'),
(5, '6298040400452', '', 'brown leather', '2021-02-28 18:12:29'),
(6, 'فرنسية', '', '', '2021-04-05 18:49:45');

-- --------------------------------------------------------

--
-- Table structure for table `purchases_receipt`
--

CREATE TABLE `purchases_receipt` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `supplier_receipt_number` varchar(300) DEFAULT NULL,
  `byuser` float NOT NULL,
  `sum_productsquantity` float NOT NULL,
  `tax` float DEFAULT NULL,
  `sum_productspricetax` float DEFAULT NULL,
  `sum_productspricewithouttax` float DEFAULT NULL,
  `receipt_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `receipt_date_back` datetime DEFAULT NULL,
  `receipt_state` int(11) DEFAULT NULL,
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `purchases_receipt`
--

INSERT INTO `purchases_receipt` (`id`, `supplier_id`, `supplier_receipt_number`, `byuser`, `sum_productsquantity`, `tax`, `sum_productspricetax`, `sum_productspricewithouttax`, `receipt_date`, `receipt_date_back`, `receipt_state`, `comment`) VALUES
(6, 1, NULL, 9, 1000, 3750, 28750, 25000, '2020-11-19 18:10:49', NULL, 1, '0'),
(7, 2, NULL, 9, 20, 150, 1150, 1000, '2021-01-07 23:40:30', NULL, 1, '0'),
(8, 3, NULL, 9, 3, 18, 138, 120, '2021-01-11 19:52:24', NULL, 0, '0'),
(9, 4, NULL, 9, 4, 0, 700, 700, '2021-01-11 21:42:36', NULL, 0, '0'),
(10, 3, NULL, 9, 6, 150, 1150, 1000, '2021-01-11 21:46:46', NULL, 0, '0'),
(11, 3, NULL, 9, 23, 474, 3634, 3160, '2021-01-16 13:23:28', NULL, 0, '0'),
(12, 3, NULL, 9, 23, 474, 3634, 3160, '2021-01-16 13:28:52', NULL, 0, '0'),
(13, 3, NULL, 9, 23, 474, 3634, 3160, '2021-01-16 13:38:50', NULL, 0, '0'),
(14, 3, NULL, 9, 23, 474, 3634, 3160, '2021-01-16 13:39:45', NULL, 1, '0'),
(15, 3, NULL, 9, 23, 474, 3634, 3160, '2021-01-16 13:39:53', NULL, 0, '0'),
(16, 3, NULL, 9, 23, 474, 3634, 3160, '2021-01-16 13:40:18', NULL, 0, '0'),
(17, 3, NULL, 9, 23, 474, 3634, 3160, '2021-01-16 13:40:40', NULL, 0, '0'),
(18, 2, NULL, 9, 5, 7.5, 57.5, 50, '2021-01-16 13:44:13', NULL, 0, '0'),
(19, 4, NULL, 9, 19, 21, 161, 140, '2021-01-16 13:45:07', NULL, 0, '0'),
(20, 2, NULL, 9, 40, 42, 322, 280, '2021-01-16 13:46:47', NULL, 0, '0'),
(21, 4, NULL, 9, 3, 2.25, 17.25, 15, '2021-01-16 13:47:58', NULL, 0, '0'),
(22, 2, NULL, 9, 5, 3, 23, 20, '2021-01-16 13:50:57', NULL, 0, '0'),
(23, 1, NULL, 9, 16, 12.6, 96.6, 84, '2021-01-16 13:54:19', NULL, 0, '0'),
(24, 3, NULL, 9, 5, 4.65, 35.65, 31, '2021-01-16 14:02:32', NULL, 0, '0'),
(25, 3, NULL, 9, 11, 7.65, 58.65, 51, '2021-01-16 14:04:04', NULL, 0, '0'),
(26, 3, NULL, 9, 5, 6, 46, 40, '2021-01-20 14:34:02', NULL, 0, '0'),
(27, 4, NULL, 9, 30, 18, 138, 120, '2021-01-20 14:34:34', NULL, 1, '0'),
(28, 4, NULL, 9, 6, 1.8, 13.8, 12, '2021-01-21 01:51:21', NULL, 1, '0'),
(29, 2, NULL, 9, 5, 1.5, 11.5, 10, '2021-01-22 00:44:09', NULL, 0, '0'),
(30, 2, NULL, 9, 4, 15, 115, 100, '2021-01-22 00:45:16', NULL, 0, '0'),
(31, 2, NULL, 9, 4, 0, 100, 100, '2021-01-22 00:45:50', NULL, 1, '0'),
(32, 5, NULL, 9, 200, 3000, 23000, 20000, '2021-02-21 13:42:54', NULL, 0, '0'),
(33, 5, NULL, 9, 1000, 7500, 57500, 50000, '2021-02-21 13:49:24', NULL, 0, '0'),
(34, 5, NULL, 9, 10000, 0, 10000, 10000, '2021-02-21 13:57:37', NULL, 0, '0'),
(35, 6, NULL, 9, 1000, 7500, 57500, 50000, '2021-03-01 14:21:59', NULL, 0, '0'),
(36, 6, NULL, 9, 50, 7.5, 57.5, 50, '2021-03-03 11:25:09', NULL, 0, '0'),
(37, 6, NULL, 9, 1, 7.5, 57.5, 50, '2021-03-03 11:27:06', NULL, 0, '0'),
(38, 6, NULL, 9, 50, 375, 2875, 2500, '2021-03-03 11:30:28', NULL, 0, '0'),
(39, 6, NULL, 9, 4, 30, 230, 200, '2021-03-03 11:31:33', NULL, 0, '0'),
(40, 3, NULL, 9, 1, 7.5, 57.5, 50, '2021-03-18 10:16:51', NULL, 0, '0'),
(41, 7, NULL, 9, 7, 93, 713, 620, '2021-03-24 20:05:37', NULL, 1, '0'),
(42, 3, NULL, 9, 13, 3.9, 29.9, 26, '2021-03-27 14:27:34', NULL, 0, '0'),
(43, 2, NULL, 9, 1000, 7500, 57500, 50000, '2021-03-31 06:27:26', NULL, 0, '0'),
(44, 5, NULL, 9, 10, 75, 575, 500, '2021-03-31 06:36:53', NULL, 0, '0'),
(45, 4, NULL, 9, 15, 135, 1035, 900, '2021-03-31 17:51:18', NULL, 0, '0'),
(46, 1, NULL, 9, 5, 37.5, 287.5, 250, '2021-03-31 17:59:58', NULL, 0, '0'),
(47, 5, NULL, 9, 1200, 18000, 138000, 120000, '2021-04-05 18:51:48', NULL, 1, '0'),
(48, 5, NULL, 9, 1000, 15000, 115000, 100000, '2021-04-05 19:00:08', '2021-05-18 11:18:49', 1, '0'),
(49, 1, NULL, 9, 120, 900, 6900, 6000, '2021-04-06 10:12:58', NULL, 0, '0'),
(50, 7, '45456', 9, 1, 1.5, 11.5, 10, '2021-04-06 10:41:39', NULL, 0, '0'),
(51, 6, '123321', 9, 300, 3750, 28750, 25000, '2021-04-10 18:11:49', NULL, 0, '0'),
(52, 6, '123321', 9, 300, 3750, 28750, 25000, '2021-04-10 18:12:48', NULL, 0, '0'),
(53, 6, '123321', 9, 300, 3750, 28750, 25000, '2021-04-10 18:16:02', NULL, 0, '0'),
(54, 6, '123321', 9, 300, 3750, 28750, 25000, '2021-04-10 18:16:25', NULL, 1, '0'),
(55, 1, '', 9, 5, 1.59, 12.16, 10.58, '2021-04-11 19:44:58', NULL, 0, '0'),
(56, 1, '', 9, 6, 1.73, 13.27, 11.54, '2021-04-11 19:49:41', NULL, 0, '0'),
(57, 1, '', 9, 6, 1.73, 13.27, 11.54, '2021-04-11 19:56:13', NULL, 0, '0'),
(58, 1, '', 9, 3, 1.17, 8.94, 7.77, '2021-04-11 19:57:10', '2021-04-18 20:08:23', 1, '0'),
(59, 1, '', 9, 5, 1.83, 14.04, 12.21, '2021-04-11 20:05:05', NULL, 0, '0'),
(60, 2, '', 9, 30, 375, 2875, 2500, '2021-04-18 19:47:59', NULL, 1, '0'),
(61, 2, '', 9, 150, 600, 4600, 4000, '2021-04-18 20:11:26', '2021-04-18 20:13:05', 1, '0'),
(62, 2, '', 9, 100, 0, 5000, 5000, '2021-04-28 03:46:53', NULL, 0, '0'),
(63, 7, '123456', 9, 100, 750, 5750, 5000, '2021-04-28 03:58:41', '2021-04-28 04:00:04', 1, '0'),
(64, 8, '56989', 9, 1000, 1575, 12075, 10500, '2021-05-06 19:03:29', NULL, 0, '0'),
(65, 1, '', 9, 100, 150, 1150, 1000, '2021-05-18 08:02:36', NULL, 0, '0'),
(66, 1, '', 9, 10, 15, 115, 100, '2021-05-18 10:03:24', NULL, 0, '0'),
(67, 3, '', 9, 6, 2.7, 20.7, 18, '2021-06-09 08:01:43', '2021-06-09 08:02:07', 1, '0'),
(68, 2, '', 9, 2000, 1500, 11500, 10000, '2021-07-27 19:56:55', NULL, 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `purchases_receiptdetails`
--

CREATE TABLE `purchases_receiptdetails` (
  `id` int(11) NOT NULL,
  `receipt_id` int(11) NOT NULL,
  `product_id` text NOT NULL,
  `product_name` text,
  `product_price` float NOT NULL,
  `product_quantity` float NOT NULL,
  `tax` float NOT NULL,
  `tax_description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `purchases_receiptdetails`
--

INSERT INTO `purchases_receiptdetails` (`id`, `receipt_id`, `product_id`, `product_name`, `product_price`, `product_quantity`, `tax`, `tax_description`) VALUES
(1, 1, '1', 'لوحة مفاتيح', 50, 10, 125, NULL),
(2, 1, 'no_product2', 'أوراق A4', 20, 1, 5, NULL),
(3, 1, 'no_product3', 'أقلام', 10, 2, 5, NULL),
(4, 2, 'no_product1', 'أثاث', 5000, 1, 1250, NULL),
(5, 3, 'no_product1', 'مرحبا', 10, 2, 3, NULL),
(6, 4, '3', 'عطر Joy Dior', 1, 2, 0.3, NULL),
(7, 5, '14', 'امنتي', 25, 1000, 3750, NULL),
(8, 6, '14', 'امنتي', 25, 1000, 3750, NULL),
(9, 7, '2', 'ماوس', 50, 20, 150, NULL),
(10, 8, 'no_product1', 'paper', 40, 3, 18, NULL),
(11, 9, '3', 'عطر Joy Dior', 100, 1, 0, NULL),
(12, 9, '4', 'عطر الورود', 200, 3, 0, NULL),
(13, 10, '2', 'ماوس', 100, 1, 15, NULL),
(14, 10, 'no_product1', 'صثقف', 200, 2, 60, NULL),
(15, 10, '4', 'عطر الورود', 100, 1, 15, NULL),
(16, 10, 'no_product2', 'ورق', 200, 2, 60, NULL),
(17, 11, '2', 'ماوس', 200, 13, 390, NULL),
(18, 12, '2', 'ماوس', 200, 13, 390, NULL),
(19, 13, '2', 'ماوس', 200, 13, 390, NULL),
(20, 14, '2', 'ماوس', 200, 13, 390, NULL),
(21, 15, '2', 'ماوس', 200, 13, 390, NULL),
(22, 16, '2', 'ماوس', 200, 13, 390, NULL),
(23, 17, '2', 'ماوس', 200, 13, 390, NULL),
(24, 17, '3', 'عطر Joy Dior', 56, 10, 84, NULL),
(25, 18, '2', 'ماوس', 10, 5, 7.5, NULL),
(26, 19, '2', 'ماوس', 7, 12, 12.6, NULL),
(27, 19, '3', 'عطر Joy Dior', 8, 7, 8.4, NULL),
(28, 20, '3', 'عطر Joy Dior', 7, 40, 42, NULL),
(29, 21, '2', 'ماوس', 5, 3, 2.25, NULL),
(30, 22, '3', 'عطر Joy Dior', 4, 5, 3, NULL),
(31, 23, '2', 'ماوس', 5, 14, 10.5, NULL),
(32, 23, '3', 'عطر Joy Dior', 7, 2, 2.1, NULL),
(33, 24, '2', 'ماوس', 5, 3, 2.25, NULL),
(34, 24, '3', 'عطر Joy Dior', 8, 2, 2.4, NULL),
(35, 25, '3', 'عطر Joy Dior', 1, 1, 0.15, NULL),
(36, 25, '2', 'ماوس', 5, 10, 7.5, NULL),
(37, 26, '1', 'لوحة مفاتيح', 8, 5, 6, NULL),
(38, 27, '3', 'عطر Joy Dior', 4, 30, 18, NULL),
(39, 28, '1', 'لوحة مفاتيح', 2, 6, 1.8, NULL),
(40, 29, '1', 'لوحة مفاتيح', 2, 5, 1.5, NULL),
(41, 30, '1', 'لوحة مفاتيح', 25, 4, 15, NULL),
(42, 31, '1', 'لوحة مفاتيح', 25, 4, 0, NULL),
(43, 32, 'no_product1', '5تي اش', 100, 200, 3000, NULL),
(44, 33, '21', 'هاي كلاس نوير', 50, 1000, 7500, NULL),
(45, 34, '22', 'عرض افتتاح القارة', 1, 10000, 0, NULL),
(46, 35, '23', 'فلورز', 50, 1000, 7500, NULL),
(47, 36, '1', 'لوحة مفاتيح', 1, 50, 7.5, NULL),
(48, 37, '3', 'عطر Joy Dior', 50, 1, 7.5, NULL),
(49, 38, '3', 'عطر Joy Dior', 50, 50, 375, NULL),
(50, 39, '3', 'عطر Joy Dior', 50, 4, 30, NULL),
(51, 40, '1', 'لوحة مفاتيح', 50, 1, 7.5, NULL),
(52, 41, '1', 'لوحة مفاتيح', 60, 2, 18, NULL),
(53, 41, '3', 'عطر Joy Dior', 100, 5, 75, NULL),
(54, 42, '2', 'ماوس', 2, 13, 3.9, NULL),
(55, 43, '4', 'عطر الورود', 50, 1000, 7500, NULL),
(56, 44, '1', 'لوحة مفاتيح', 50, 10, 75, NULL),
(57, 45, '2', 'ماوس', 60, 15, 135, NULL),
(58, 46, '2', 'ماوس', 50, 5, 37.5, NULL),
(59, 47, '26', 'ايس', 100, 1200, 18000, NULL),
(60, 48, '26', 'ايس', 100, 1000, 15000, NULL),
(61, 49, '1', 'لوحة مفاتيح', 50, 120, 900, NULL),
(62, 50, '1', 'لوحة مفاتيح', 10, 1, 1.5, NULL),
(63, 51, '2', 'ماوس', 50, 100, 750, NULL),
(64, 51, '4', 'عطر الورود', 100, 200, 3000, NULL),
(65, 52, '2', 'ماوس', 50, 100, 750, NULL),
(66, 52, '4', 'عطر الورود', 100, 200, 3000, NULL),
(67, 53, '2', 'ماوس', 50, 100, 750, NULL),
(68, 53, '4', 'عطر الورود', 100, 200, 3000, NULL),
(69, 54, '2', 'ماوس', 50, 100, 750, NULL),
(70, 54, '4', 'عطر الورود', 100, 200, 3000, NULL),
(71, 55, '1', 'لوحة مفاتيح', 2.123, 2, 0.64, NULL),
(72, 55, '3', 'عطر Joy Dior', 2.111, 3, 0.95, NULL),
(73, 56, '3', 'عطر Joy Dior', 1.11, 2, 0.33, NULL),
(74, 56, '1', 'لوحة مفاتيح', 2.33, 4, 1.4, NULL),
(75, 57, '3', 'عطر Joy Dior', 1.11, 2, 0.33, NULL),
(76, 57, '1', 'لوحة مفاتيح', 2.33, 4, 1.4, NULL),
(77, 58, '1', 'لوحة مفاتيح', 3.33, 2, 1, NULL),
(78, 58, '2', 'ماوس', 1.11, 1, 0.17, NULL),
(79, 59, 'no_product1', 'fg', 1.11, 2, 0.33, NULL),
(80, 59, 'no_product2', 'dfg', 3.33, 3, 1.5, NULL),
(81, 60, '2', 'ماوس', 50, 10, 75, NULL),
(82, 60, '3', 'عطر Joy Dior', 100, 20, 300, NULL),
(83, 61, '7', 'برويطة', 20, 50, 150, NULL),
(84, 61, '8', 'ماء اكوافينا', 30, 100, 450, NULL),
(85, 62, '4', 'عطر الورود', 50, 100, 0, NULL),
(86, 63, '1', 'لوحة مفاتيح', 50, 100, 750, NULL),
(87, 64, '1', 'لوحة مفاتيح', 10.5, 1000, 1575, NULL),
(88, 65, '2', 'ماوس', 10, 100, 150, NULL),
(89, 66, '2', 'ماوس', 10, 10, 15, NULL),
(90, 67, '2', 'ماوس', 3, 6, 2.7, NULL),
(91, 68, '3', 'عطر Joy Dior', 5, 2000, 1500, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `quantity_request`
--

CREATE TABLE `quantity_request` (
  `id` int(11) NOT NULL,
  `from_branch_id` int(11) NOT NULL,
  `to_branch_id` int(11) DEFAULT NULL,
  `byuser` float NOT NULL,
  `request_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `state` varchar(200) DEFAULT NULL,
  `decrease_quantity_state` int(11) NOT NULL DEFAULT '0',
  `comment` text,
  `notification_check` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quantity_request`
--

INSERT INTO `quantity_request` (`id`, `from_branch_id`, `to_branch_id`, `byuser`, `request_date`, `state`, `decrease_quantity_state`, `comment`, `notification_check`) VALUES
(1, 3, 7, 9, '2020-09-08 14:29:09', '4', 1, '0', 0),
(2, 3, 7, 9, '2020-09-08 14:33:07', '3', 1, '0', 0),
(3, 3, 6, 9, '2020-09-08 14:35:11', '3', 1, '0', 0),
(4, 3, 6, 9, '2020-11-01 15:43:13', '1', 0, '0', 0),
(5, 3, 6, 9, '2020-11-01 15:44:56', '5', 0, '0', 0),
(6, 3, 6, 9, '2020-11-01 16:02:08', '3', 1, '0', 0),
(7, 3, 6, 9, '2020-11-15 13:16:23', '1', 0, '0', 0),
(8, 3, 7, 9, '2020-11-15 13:17:41', '1', 0, '0', 0),
(9, 3, 6, 9, '2020-11-19 18:13:20', '1', 0, '0', 0),
(10, 3, 6, 9, '2020-11-21 05:57:04', '1', 0, '0', 0),
(11, 6, 7, 11, '2020-11-23 20:45:03', '1', 0, '0', 0),
(12, 6, 3, 11, '2020-11-23 20:55:38', '1', 0, '0', 0),
(13, 7, 3, 11, '2020-11-23 20:56:31', '1', 0, '0', 0),
(14, 6, 7, 11, '2020-11-23 21:04:24', '1', 0, '0', 0),
(15, 6, 7, 11, '2020-11-23 21:06:44', '6', 0, '0', 0),
(16, 3, 7, 9, '2020-11-30 16:31:02', '1', 0, '0', 1),
(17, 3, 7, 9, '2020-11-30 16:34:07', '1', 0, '0', 1),
(18, 3, 7, 9, '2020-11-30 16:39:38', '1', 0, '0', 1),
(19, 3, 7, 9, '2020-11-30 18:04:55', '1', 0, '0', 1),
(20, 3, 6, 9, '2021-01-20 14:38:55', '4', 0, '0', 1),
(21, 6, 3, 11, '2021-01-20 14:42:54', '4', 0, '0', 1),
(22, 6, 7, 11, '2021-01-20 15:34:25', '4', 1, '0', 1),
(23, 7, 6, 12, '2021-01-20 15:56:49', '4', 0, '0', 1),
(24, 3, 6, 9, '2021-01-22 00:03:15', '1', 0, '0', 1),
(25, 3, 6, 9, '2021-01-22 00:05:03', '1', 0, '0', 1),
(26, 3, 6, 9, '2021-01-22 00:06:28', '1', 0, '0', 1),
(27, 3, 6, 9, '2021-01-22 00:20:47', '1', 0, '0', 1),
(28, 3, 6, 9, '2021-01-22 00:24:34', '1', 0, '0', 1),
(29, 3, 6, 9, '2021-01-22 00:28:07', '1', 0, '0', 1),
(30, 3, 6, 9, '2021-01-22 00:28:22', '1', 0, '0', 1),
(31, 3, 7, 9, '2021-01-22 00:32:11', '4', 0, '0', 1),
(32, 3, 6, 9, '2021-01-28 14:44:57', '1', 0, '0', 1),
(33, 6, 3, 9, '2021-01-28 14:45:41', '2', 0, '0', 1),
(34, 6, 3, 9, '2021-01-28 15:00:58', '2', 0, '0', 1),
(35, 3, 6, 9, '2021-01-28 15:05:12', '1', 0, '0', 1),
(36, 3, 9, 9, '2021-03-03 10:43:30', '1', 0, '0', 0),
(37, 3, 6, 9, '2021-03-03 12:52:57', '1', 0, '0', 1),
(38, 9, 3, 15, '2021-03-15 17:05:51', '2', 0, '0', 1),
(39, 3, 6, 9, '2021-03-18 10:19:54', '1', 0, '0', 1),
(40, 3, 7, 9, '2021-03-30 23:42:37', '1', 0, '0', 1),
(41, 3, 6, 9, '2021-03-31 06:31:06', '3', 1, '0', 1),
(42, 3, 6, 9, '2021-03-31 17:55:29', '3', 1, '0', 1),
(43, 6, 3, 9, '2021-04-06 10:30:14', '2', 0, '0', 1),
(44, 3, 6, 9, '2021-04-06 10:32:21', '1', 0, '0', 1),
(45, 3, 6, 9, '2021-04-06 10:33:28', '1', 0, '0', 1),
(73, 3, 6, 9, '2021-04-11 18:41:34', '1', 0, '0', 1),
(74, 7, 3, 9, '2021-04-28 03:47:53', '4', 1, '0', 1),
(75, 9, 3, 15, '2021-05-06 19:08:30', '4', 1, '0', 1),
(76, 3, 9, 9, '2021-05-06 19:19:34', '2', 0, '0', 0),
(77, 3, 9, 9, '2021-05-06 19:22:58', '2', 0, '0', 0),
(78, 3, 9, 9, '2021-05-19 11:46:54', '2', 0, '0', 0),
(79, 3, 9, 9, '2021-05-19 11:51:18', '2', 0, '0', 0),
(80, 9, 3, 15, '2021-05-19 11:53:58', '2', 0, '0', 1),
(81, 6, 7, 11, '2021-05-19 20:56:58', '4', 1, '0', 1),
(82, 9, 3, 9, '2021-05-20 11:36:33', '4', 1, '0', 1),
(83, 9, 3, 15, '2021-05-20 11:39:07', '4', 1, '0', 1),
(84, 9, 3, 15, '2021-05-20 11:43:58', '4', 1, '0', 1),
(85, 9, 3, 9, '2021-05-20 11:53:40', '5', 0, '0', 1),
(86, 3, 7, 9, '2021-05-22 07:08:59', '1', 0, '0', 1),
(87, 3, 6, 9, '2021-06-09 08:08:38', '4', 1, '0', 1),
(88, 9, 3, 9, '2021-06-26 14:31:31', '4', 1, '0', 1),
(89, 9, 3, 15, '2021-06-26 14:57:41', '6', 0, '0', 1),
(90, 9, 3, 15, '2021-06-26 14:58:31', '4', 1, '0', 1),
(91, 9, 3, 15, '2021-06-26 15:03:11', '4', 1, '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `quantity_requestdetails`
--

CREATE TABLE `quantity_requestdetails` (
  `id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` text,
  `quantity_request` float NOT NULL,
  `state` varchar(200) DEFAULT NULL,
  `quantity_request_pricee` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quantity_requestdetails`
--

INSERT INTO `quantity_requestdetails` (`id`, `request_id`, `product_id`, `product_name`, `quantity_request`, `state`, `quantity_request_pricee`) VALUES
(1, 1, 1, 'لوحة مفاتيح', 10, '1', 0),
(2, 2, 2, 'ماوس', 16, '1', 0),
(3, 3, 3, 'عطر Joy Dior', 15, '1', 0),
(4, 5, 12, 'عطر حسن', 1, '1', 0),
(5, 6, 1, 'لوحة مفاتيح', 5, '1', 0),
(6, 7, 3, 'عطر Joy Dior', 10, '0', 0),
(7, 8, 4, 'عطر الورود', 10, '0', 0),
(8, 8, 3, 'عطر Joy Dior', 10, '0', 0),
(9, 9, 14, 'امنتي', 1000, '0', 0),
(10, 10, 1, 'لوحة مفاتيح', 6, '0', 0),
(11, 11, 9, 'ماء اكوافينا 1', 0, '0', 0),
(12, 11, 8, 'ماء اكوافينا', 0, '0', 0),
(13, 12, 8, 'ماء اكوافينا', 0, '0', 0),
(14, 12, 9, 'ماء اكوافينا 1', 0, '0', 0),
(15, 13, 8, 'ماء اكوافينا', 0, '0', 0),
(16, 13, 9, 'ماء اكوافينا 1', 0, '0', 0),
(17, 14, 1, 'لوحة مفاتيح', 7, '0', 0),
(18, 14, 4, 'عطر الورود', 66, '0', 0),
(19, 15, 2, 'ماوس', 9, '0', 67),
(20, 15, 4, 'عطر الورود', 6, '0', 0),
(21, 16, 1, 'لوحة مفاتيح', 7, '0', 0),
(22, 16, 3, 'عطر Joy Dior', 2, '0', 0),
(23, 17, 2, 'ماوس', 2, '0', 0),
(24, 17, 1, 'لوحة مفاتيح', 3, '0', 0),
(25, 18, 4, 'عطر الورود', 5, '0', 450),
(26, 19, 2, 'ماوس', 2, '0', 50),
(27, 19, 3, 'عطر Joy Dior', 3, '0', 750),
(28, 20, 4, 'عطر الورود', 6, '1', 2700),
(29, 21, 1, 'لوحة مفاتيح', 2, '1', 50),
(30, 22, 2, 'ماوس', 3, '1', 75),
(31, 22, 4, 'عطر الورود', 4, '1', 1800),
(32, 23, 2, 'ماوس', 6, '1', 150),
(33, 31, 1, 'لوحة مفاتيح', 10, '1', 1150),
(34, 31, 2, 'ماوس', 8, '1', 230),
(35, 32, 1, 'لوحة مفاتيح', 30, '0', 3000),
(36, 33, 1, 'لوحة مفاتيح', 10, '0', 1000),
(37, 34, 3, 'عطر Joy Dior', 4, '1', 1150),
(38, 34, 2, 'ماوس', 4, '1', 115),
(39, 35, 6, 'كريك', 5, '0', 402.5),
(40, 35, 9, 'ماء اكوافينا 1', 5, '0', 86.25),
(41, 36, 1, 'لوحة مفاتيح', 10, '0', 14.03),
(42, 37, 3, 'عطر Joy Dior', 9, '0', 2255.4),
(43, 38, 21, 'هاي كلاس نوير', 5, '1', 500),
(44, 39, 1, 'لوحة مفاتيح', 65, '0', 91.195),
(45, 40, 1, 'لوحة مفاتيح', 10, '0', 1150),
(46, 41, 5, 'عطر الذهبي', 15, '1', 9487.5),
(47, 41, 6, 'كريك', 5, '1', 402.5),
(48, 42, 1, 'لوحة مفاتيح', 10, '1', 1150),
(49, 42, 2, 'ماوس', 20, '1', 575),
(50, 43, 1, 'لوحة مفاتيح', 19, '1', 2185),
(51, 44, 1, 'لوحة مفاتيح', 10, '0', 1150),
(52, 45, 1, 'لوحة مفاتيح', 50, '0', 5750),
(82, 73, 1, 'لوحة مفاتيح', 2, '0', 230),
(83, 74, 4, 'عطر الورود', 100, '1', 51750),
(84, 75, 20, '5تي اش', 100, '0', 20000),
(85, 76, 1, 'لوحة مفاتيح', 50, '1', 5000),
(86, 77, 1, 'لوحة مفاتيح', 2, '1', 200),
(87, 78, 1, 'لوحة مفاتيح', 50, '1', 5750),
(88, 79, 1, 'لوحة مفاتيح', 100, '1', 11500),
(89, 80, 1, 'لوحة مفاتيح', 100, '1', 11500),
(90, 81, 3, 'عطر Joy Dior', 5, '1', 1440.95),
(91, 82, 3, 'عطر Joy Dior', 8, '1', 2004.8),
(92, 83, 3, 'عطر Joy Dior', 10, '0', 2506),
(93, 84, 3, 'عطر Joy Dior', 20, '1', 5012),
(94, 85, 3, 'عطر Joy Dior', 5, '1', 1253),
(95, 86, 2, 'ماوس', 1, '0', 28.75),
(96, 87, 2, 'ماوس', 5, '1', 143.75),
(97, 88, 1, 'لوحة مفاتيح', 20, '1', 2000),
(98, 89, 1, 'لوحة مفاتيح', 28, '0', 3220),
(99, 90, 1, 'لوحة مفاتيح', 28, '1', 2800),
(100, 91, 1, 'لوحة مفاتيح', 100, '1', 11500);

-- --------------------------------------------------------

--
-- Table structure for table `quantity_request_state`
--

CREATE TABLE `quantity_request_state` (
  `id` int(11) NOT NULL,
  `state` varchar(200) DEFAULT NULL,
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quantity_request_state`
--

INSERT INTO `quantity_request_state` (`id`, `state`, `comment`) VALUES
(1, 'قيد الانتظار', NULL),
(2, 'تمت الموافقة', NULL),
(3, 'تم الشحن', NULL),
(4, 'تم استلام الكمية', NULL),
(5, 'رفض الطلب', NULL),
(6, 'الغاء الطلب', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `rate` float NOT NULL,
  `name` text NOT NULL,
  `comment` text NOT NULL,
  `date_now` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`id`, `store_id`, `rate`, `name`, `comment`, `date_now`) VALUES
(1, 32, 4.5, 'طارق مندور', 'هذا النص يمكن ان يستبدل', '2020-03-05 14:48:12'),
(3, 32, 2, 'kimo kimo', 'بلا بلا بل', '2020-06-08 15:18:43'),
(4, 32, 4.5, 'kimo kimo', 'بلا بلا بل', '2020-06-08 15:24:46');

-- --------------------------------------------------------

--
-- Table structure for table `sales_receipt`
--

CREATE TABLE `sales_receipt` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `byuser` float NOT NULL,
  `branch_id` int(11) NOT NULL,
  `sum_productsquantity` float NOT NULL,
  `tax` float DEFAULT NULL,
  `sum_productspricetax` float DEFAULT NULL,
  `sum_productspricewithouttax` float DEFAULT NULL,
  `receipt_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `receipt_date_back` datetime DEFAULT NULL,
  `payment_method` int(11) NOT NULL DEFAULT '0',
  `cash_payment_method` float NOT NULL DEFAULT '0',
  `card_payment_method` float NOT NULL DEFAULT '0',
  `receipt_barcode` text,
  `occasion_notes_receipt` text,
  `state` varchar(150) DEFAULT '1',
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sales_receipt`
--

INSERT INTO `sales_receipt` (`id`, `client_id`, `byuser`, `branch_id`, `sum_productsquantity`, `tax`, `sum_productspricetax`, `sum_productspricewithouttax`, `receipt_date`, `receipt_date_back`, `payment_method`, `cash_payment_method`, `card_payment_method`, `receipt_barcode`, `occasion_notes_receipt`, `state`, `comment`) VALUES
(1, 7, 9, 3, 2, 50, 250, 200, '2020-09-08 09:37:26', '2021-04-18 20:30:18', 0, 0, 0, NULL, NULL, '3', '0'),
(2, 8, 9, 3, 3, 22.5, 112.5, 90, '2020-09-08 09:41:01', NULL, 0, 0, 0, NULL, NULL, '3', '0'),
(3, 7, 9, 3, 6, 450, 2250, 1800, '2020-09-08 09:52:35', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(4, 8, 9, 3, 4, 127.5, 637.5, 510, '2020-09-08 10:26:59', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(5, 8, 9, 3, 5, 127.5, 637.5, 510, '2020-09-08 10:32:11', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(6, 10, 9, 3, 1, 37.5, 187.5, 150, '2020-09-08 10:46:59', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(7, 9, 12, 7, 1, 75, 375, 300, '2020-09-08 11:31:44', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(8, 10, 12, 7, 2, 95, 475, 380, '2020-09-08 11:32:18', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(9, 8, 11, 6, 1, 7.5, 37.5, 30, '2020-09-08 11:34:00', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(10, 7, 11, 6, 1, 125, 625, 500, '2020-09-08 11:34:19', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(11, 10, 11, 6, 1, 75, 375, 300, '2020-09-08 11:34:35', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(12, 7, 9, 3, 13, 50, 250, 200, '2020-10-28 20:57:48', NULL, 1, 0, 0, NULL, NULL, '1', '0'),
(13, 7, 9, 3, 20, 100, 500, 400, '2020-10-29 00:00:06', NULL, 0, 0, 0, NULL, NULL, '1', NULL),
(14, 7, 9, 3, 1, 5, 25, 20, '2020-10-28 21:03:29', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(15, 7, 9, 3, 2, 10, 50, 40, '2020-10-29 00:01:14', NULL, 0, 0, 0, NULL, NULL, '1', NULL),
(16, 9, 9, 3, 10, 25, 125, 100, '2020-10-28 21:09:17', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(17, 7, 9, 3, 8, 27.5, 137.5, 110, '2020-10-29 00:13:47', NULL, 0, 0, 0, NULL, NULL, '1', NULL),
(18, 9, 9, 3, 5, 12.5, 62.5, 50, '2020-10-28 21:15:25', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(19, 7, 9, 3, 2, 5, 25, 20, '2020-10-29 00:14:28', NULL, 0, 0, 0, NULL, NULL, '1', NULL),
(20, 7, 9, 3, 1, 2.5, 12.5, 10, '2020-10-29 00:14:43', NULL, 0, 0, 0, NULL, NULL, '1', NULL),
(21, 8, 9, 3, 9, 1125, 5625, 4500, '2020-10-30 20:25:14', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(22, 11, 9, 3, 1, 0, 0, 0, '2020-11-01 15:34:58', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(23, 7, 9, 3, 2, 5, 25, 20, '2020-11-01 15:36:33', NULL, 0, 0, 0, NULL, NULL, '3', '0'),
(24, 13, 9, 3, 1, 75, 375, 300, '2020-11-02 06:31:48', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(25, 13, 9, 3, 1, 25, 125, 100, '2020-11-02 07:12:27', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(26, 13, 9, 3, 1, 75, 375, 300, '2020-11-02 08:19:40', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(27, 14, 9, 3, 1, 75, 375, 300, '2020-11-14 11:46:39', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(28, 14, 9, 3, 1, 150, 750, 600, '2020-11-15 13:24:55', NULL, 1, 0, 0, NULL, NULL, '1', '0'),
(29, 12, 9, 3, 5, 750, 3750, 3000, '2020-11-15 13:26:14', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(30, 15, 9, 3, 2, 200, 1000, 800, '2020-11-15 13:28:13', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(31, 15, 9, 3, 3, 225, 1125, 900, '2020-11-15 13:29:57', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(32, 7, 9, 3, 2, 50, 250, 200, '2020-11-21 05:50:04', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(33, 14, 9, 3, 1, 15, 115, 100, '2020-11-26 15:04:03', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(34, 9, 9, 3, 1, 45, 345, 300, '2021-01-07 23:39:19', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(35, 12, 9, 3, 1, 4.5, 34.5, 30, '2021-01-07 23:41:00', NULL, 0, 0, 0, NULL, NULL, '1', NULL),
(36, 12, 9, 3, 1, 15, 115, 100, '2021-01-07 23:46:18', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(37, 13, 9, 3, 1, 15, 115, 100, '2021-01-10 14:22:28', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(38, 13, 9, 3, 1, 15, 115, 100, '2021-01-10 14:26:50', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(39, 13, 9, 3, 1, 4.5, 34.5, 30, '2021-01-10 14:27:31', NULL, 0, 0, 0, NULL, NULL, '1', NULL),
(40, 14, 9, 3, 1, 4.5, 34.5, 30, '2021-01-10 21:05:28', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(41, 14, 9, 3, 1, 45, 345, 300, '2021-01-10 22:17:35', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(42, 9, 9, 3, 1, 75, 575, 500, '2021-01-10 22:19:05', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(43, 14, 9, 3, 1, 45, 345, 300, '2021-01-10 22:21:40', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(44, 13, 9, 3, 1, 4.5, 34.5, 30, '2021-01-10 22:25:48', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(45, 13, 9, 3, 1, 45, 345, 300, '2021-01-10 22:26:34', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(46, 14, 9, 3, 1, 4.5, 34.5, 30, '2021-01-10 22:27:57', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(47, 15, 9, 3, 1, 90, 690, 600, '2021-01-10 22:31:36', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(48, 14, 9, 3, 1, 90, 690, 600, '2021-01-10 22:33:31', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(49, 10, 9, 3, 1, 75, 575, 500, '2021-01-10 22:34:30', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(50, 14, 9, 3, 1, 45, 345, 300, '2021-01-10 22:40:14', NULL, 0, 0, 0, NULL, NULL, '1', '0'),
(51, 11, 9, 3, 1, 45, 345, 300, '2021-01-10 23:01:05', NULL, 0, 0, 0, NULL, NULL, '1', NULL),
(52, 12, 9, 3, 1, 4.5, 34.5, 30, '2021-01-10 23:06:05', NULL, 0, 0, 0, NULL, 'cghfghf', '1', '0'),
(53, 12, 11, 6, 1, 45, 345, 300, '2021-01-10 23:43:23', NULL, 0, 0, 0, NULL, 'العيد الوطني', '1', '0'),
(54, 11, 9, 3, 1, 4.5, 34.5, 30, '2021-01-10 23:47:16', NULL, 0, 0, 0, NULL, 'العيد الوطني', '1', '0'),
(55, 9, 9, 3, 1, 4.5, 34.5, 30, '2021-01-10 23:48:36', NULL, 0, 0, 0, NULL, 'العيد الوطني', '1', '0'),
(56, 12, 11, 6, 1, 45, 345, 300, '2021-01-10 23:50:09', NULL, 0, 0, 0, NULL, 'العيد الوطني', '1', '0'),
(57, 13, 11, 6, 1, 45, 345, 300, '2021-01-10 23:50:57', NULL, 0, 0, 0, NULL, 'العيد الوطني', '1', '0'),
(58, 8, 11, 6, 1, 45, 345, 300, '2021-01-10 23:51:24', NULL, 0, 0, 0, NULL, 'العيد الوطني', '1', '0'),
(59, 7, 11, 6, 1, 45, 345, 300, '2021-01-10 23:52:53', NULL, 0, 0, 0, NULL, 'العيد الوطني', '1', '0'),
(60, 12, 11, 6, 2, 90, 690, 600, '2021-01-10 23:54:28', NULL, 0, 0, 0, NULL, 'العيد الوطني', '3', '0'),
(61, 14, 11, 6, 4, 180, 1380, 1200, '2021-01-14 22:51:53', NULL, 0, 0, 0, NULL, 'الأضحى', '2', '0'),
(62, 13, 9, 3, 7, 315, 2415, 2100, '2021-01-11 00:04:24', NULL, 0, 0, 0, NULL, 'الأضحى', '2', '0'),
(63, 8, 9, 3, 1, 45, 345, 300, '2021-01-15 10:54:21', NULL, 0, 0, 0, NULL, 'الأضحى', '1', '0'),
(64, 11, 9, 3, 1, 4.5, 34.5, 30, '2021-01-17 00:43:26', NULL, 0, 0, 0, NULL, 'الأضحى', '1', '0'),
(65, 14, 9, 3, 1, 45, 345, 300, '2021-01-17 01:00:28', NULL, 2, 0, 0, NULL, 'الأضحى', '1', '0'),
(66, 13, 9, 3, 1, 4.5, 34.5, 30, '2021-01-17 01:29:58', NULL, 1, 0, 0, NULL, 'الأضحى', '1', NULL),
(67, 13, 9, 3, 1, 1.5, 11.5, 10, '2021-01-17 01:15:14', NULL, 1, 0, 0, NULL, 'الأضحى', '1', NULL),
(68, 13, 9, 3, 1, 45, 345, 300, '2021-01-17 01:36:23', NULL, 2, 0, 0, NULL, 'الأضحى', '1', '0'),
(69, 7, 0, 0, 1, 90, 690, 600, '2021-01-17 01:37:36', NULL, 2, 0, 0, NULL, 'الأضحى', '1', '0'),
(70, 13, 9, 3, 1, 75, 575, 500, '2021-01-17 01:39:41', NULL, 1, 0, 0, NULL, 'الأضحى', '1', '0'),
(71, 15, 9, 3, 1, 75, 575, 500, '2021-01-17 01:38:55', NULL, 1, 0, 0, NULL, 'الأضحى', '1', NULL),
(72, 13, 9, 3, 2, 28.5, 218.5, 190, '2021-01-17 01:43:44', NULL, 2, 40, 20, NULL, 'الأضحى', '1', '0'),
(73, 13, 9, 3, 1, 15, 115, 100, '2021-01-17 01:45:49', NULL, 2, 3, 1, NULL, 'الأضحى', '1', '0'),
(74, 13, 9, 3, 1, 75, 575, 500, '2021-01-17 01:48:19', NULL, 2, 9, 8, NULL, 'الأضحى', '1', '0'),
(75, 9, 9, 3, 1, 45, 345, 300, '2021-01-17 02:15:56', NULL, 2, 0, 0, NULL, 'الأضحى', '1', '0'),
(76, 15, 9, 3, 1, 45, 345, 300, '2021-01-17 02:18:01', NULL, 2, 7, 6, NULL, 'الأضحى', '1', '0'),
(77, 13, 9, 3, 1, 90, 690, 600, '2021-01-17 02:20:26', NULL, 0, 0, 0, NULL, 'الأضحى', '1', '0'),
(78, 7, 9, 3, 1, 45, 345, 300, '2021-01-17 02:26:25', NULL, 0, 0, 0, NULL, 'الأضحى', '1', '0'),
(79, 11, 9, 3, 1, 75, 575, 500, '2021-01-17 02:30:43', NULL, 0, 0, 0, NULL, 'الأضحى', '1', '0'),
(80, 11, 0, 0, 1, 75, 575, 500, '2021-01-17 13:08:26', NULL, 0, 0, 0, NULL, 'الأضحى', '1', '0'),
(81, 11, 9, 3, 1, 45, 345, 300, '2021-01-17 14:12:02', NULL, 2, 44, 55, NULL, 'الأضحى', '1', '0'),
(82, 13, 9, 3, 1, 4.5, 34.5, 30, '2021-01-17 14:15:50', NULL, 2, 2, 1, NULL, 'الأضحى', '1', NULL),
(83, 7, 9, 3, 1, 4.5, 34.5, 30, '2021-01-17 14:17:40', NULL, 2, 2, 1, NULL, 'الأضحى', '1', NULL),
(84, 11, 9, 3, 1, 45, 345, 300, '2021-01-17 14:18:48', NULL, 2, 6, 5, NULL, 'الأضحى', '1', '0'),
(85, 14, 9, 3, 1, 15, 115, 100, '2021-01-17 14:34:55', NULL, 2, 2, 1, NULL, 'الأضحى', '1', NULL),
(86, 11, 9, 3, 1, 15, 115, 100, '2021-01-17 14:41:20', NULL, 0, 0, 0, NULL, 'الأضحى', '1', NULL),
(87, 14, 9, 3, 4, 16.5, 126.5, 110, '2021-01-17 22:14:49', NULL, 0, 0, 0, NULL, 'الأضحى', '1', '0'),
(88, 14, 11, 6, 1, 4.5, 34.5, 30, '2021-01-20 15:38:31', NULL, 0, 0, 0, NULL, 'الأضحى', '1', '0'),
(89, 13, 9, 3, 5, 0, 143.75, 125, '2021-01-21 02:53:30', NULL, 0, 0, 0, NULL, 'الأضحى', '3', '0'),
(90, 11, 9, 3, 5, 18.75, 143.75, 125, '2021-01-22 01:04:04', NULL, 0, 0, 0, NULL, 'الأضحى', '1', '0'),
(91, 14, 9, 3, 12, 360, 2760, 2400, '2021-01-29 13:19:00', NULL, 2, 2700, 60, NULL, 'الأضحى', '1', '0'),
(92, 14, 9, 3, 2, 34.5, 264.5, 230, '2021-01-29 13:50:28', NULL, 0, 0, 0, NULL, 'الأضحى', '1', '0'),
(93, 14, 9, 3, 1, 4.5, 34.5, 30, '2021-01-29 13:56:53', NULL, 0, 34.5, 0, NULL, 'الأضحى', '1', '0'),
(94, 14, 9, 3, 1, 30, 230, 200, '2021-01-29 13:57:34', NULL, 1, 0, 230, NULL, 'الأضحى', '1', '0'),
(95, 14, 9, 3, 1, 1.2, 9.2, 8, '2021-01-29 13:58:12', NULL, 2, 8, 1, NULL, 'الأضحى', '1', '0'),
(96, 14, 9, 3, 1, 4.5, 34.5, 30, '2021-01-29 14:04:04', NULL, 2, 4, 6, NULL, 'الأضحى', '1', '0'),
(97, 11, 9, 3, 1, 30, 230, 200, '2021-01-29 14:04:23', NULL, 0, 230, 0, NULL, 'الأضحى', '1', '0'),
(98, 11, 9, 3, 1, 30, 230, 200, '2021-01-29 14:06:55', NULL, 1, 0, 230, NULL, 'الأضحى', '1', '0'),
(99, 13, 9, 3, 1, 4.5, 34.5, 30, '2021-01-29 14:12:52', NULL, 0, 34.5, 0, NULL, 'الأضحى', '1', '0'),
(100, 13, 9, 3, 1, 4.5, 34.5, 30, '2021-01-29 14:13:17', NULL, 0, 34.5, 0, NULL, 'الأضحى', '1', '0'),
(101, 14, 9, 3, 1, 30, 230, 200, '2021-01-29 14:15:24', NULL, 0, 230, 0, NULL, 'الأضحى', '1', '0'),
(102, 11, 9, 3, 1, 30, 230, 200, '2021-01-29 14:16:51', NULL, 0, 230, 0, NULL, 'الأضحى', '1', '0'),
(103, 15, 9, 3, 1, 30, 230, 200, '2021-01-29 14:23:35', NULL, 1, 0, 230, NULL, 'الأضحى', '1', '0'),
(104, 11, 9, 3, 1, 30, 230, 200, '2021-01-29 14:24:20', NULL, 0, 230, 0, NULL, 'الأضحى', '1', '0'),
(105, 13, 9, 3, 1, 4.5, 34.5, 30, '2021-01-29 14:24:39', NULL, 0, 34.5, 0, NULL, 'الأضحى', '1', '0'),
(106, 11, 9, 3, 1, 4.5, 34.5, 30, '2021-01-29 14:30:15', NULL, 0, 34.5, 0, NULL, 'الأضحى', '1', '0'),
(107, 14, 9, 3, 1, 75, 575, 500, '2021-01-29 14:31:12', NULL, 0, 575, 0, NULL, 'الأضحى', '1', '0'),
(108, 13, 9, 3, 1, 4.5, 34.5, 30, '2021-01-29 14:31:23', NULL, 1, 0, 34.5, NULL, 'الأضحى', '1', '0'),
(109, 14, 9, 3, 1, 30, 230, 200, '2021-01-29 14:31:41', NULL, 2, 4, 5, NULL, 'الأضحى', '1', '0'),
(110, 14, 9, 3, 1, 30, 230, 200, '2021-01-29 14:32:46', NULL, 2, 7, 9, NULL, 'الأضحى', '1', '0'),
(111, 13, 11, 6, 1, 30, 230, 200, '2021-01-29 14:42:49', NULL, 2, 60, 70, 'zxvvcbghgfhfg', 'الأضحى', '3', '0'),
(112, 11, 9, 3, 2, 34.5, 264.5, 230, '2021-01-29 14:48:16', NULL, 0, 264.5, 0, NULL, 'الأضحى', '1', NULL),
(113, 11, 9, 3, 2, 34.5, 264.5, 230, '2021-01-29 14:49:31', NULL, 0, 264.5, 0, NULL, 'الأضحى', '1', '0'),
(114, 13, 9, 3, 1, 30, 230, 200, '2021-01-29 14:51:28', NULL, 2, 2, 6, NULL, 'الأضحى', '1', NULL),
(115, 9, 9, 3, 1, 1.5, 11.5, 10, '2021-01-29 14:59:52', NULL, 0, 11.5, 0, NULL, 'الأضحى', '1', '0'),
(116, 9, 9, 3, 1, 30, 230, 200, '2021-01-29 14:52:02', NULL, 1, 0, 230, NULL, 'الأضحى', '1', NULL),
(117, 13, 12, 7, 1, 30, 230, 200, '2021-01-29 15:03:43', NULL, 2, 4, 6, NULL, 'الأضحى', '1', NULL),
(118, 9, 12, 7, 1, 30, 230, 200, '2021-01-29 15:10:34', NULL, 1, 0, 230, NULL, 'الأضحى', '1', '0'),
(119, 9, 12, 7, 1, 4.5, 34.5, 30, '2021-01-29 15:05:01', NULL, 2, 6, 8, NULL, 'الأضحى', '1', NULL),
(120, 13, 12, 7, 1, 30, 230, 200, '2021-01-29 15:06:26', NULL, 1, 0, 230, NULL, 'الأضحى', '1', NULL),
(121, 13, 12, 7, 1, 30, 230, 200, '2021-01-29 15:09:30', NULL, 2, 7, 8, NULL, 'الأضحى', '1', NULL),
(122, 13, 9, 3, 12, 45, 345, 300, '2021-01-31 16:30:22', NULL, 0, 345, 0, NULL, 'الأضحى', '1', '0'),
(123, 13, 9, 3, 12, 45, 345, 300, '2021-01-31 16:40:23', NULL, 0, 345, 0, NULL, 'الأضحى', '1', '0'),
(124, 13, 9, 3, 4, 4.5, 34.5, 30, '2021-01-31 20:03:32', NULL, 2, 5, 7, NULL, 'الأضحى', '1', '0'),
(125, 9, 9, 3, 1, 30, 230, 200, '2021-01-31 21:44:20', NULL, 0, 230, 0, NULL, 'الأضحى', '1', '0'),
(126, 11, 9, 3, 1, 30, 230, 200, '2021-01-31 21:45:14', NULL, 0, 230, 0, NULL, 'الأضحى', '1', '0'),
(127, 9, 9, 3, 1, 30, 230, 200, '2021-01-31 21:46:07', NULL, 0, 230, 0, NULL, 'الأضحى', '1', '0'),
(128, 13, 9, 3, 1, 30, 230, 200, '2021-01-31 21:50:01', NULL, 0, 230, 0, 'CFtZJd83pc', 'الأضحى', '1', '0'),
(129, 8, 9, 3, 1, 30, 230, 200, '2021-01-31 21:55:13', NULL, 0, 230, 0, 'IbLWThaWkW', 'الأضحى', '1', '0'),
(130, 12, 9, 3, 1, 30, 230, 200, '2021-01-31 21:56:52', NULL, 0, 230, 0, 'mPFem7aXgC', 'الأضحى', '1', '0'),
(131, 13, 9, 3, 1, 30, 230, 200, '2021-01-31 22:06:40', NULL, 0, 230, 0, '1emVM1V4VF', 'الأضحى', '1', '0'),
(132, 15, 9, 3, 1, 75, 575, 500, '2021-01-31 22:07:55', NULL, 0, 575, 0, 'Q40AvZZwra', 'الأضحى', '1', '0'),
(133, 14, 11, 6, 1, 30, 230, 200, '2021-01-31 22:22:35', NULL, 0, 230, 0, 'ESo8nPhdrk', 'الأضحى', '3', '0'),
(134, 13, 9, 3, 9, 15, 115, 100, '2021-01-31 20:41:20', NULL, 2, 8, 9, '6vJLOB60ZV', 'الأضحى', '1', '0'),
(135, 16, 9, 3, 1, 30, 230, 200, '2021-02-02 12:44:48', NULL, 2, 100, 120, '1EsJmSXh1s', 'الأضحى', '1', '0'),
(136, 16, 9, 3, 1, 30, 230, 200, '2021-02-02 12:46:30', NULL, 2, 100, 130, '8i3lc0Lybk', 'الأضحى', '1', '0'),
(137, 13, 12, 7, 15, 92.196, 706.836, 614.64, '2021-02-05 09:09:57', NULL, 2, 60, 20, 'JlfjPmGLrC', 'الأضحى', '3', '0'),
(138, 17, 9, 3, 1, 22.5, 172.5, 150, '2021-02-21 13:51:55', NULL, 2, 100, 50, 'GhvgQSQKie', 'الأضحى', '1', '0'),
(139, 17, 9, 3, 2, 15.651, 119.991, 104.34, '2021-02-21 13:59:02', NULL, 1, 0, 59.9955, '1YYf6vIQpY', 'الأضحى', '3', '0'),
(140, 15, 9, 3, 2, 20.217, 154.997, 134.78, '2021-03-01 14:46:34', NULL, 0, 154.997, 0, 'EfKxeo4NHp', 'الأضحى', '3', '0'),
(141, 19, 9, 3, 1, 30, 230, 200, '2021-03-03 12:10:37', NULL, 0, 230, 0, '2Dhs7dLzvy', 'الأضحى', '1', '0'),
(142, 19, 9, 3, 1, 30, 230, 200, '2021-03-03 12:26:19', NULL, 2, 100, 30, 'G50W9nzEye', 'الأضحى', '1', '0'),
(143, 19, 9, 3, 1, 30, 230, 200, '2021-03-03 12:29:14', NULL, 2, 100, 30, 'zAvp9SzI6m', 'الأضحى', '3', '0'),
(144, 19, 9, 3, 1, 30, 230, 200, '2021-03-03 12:35:05', NULL, 0, 230, 0, 'qUbe2eRsrk', 'الأضحى', '3', '0'),
(145, 16, 9, 3, 1, 6, 46, 40, '2021-03-27 21:39:27', NULL, 1, 0, 46, 'FhpfcpkJgK', 'الأضحى', '1', '0'),
(146, 18, 9, 3, 2, 105, 805, 700, '2021-03-27 21:46:02', NULL, 0, 805, 0, 'ylJh8qRG4o', 'الأضحى', '1', '0'),
(147, 13, 9, 3, 2, 135, 1035, 900, '2021-03-27 21:46:33', NULL, 2, 1000, 35, 'NTdmVek7Mm', 'الأضحى', '1', '0'),
(148, 13, 9, 3, 1, 45, 345, 300, '2021-03-28 19:31:19', NULL, 0, 345, 0, 't4BZJMGgPC', 'الأضحى', '1', '0'),
(149, 18, 9, 3, 2, 7.5, 57.5, 50, '2021-03-28 19:32:20', NULL, 2, 57, 0.5, 'Q7KTg3NN73', 'الأضحى', '1', '0'),
(150, 13, 9, 3, 4, 28.0425, 214.992, 186.95, '2021-03-28 20:06:28', NULL, 0, 214.992, 0, 'KS3BJO8ume', 'الأضحى', '1', '0'),
(151, 18, 9, 3, 2, 59.4, 455.4, 396, '2021-03-28 20:19:24', NULL, 0, 455.4, 0, 'SFX22GHjHf', 'الأضحى', '1', '0'),
(152, 16, 9, 3, 1, 54, 414, 360, '2021-03-28 20:19:56', NULL, 0, 414, 0, 'eyGcz06k9V', 'الأضحى', '1', '0'),
(153, 16, 9, 3, 5, 300, 2300, 2000, '2021-03-28 20:20:43', NULL, 0, 2300, 0, 'XCY5o2luk6', 'الأضحى', '1', '0'),
(154, 17, 9, 3, 1, 60, 460, 400, '2021-03-28 20:22:05', NULL, 0, 460, 0, 'RPybAu5sht', 'الأضحى', '1', '0'),
(155, 20, 9, 3, 1, 60, 460, 400, '2021-03-28 21:08:46', NULL, 0, 460, 0, '3GSyLstILD', 'الأضحى', '1', '0'),
(156, 20, 9, 3, 1, 45, 345, 300, '2021-03-28 21:27:37', NULL, 0, 345, 0, 'XQIsNGzE2T', 'الأضحى', '3', '0'),
(157, 20, 9, 3, 2, 66, 506, 440, '2021-03-28 21:34:54', NULL, 0, 506, 0, 'zxthaGTNrQ', 'الأضحى', '1', '0'),
(158, 13, 9, 3, 3, 15, 115, 100, '2021-03-30 19:18:52', NULL, 0, 115, 0, 'MjtiJovbbq', 'الأضحى', '1', '0'),
(159, 27, 9, 3, 9, 181.953, 1394.97, 1213.02, '2021-03-31 06:24:42', NULL, 0, 1549.97, 0, 'eCVgR7XV0p', 'الأضحى', '2', '0'),
(160, 27, 9, 3, 2, 60, 460, 400, '2021-03-31 06:47:16', NULL, 2, 200, 260, 'CO7UJPqBTG', 'الأضحى', '1', '0'),
(161, 13, 9, 3, 9, 54, 414, 360, '2021-03-31 18:15:35', NULL, 2, 400, 14, 'reyRmXQgFB', 'الأضحى', '3', '0'),
(162, 15, 9, 3, 1, 11.0872, 85.0023, 73.915, '2021-04-05 19:15:09', NULL, 0, 85.0023, 0, 'OIUqdKwoCs', 'الأضحى', '3', '0'),
(163, 15, 9, 3, 1, 11.0872, 85.0023, 73.915, '2021-04-05 19:18:12', NULL, 0, 85.0023, 0, 'SXoiCabnIy', 'الأضحى', '1', '0'),
(164, 28, 9, 3, 1, 60, 460, 400, '2021-04-05 19:21:11', NULL, 0, 460, 0, 'D9uSzdkkf4', 'الأضحى', '3', '0'),
(165, 15, 9, 3, 1, 11.0872, 85.0023, 73.915, '2021-04-06 11:03:32', NULL, 2, 80, 5.00225, 'VbW9jPONab', 'الأضحى', '1', '0'),
(166, 15, 9, 3, 1, 11.0872, 85.0023, 73.915, '2021-04-06 11:04:07', NULL, 0, 85.0023, 0, '3NkaVMRwIt', 'الأضحى', '1', '0'),
(167, 15, 9, 3, 1, 11.0872, 85.0023, 73.915, '2021-04-06 11:10:27', NULL, 0, 85.0023, 0, 'LohbakphnX', 'الأضحى', '1', '0'),
(168, 15, 9, 3, 12, 16.5, 126.5, 110, '2021-04-12 22:04:29', NULL, 0, 126.5, 0, 'J7t8Jcoqnw', 'الأضحى', '1', '0'),
(169, 16, 9, 3, 60, 873.26, 6694.96, 5821.7, '2021-04-18 20:39:25', '2021-04-18 20:44:05', 0, 6694.96, 0, 'Sfu2sgxmfr', 'الأضحى', '3', '0'),
(170, 16, 9, 3, 0, 0, 0, 0, '2021-04-20 20:12:02', NULL, 0, 60, 0, 'iC8QrR1Ze2', 'الأضحى', '2', '0'),
(171, 15, 9, 3, 20, 156.51, 1199.91, 1043.4, '2021-04-20 23:38:16', '2021-04-20 23:39:36', 0, 1199.91, 0, 'I7cOtDCOXh', 'الأضحى', '3', '0'),
(172, 29, 9, 3, 50, 2406.51, 18449.9, 16043.4, '2021-04-21 18:37:26', NULL, 0, 18449.9, 0, 'DoevbOu3aU', 'الأضحى', '1', '0'),
(173, 29, 9, 3, 10, 67.5, 517.5, 450, '2021-04-21 18:42:19', NULL, 0, 517.5, 0, 'upnnSd2f9V', 'الأضحى', '1', '0'),
(174, 29, 9, 3, 10, 78.26, 599.96, 521.7, '2021-04-21 19:04:38', NULL, 0, 599.96, 0, 'UPHkyET10w', 'الأضحى', '1', '0'),
(175, 29, 9, 3, 7, 60, 460, 400, '2021-04-21 19:07:05', NULL, 0, 460, 0, '56KqWDrR14', 'الأضحى', '1', '0'),
(176, 29, 9, 3, 6, 46.95, 359.97, 313.02, '2021-04-21 19:08:05', NULL, 0, 359.97, 0, '0CPIL21wsC', 'الأضحى', '1', '0'),
(177, 16, 9, 3, 3, 23.48, 179.99, 156.51, '2021-04-24 13:51:08', NULL, 0, 179.99, 0, 'phpqH3hgDd', 'الأضحى', '1', '0'),
(178, 16, 9, 3, 13, 116.41, 892.45, 776.04, '2021-04-24 13:54:22', NULL, 0, 892.45, 0, 'D7BY0PI0xe', 'الأضحى', '1', '0'),
(179, 30, 9, 3, 30, 906.51, 6949.91, 6043.4, '2021-04-24 13:58:31', '2021-04-24 14:00:25', 0, 6949.91, 0, 'S3PUHmUZvY', 'الأضحى', '3', '0'),
(180, 30, 9, 3, 2, 15.65, 119.99, 104.34, '2021-04-24 13:58:50', '2021-04-24 14:00:09', 0, 119.99, 0, '6VBtYU0Wv8', 'الأضحى', '3', '0'),
(181, 30, 9, 3, 16, 528.26, 4049.96, 3521.7, '2021-04-24 16:38:27', NULL, 0, 4049.96, 0, 'iuvgbSG3eq', 'الأضحى', '1', '0'),
(182, 30, 9, 3, 4, 36, 276, 240, '2021-04-24 16:39:26', '2021-04-24 16:43:28', 0, 276, 0, '3MvvmxEaYa', 'الأضحى', '3', '0'),
(183, 30, 9, 3, 9, 60.75, 465.75, 405, '2021-04-24 16:40:45', '2021-04-24 16:42:42', 0, 465.75, 0, 'kXQ5YyavCJ', 'الأضحى', '3', '0'),
(184, 30, 11, 6, 1, 6, 46, 40, '2021-04-24 20:16:49', '2021-04-24 20:18:21', 0, 46, 0, 'SPjsZUmpNU', 'الأضحى', '3', '0'),
(185, 30, 11, 6, 0, 0, 0, 0, '2021-04-24 20:19:48', NULL, 0, 0, 0, 'MTDySYwGUS', 'الأضحى', '1', '0'),
(186, 30, 11, 6, 0, 0, 0, 0, '2021-04-24 20:22:26', NULL, 0, 0, 0, 'YiMThMxRen', 'الأضحى', '1', '0'),
(187, 30, 11, 6, 11, 682.83, 5235, 4552.17, '2021-04-24 21:09:50', NULL, 0, 5235, 0, 'UMOtVr2BGb', 'الأضحى', '1', '0'),
(188, 16, 9, 3, 10, 90, 690, 600, '2021-04-28 04:00:49', NULL, 0, 690, 0, '51HylPxDId', 'الأضحى', '1', '0'),
(189, 16, 9, 3, 20, 180, 1380, 1200, '2021-04-28 04:01:36', '2021-04-28 04:02:35', 0, 1380, 0, 'dKLqP3ybMF', 'الأضحى', '3', '0'),
(190, 28, 12, 7, 1, 0.75, 5.75, 5, '2021-04-28 04:03:25', '2021-04-28 04:04:39', 0, 34.5, 0, 'tkgcIftspT', 'الأضحى', '3', '0'),
(191, 16, 12, 7, 10, 90, 690, 600, '2021-04-28 04:05:32', '2021-04-28 04:06:10', 0, 690, 0, 'zdUZvfWIeg', 'الأضحى', '3', '0'),
(192, 16, 9, 3, 5, 112.5, 862.5, 750, '2021-04-28 04:08:17', '2021-04-28 04:08:48', 0, 862.5, 0, 'LOA8z890Ci', 'الأضحى', '3', '0'),
(193, 16, 12, 7, 10, 90, 690, 600, '2021-04-28 04:09:40', '2021-04-28 04:10:16', 0, 690, 0, '2UbVidbV9B', 'الأضحى', '3', '0'),
(194, 16, 9, 3, 12, 7.5, 57.5, 50, '2021-04-28 04:15:24', NULL, 0, 57.5, 0, 'KKfWjzSiNu', 'الأضحى', '1', '0'),
(195, 18, 9, 3, 1, 7.83, 60, 52.17, '2021-05-01 08:49:53', NULL, 0, 60, 0, 'MnIo5yiVv2', 'الأضحى', '1', '0'),
(196, 15, 9, 3, 1, 7.83, 60, 52.17, '2021-05-01 09:01:48', NULL, 0, 60, 0, 'IyL9rDD3xZ', 'الأضحى', '1', '0'),
(197, 29, 9, 3, 1, 22.5, 172.5, 150, '2021-05-01 09:21:30', NULL, 0, 172.5, 0, 'jamxtfmU1F', 'الأضحى', '1', '0'),
(198, 28, 9, 3, 4, 52.17, 400, 347.83, '2021-05-05 20:33:57', NULL, 0, 400, 0, 'rbFnaFEgEP', 'الأضحى', '1', '0'),
(199, 28, 9, 3, 9, 128.48, 984.98, 856.5, '2021-05-06 19:48:02', NULL, 0, 984.98, 0, 'vFB8aIWFLy', 'الأضحى', '1', '0'),
(200, 28, 9, 3, 2, 7.8255, 59.9955, 52.17, '2021-05-11 10:49:42', NULL, 0, 30, 0, 'mV7bs1cR7P', 'الأضحى', '2', '0'),
(201, 16, 9, 3, 1, 7.5, 57.5, 50, '2021-05-18 10:10:06', '2021-05-18 10:10:37', 0, 57.5, 0, 'DR2BEqk33C', 'الأضحى', '3', '0'),
(202, 7, 9, 3, 10, 75, 575, 500, '2021-05-18 10:19:55', NULL, 0, 575, 0, 'NmvclsUPmv', 'الأضحى', '1', '0'),
(203, 15, 9, 3, 5, 39.13, 299.98, 260.85, '2021-06-09 08:00:10', '2021-06-09 08:01:03', 0, 299.98, 0, 'jeAQrSrhFB', 'الأضحى', '3', '0'),
(204, 28, 9, 3, 2, 7.8255, 59.9955, 52.17, '2021-06-26 14:19:18', NULL, 0, 119.99, 0, 'erZbridQam', 'الأضحى', '2', '0'),
(205, 28, 9, 3, 1, 13.5, 103.5, 90, '2021-06-26 14:35:27', NULL, 0, 103.5, 0, 'nEcdzDAcxm', 'الأضحى', '1', '0'),
(206, 28, 9, 3, 1, 9.78, 75, 65.22, '2021-07-06 12:38:09', NULL, 0, 75, 0, 'phprMXrMiJ', 'الأضحى', '1', '0'),
(207, 15, 9, 3, 1, 6.75, 51.75, 45, '2021-07-07 12:56:55', NULL, 0, 51.75, 0, 'SfalKLn5wP', 'الأضحى', '1', '0'),
(208, 7, 9, 3, 1, 6.75, 51.75, 45, '2021-07-07 13:09:17', NULL, 0, 51.75, 0, 'zU9AFyyxWw', 'الأضحى', '1', '0'),
(210, 37, 9, 3, 50, 2250, 17250, 15000, '2021-07-27 19:57:25', NULL, 0, 17250, 0, '1LPtigk8Zb', 'الأضحى', '1', '0'),
(218, 28, 9, 3, 1, 22.17, 170, 147.83, '2021-07-29 13:36:23', NULL, 0, 170, 0, 'icsfbI51ET', 'الأضحى', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `sales_receiptdetails`
--

CREATE TABLE `sales_receiptdetails` (
  `id` int(11) NOT NULL,
  `receipt_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_price_beforesale` float NOT NULL,
  `product_price` float NOT NULL,
  `product_quantity` float NOT NULL,
  `product_pricewithouttax` float NOT NULL,
  `tax` float NOT NULL,
  `tax_description` text,
  `product_pricewithtax` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sales_receiptdetails`
--

INSERT INTO `sales_receiptdetails` (`id`, `receipt_id`, `product_id`, `product_price_beforesale`, `product_price`, `product_quantity`, `product_pricewithouttax`, `tax`, `tax_description`, `product_pricewithtax`) VALUES
(1, 1, 1, 100, 100, 2, 200, 50, NULL, 250),
(2, 2, 2, 30, 30, 3, 90, 22.5, NULL, 112.5),
(3, 3, 3, 300, 300, 6, 1800, 450, NULL, 2250),
(4, 4, 7, 300, 70, 2, 140, 35, NULL, 175),
(5, 4, 6, 80, 70, 1, 70, 17.5, NULL, 87.5),
(6, 4, 3, 300, 300, 1, 300, 75, NULL, 375),
(7, 5, 3, 300, 300, 1, 300, 75, NULL, 375),
(8, 5, 4, 500, 500, 1, 0, 0, NULL, 0),
(9, 5, 1, 100, 100, 2, 200, 50, NULL, 250),
(10, 5, 2, 30, 30, 1, 10, 2.5, NULL, 12.5),
(11, 6, 7, 300, 150, 1, 150, 37.5, NULL, 187.5),
(12, 7, 3, 300, 300, 1, 300, 75, NULL, 375),
(13, 8, 7, 300, 300, 1, 300, 75, NULL, 375),
(14, 8, 6, 80, 80, 1, 80, 20, NULL, 100),
(15, 9, 2, 30, 30, 1, 30, 7.5, NULL, 37.5),
(16, 10, 4, 500, 500, 1, 500, 125, NULL, 625),
(17, 11, 7, 300, 300, 1, 300, 75, NULL, 375),
(18, 12, 8, 0, 0, 3, 0, 0, NULL, 0),
(19, 12, 9, 20, 20, 10, 200, 50, NULL, 250),
(20, 13, 9, 20, 20, 20, 400, 100, NULL, 500),
(21, 14, 9, 20, 20, 1, 20, 5, NULL, 25),
(22, 15, 9, 20, 20, 2, 40, 10, NULL, 50),
(23, 16, 11, 10, 10, 10, 100, 25, NULL, 125),
(24, 17, 9, 20, 20, 3, 60, 15, NULL, 75),
(25, 17, 11, 10, 10, 5, 50, 12.5, NULL, 62.5),
(26, 18, 11, 10, 10, 5, 50, 12.5, NULL, 62.5),
(27, 19, 11, 10, 10, 2, 20, 5, NULL, 25),
(28, 20, 11, 10, 10, 1, 10, 2.5, NULL, 12.5),
(29, 21, 4, 500, 500, 9, 4500, 1125, NULL, 5625),
(30, 22, 8, 0, 0, 1, 0, 0, NULL, 0),
(31, 23, 11, 10, 10, 2, 20, 5, NULL, 25),
(32, 24, 3, 300, 300, 1, 300, 75, NULL, 375),
(33, 25, 1, 100, 100, 1, 100, 25, NULL, 125),
(34, 26, 3, 300, 300, 1, 300, 75, NULL, 375),
(35, 27, 3, 300, 300, 1, 300, 75, NULL, 375),
(36, 28, 5, 600, 600, 1, 600, 150, NULL, 750),
(37, 29, 5, 600, 600, 5, 3000, 750, NULL, 3750),
(38, 30, 3, 300, 300, 1, 300, 75, NULL, 375),
(39, 30, 4, 500, 500, 1, 500, 125, NULL, 625),
(40, 31, 3, 300, 300, 3, 900, 225, NULL, 1125),
(41, 32, 1, 100, 100, 2, 200, 50, NULL, 250),
(42, 33, 1, 100, 100, 1, 100, 15, NULL, 115),
(43, 34, 3, 300, 300, 1, 300, 45, NULL, 345),
(44, 35, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(45, 36, 1, 100, 100, 1, 100, 15, NULL, 115),
(46, 37, 1, 100, 100, 1, 100, 15, NULL, 115),
(47, 38, 1, 100, 100, 1, 100, 15, NULL, 115),
(48, 39, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(49, 40, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(50, 41, 3, 300, 300, 1, 300, 45, NULL, 345),
(51, 42, 4, 500, 500, 1, 500, 75, NULL, 575),
(52, 43, 3, 300, 300, 1, 300, 45, NULL, 345),
(53, 44, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(54, 45, 3, 300, 300, 1, 300, 45, NULL, 345),
(55, 46, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(56, 47, 5, 600, 600, 1, 600, 90, NULL, 690),
(57, 48, 5, 600, 600, 1, 600, 90, NULL, 690),
(58, 49, 4, 500, 500, 1, 500, 75, NULL, 575),
(59, 50, 3, 300, 300, 1, 300, 45, NULL, 345),
(60, 51, 3, 300, 300, 1, 300, 45, NULL, 345),
(61, 52, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(62, 53, 3, 300, 300, 1, 300, 45, NULL, 345),
(63, 54, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(64, 55, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(65, 56, 3, 300, 300, 1, 300, 45, NULL, 345),
(66, 57, 3, 300, 300, 1, 300, 45, NULL, 345),
(67, 58, 3, 300, 300, 1, 300, 45, NULL, 345),
(68, 59, 3, 300, 300, 1, 300, 45, NULL, 345),
(71, 60, 3, 300, 300, 2, 600, 90, NULL, 690),
(73, 61, 3, 300, 300, 4, 1200, 180, NULL, 1380),
(75, 62, 3, 300, 300, 7, 2100, 315, NULL, 2415),
(76, 63, 3, 300, 300, 1, 300, 45, NULL, 345),
(77, 64, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(78, 65, 3, 300, 300, 1, 300, 45, NULL, 345),
(79, 66, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(80, 67, 11, 10, 10, 1, 10, 1.5, NULL, 11.5),
(81, 68, 3, 300, 300, 1, 300, 45, NULL, 345),
(82, 69, 5, 600, 600, 1, 600, 90, NULL, 690),
(83, 70, 4, 500, 500, 1, 500, 75, NULL, 575),
(84, 71, 4, 500, 500, 1, 500, 75, NULL, 575),
(85, 72, 14, 0, 95, 1, 95, 14.25, NULL, 109.25),
(86, 72, 15, 345, 95, 1, 95, 14.25, NULL, 109.25),
(87, 73, 1, 100, 100, 1, 100, 15, NULL, 115),
(88, 74, 4, 500, 500, 1, 500, 75, NULL, 575),
(89, 75, 3, 300, 300, 1, 300, 45, NULL, 345),
(90, 76, 3, 300, 300, 1, 300, 45, NULL, 345),
(91, 77, 5, 600, 600, 1, 600, 90, NULL, 690),
(92, 78, 3, 300, 300, 1, 300, 45, NULL, 345),
(93, 79, 4, 500, 500, 1, 500, 75, NULL, 575),
(94, 80, 4, 500, 500, 1, 500, 75, NULL, 575),
(95, 81, 3, 300, 300, 1, 300, 45, NULL, 345),
(96, 82, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(97, 83, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(98, 84, 3, 300, 300, 1, 300, 45, NULL, 345),
(99, 85, 1, 100, 100, 1, 100, 15, NULL, 115),
(100, 86, 1, 100, 100, 1, 100, 15, NULL, 115),
(101, 87, 1, 100, 25, 1, 25, 3.75, NULL, 28.75),
(102, 87, 2, 30, 30, 2, 60, 9, NULL, 69),
(103, 87, 3, 300, 25, 1, 25, 3.75, NULL, 28.75),
(104, 88, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(106, 89, 13, 8, 25, 5, 125, 18.75, NULL, 143.75),
(107, 90, 1, 200, 25, 5, 125, 18.75, NULL, 143.75),
(108, 91, 1, 200, 200, 12, 2400, 360, NULL, 2760),
(109, 92, 1, 200, 200, 1, 200, 30, NULL, 230),
(110, 92, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(111, 93, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(112, 94, 3, 300, 200, 1, 200, 30, NULL, 230),
(113, 95, 17, 8, 8, 1, 8, 1.2, NULL, 9.2),
(114, 96, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(115, 97, 3, 300, 200, 1, 200, 30, NULL, 230),
(116, 98, 3, 300, 200, 1, 200, 30, NULL, 230),
(117, 99, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(118, 100, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(119, 101, 1, 200, 200, 1, 200, 30, NULL, 230),
(120, 102, 3, 300, 200, 1, 200, 30, NULL, 230),
(121, 103, 3, 300, 200, 1, 200, 30, NULL, 230),
(122, 104, 3, 300, 200, 1, 200, 30, NULL, 230),
(123, 105, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(124, 106, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(125, 107, 4, 500, 500, 1, 500, 75, NULL, 575),
(126, 108, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(127, 109, 3, 300, 200, 1, 200, 30, NULL, 230),
(128, 110, 3, 300, 200, 1, 200, 30, NULL, 230),
(129, 111, 1, 200, 200, 1, 200, 30, NULL, 230),
(130, 112, 1, 200, 200, 1, 200, 30, NULL, 230),
(131, 113, 6, 80, 80, 1, 80, 12, NULL, 92),
(132, 112, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(133, 113, 7, 300, 150, 1, 150, 22.5, NULL, 172.5),
(134, 114, 1, 200, 200, 1, 200, 30, NULL, 230),
(135, 115, 11, 10, 10, 1, 10, 1.5, NULL, 11.5),
(136, 116, 3, 300, 200, 1, 200, 30, NULL, 230),
(137, 117, 1, 200, 200, 1, 200, 30, NULL, 230),
(138, 118, 3, 300, 200, 1, 200, 30, NULL, 230),
(139, 119, 2, 30, 30, 1, 30, 4.5, NULL, 34.5),
(140, 120, 1, 200, 200, 1, 200, 30, NULL, 230),
(141, 121, 1, 200, 200, 1, 200, 30, NULL, 230),
(142, 122, 2, 30, 25, 12, 300, 45, NULL, 345),
(143, 123, 2, 30, 25, 12, 300, 45, NULL, 345),
(144, 124, 2, 40, 30, 4, 30, 4.5, NULL, 34.5),
(145, 125, 1, 200, 200, 1, 200, 30, NULL, 230),
(146, 126, 3, 300, 200, 1, 200, 30, NULL, 230),
(147, 127, 3, 300, 200, 1, 200, 30, NULL, 230),
(148, 128, 1, 200, 200, 1, 200, 30, NULL, 230),
(149, 129, 3, 300, 200, 1, 200, 30, NULL, 230),
(150, 130, 1, 200, 200, 1, 200, 30, NULL, 230),
(151, 131, 3, 300, 200, 1, 200, 30, NULL, 230),
(152, 132, 4, 500, 500, 1, 500, 75, NULL, 575),
(153, 133, 3, 300, 200, 1, 200, 30, NULL, 230),
(154, 134, 2, 40, 60, 9, 100, 15, NULL, 115),
(155, 135, 3, 300, 200, 1, 200, 30, NULL, 230),
(156, 136, 3, 300, 200, 1, 200, 30, NULL, 230),
(158, 138, 21, 150, 150, 1, 150, 22.5, NULL, 172.5),
(160, 139, 22, 52.17, 52.17, 2, 104.34, 15.651, NULL, 119.991),
(161, 140, 23, 134.78, 134.78, 2, 134.78, 20.217, NULL, 154.997),
(162, 141, 3, 300, 200, 1, 200, 30, NULL, 230),
(163, 142, 3, 300, 200, 1, 200, 30, NULL, 230),
(164, 143, 3, 300, 200, 1, 200, 30, NULL, 230),
(165, 144, 3, 300, 200, 1, 200, 30, NULL, 230),
(166, 137, 1, 200, 200, 15, 614.64, 92.196, NULL, 706.836),
(167, 145, 2, 40, 40, 1, 40, 6, NULL, 46),
(168, 146, 1, 400, 400, 1, 400, 60, NULL, 460),
(169, 146, 3, 300, 300, 1, 300, 45, NULL, 345),
(170, 147, 1, 400, 400, 1, 400, 60, NULL, 460),
(171, 147, 4, 500, 500, 1, 500, 75, NULL, 575),
(172, 148, 3, 300, 300, 1, 300, 45, NULL, 345),
(173, 149, 2, 40, 40, 1, 40, 6, NULL, 46),
(174, 149, 18, 10, 10, 1, 10, 1.5, NULL, 11.5),
(175, 150, 23, 134.78, 134.78, 1, 134.78, 20.217, NULL, 154.997),
(176, 150, 22, 52.17, 52.17, 3, 52.17, 7.8255, NULL, 59.9955),
(177, 151, 1, 400, 360, 1, 360, 54, NULL, 414),
(178, 151, 2, 40, 36, 1, 36, 5.4, NULL, 41.4),
(179, 152, 1, 400, 360, 1, 360, 54, NULL, 414),
(180, 153, 1, 400, 400, 5, 2000, 300, NULL, 2300),
(181, 154, 1, 400, 400, 1, 400, 60, NULL, 460),
(182, 155, 1, 400, 400, 1, 400, 60, NULL, 460),
(183, 156, 3, 300, 300, 1, 300, 45, NULL, 345),
(184, 157, 1, 400, 400, 1, 400, 60, NULL, 460),
(185, 157, 2, 40, 40, 1, 40, 6, NULL, 46),
(186, 158, 1, 400, 400, 2, 100, 15, NULL, 115),
(187, 158, 2, 40, 40, 1, 0, 0, NULL, 0),
(189, 160, 1, 400, 200, 2, 400, 60, NULL, 460),
(190, 161, 2, 40, 40, 9, 360, 54, NULL, 414),
(191, 162, 26, 147.83, 73.915, 1, 73.915, 11.0872, NULL, 85.0023),
(192, 163, 26, 147.83, 73.915, 1, 73.915, 11.0872, NULL, 85.0023),
(193, 164, 1, 400, 400, 1, 400, 60, NULL, 460),
(194, 165, 26, 147.83, 73.915, 1, 73.915, 11.0872, NULL, 85.0023),
(195, 166, 26, 147.83, 73.915, 1, 73.915, 11.0872, NULL, 85.0023),
(196, 167, 26, 147.83, 73.915, 1, 73.915, 11.0872, NULL, 85.0023),
(197, 159, 23, 134.78, 134.78, 9, 1213.02, 181.953, NULL, 1394.97),
(198, 168, 26, 10, 9.17, 12, 110, 16.5, NULL, 126.5),
(199, 169, 1, 52.17, 52, 10, 521.7, 78.26, NULL, 599.96),
(200, 169, 2, 40, 40, 20, 800, 120, NULL, 920),
(201, 169, 3, 300, 150, 30, 4500, 675, NULL, 5175),
(203, 171, 1, 52.17, 52, 20, 1043.4, 156.51, NULL, 1199.91),
(207, 170, 1, 52.17, 52, 0, 0, 0, NULL, 0),
(208, 172, 1, 52.17, 52, 20, 1043.4, 156.51, NULL, 1199.91),
(209, 172, 4, 500, 500, 30, 15000, 2250, NULL, 17250),
(210, 173, 25, 90, 45, 10, 450, 67.5, NULL, 517.5),
(211, 174, 1, 52.17, 52, 10, 521.7, 78.26, NULL, 599.96),
(212, 175, 2, 40, 40, 7, 400, 60, NULL, 460),
(213, 176, 1, 52.17, 52, 6, 313.02, 46.95, NULL, 359.97),
(214, 177, 1, 52.17, 52, 3, 156.51, 23.48, NULL, 179.99),
(215, 178, 1, 52.17, 52, 12, 626.04, 93.91, NULL, 719.95),
(216, 178, 3, 300, 150, 1, 150, 22.5, NULL, 172.5),
(217, 179, 1, 52.17, 52, 20, 1043.4, 156.51, NULL, 1199.91),
(218, 179, 4, 500, 500, 10, 5000, 750, NULL, 5750),
(219, 180, 1, 52.17, 52, 2, 104.34, 15.65, NULL, 119.99),
(220, 181, 1, 52.17, 52, 10, 521.7, 78.26, NULL, 599.96),
(221, 181, 4, 500, 500, 6, 3000, 450, NULL, 3450),
(222, 182, 2, 40, 40, 4, 240, 36, NULL, 276),
(223, 183, 25, 90, 45, 9, 405, 60.75, NULL, 465.75),
(224, 184, 2, 40, 40, 1, 40, 6, NULL, 46),
(225, 185, 3, 300, 270, 10, 2700, 405, NULL, 3105),
(226, 186, 4, 500, 450, 10, 4500, 675, NULL, 5175),
(227, 187, 4, 500, 450, 10, 4500, 675, NULL, 5175),
(228, 187, 1, 52.17, 52, 1, 52.17, 7.83, NULL, 60),
(229, 188, 2, 40, 40, 10, 600, 90, NULL, 690),
(230, 189, 2, 40, 40, 20, 1200, 180, NULL, 1380),
(232, 190, 6, 10, 5, 1, 5, 0.75, NULL, 5.75),
(233, 191, 2, 40, 40, 10, 600, 90, NULL, 690),
(234, 192, 3, 300, 150, 5, 750, 112.5, NULL, 862.5),
(235, 193, 2, 40, 40, 10, 600, 90, NULL, 690),
(236, 194, 25, 90, 4.17, 12, 50, 7.5, NULL, 57.5),
(237, 195, 1, 52.17, 52, 1, 52.17, 7.83, NULL, 60),
(238, 196, 1, 52.17, 52, 1, 52.17, 7.83, NULL, 60),
(239, 197, 3, 300, 150, 1, 150, 22.5, NULL, 172.5),
(240, 198, 23, 134.78, 135, 1, 200, 30, NULL, 230),
(241, 198, 21, 130.43, 130, 1, 0, 0, NULL, 0),
(242, 198, 20, 191.3, 191, 1, 0, 0, NULL, 0),
(243, 198, 26, 147.83, 148, 1, 147.83, 22.17, NULL, 170),
(244, 199, 21, 130.43, 65.22, 4, 260.86, 39.13, NULL, 299.99),
(245, 199, 20, 191.3, 95.65, 2, 191.3, 28.7, NULL, 220),
(246, 199, 23, 134.78, 134.78, 3, 404.34, 60.65, NULL, 464.99),
(248, 201, 2, 40, 40, 1, 50, 7.5, NULL, 57.5),
(249, 200, 1, 52.17, 26.085, 2, 52.17, 7.8255, NULL, 59.9955),
(250, 202, 2, 40, 40, 10, 500, 75, NULL, 575),
(251, 203, 1, 52.17, 52, 5, 260.85, 39.13, NULL, 299.98),
(253, 204, 1, 52.17, 26.085, 2, 52.17, 7.8255, NULL, 59.9955),
(254, 205, 25, 90, 90, 1, 90, 13.5, NULL, 103.5),
(255, 206, 21, 130.43, 65.22, 1, 65.22, 9.78, NULL, 75),
(256, 207, 25, 90, 45, 1, 45, 6.75, NULL, 51.75),
(257, 208, 25, 90, 45, 1, 45, 6.75, NULL, 51.75),
(259, 210, 3, 300, 300, 50, 15000, 2250, NULL, 17250),
(267, 218, 26, 147.83, 148, 1, 147.83, 22.17, NULL, 170);

-- --------------------------------------------------------

--
-- Table structure for table `samp_quant_movement`
--

CREATE TABLE `samp_quant_movement` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` float DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `samp_quant_movement`
--

INSERT INTO `samp_quant_movement` (`id`, `branch_id`, `product_id`, `quantity`, `created_date`) VALUES
(16, 6, 2, -60, '2019-04-18 09:11:46'),
(17, 6, 3, 10, '2021-04-18 09:12:04'),
(18, 8, 2, -5, '2021-04-18 09:12:46'),
(19, 6, 2, 20, '2019-04-18 09:11:46'),
(20, 3, 12, 10, '2021-04-18 09:29:25'),
(21, 3, 12, -5, '2021-04-18 09:31:00'),
(22, 6, 2, 10, '2021-04-24 16:15:15'),
(23, 3, 5, 5, '2021-04-27 23:55:44'),
(24, 3, 2, -30, '2021-04-27 23:56:16'),
(25, 7, 5, 3, '2021-04-27 23:57:06'),
(26, 3, 1, 1, '2021-05-05 15:24:28'),
(27, 6, 1, -10, '2021-05-06 15:04:40'),
(28, 6, 1, 51, '2021-05-06 15:04:53'),
(29, 6, 1, 10, '2021-05-06 15:05:02'),
(30, 6, 1, -71, '2021-05-06 15:05:08'),
(31, 6, 2, -30, '2021-05-06 15:05:17'),
(32, 6, 2, 30, '2021-05-06 15:05:26'),
(33, 9, 1, 1, '2021-05-06 15:06:27'),
(34, 9, 1, 9, '2021-05-06 15:06:46'),
(35, 9, 1, -10, '2021-05-06 15:07:22'),
(36, 3, 4, 5, '2021-05-18 04:06:45'),
(37, 3, 4, -2, '2021-05-18 04:07:10'),
(38, 3, 3, -1, '2021-05-18 04:18:03'),
(39, 3, 2, 1, '2021-05-18 06:05:46'),
(40, 6, 2, 0, '2021-05-18 06:06:57'),
(41, 6, 2, 1, '2021-05-18 06:07:01'),
(42, 6, 2, 1, '2021-05-18 06:07:52'),
(43, 3, 1, -1, '2021-06-26 09:35:28');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(120) COLLATE utf8_bin DEFAULT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('124bf67c82b262d16afedd57caf79258', '5.245.109.111', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', 1628684434, 'a:9:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:2:\"13\";s:9:\"user_name\";s:21:\"موظف مبيعات\";s:10:\"user_phone\";s:4:\"1234\";s:14:\"user_branch_id\";s:1:\"6\";s:10:\"user_group\";s:1:\"1\";s:10:\"user_email\";s:20:\"sal_employee@f4h.com\";s:9:\"logged_in\";b:1;s:3:\"lan\";s:2:\"ar\";}'),
('20024ba62faec12940085918eaea2fe5', '77.232.124.207', 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_6 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.1 Mobile/15', 1628691649, 'a:9:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:2:\"13\";s:9:\"user_name\";s:21:\"موظف مبيعات\";s:10:\"user_phone\";s:4:\"1234\";s:14:\"user_branch_id\";s:1:\"6\";s:10:\"user_group\";s:1:\"1\";s:10:\"user_email\";s:20:\"sal_employee@f4h.com\";s:9:\"logged_in\";b:1;s:3:\"lan\";s:2:\"ar\";}');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `site_title` text NOT NULL,
  `receipt_start` text,
  `client_point_notification` int(11) NOT NULL DEFAULT '0',
  `product_store_notification` int(11) NOT NULL DEFAULT '0',
  `receipt_sales_start` text NOT NULL,
  `receipt_start_masrofat` varchar(250) DEFAULT NULL,
  `site_desc` text NOT NULL,
  `site_key` text NOT NULL,
  `site_logo` text NOT NULL,
  `site_icon` text NOT NULL,
  `receipt_logo` varchar(250) DEFAULT NULL,
  `site_lang` varchar(10) NOT NULL,
  `occasion_notes_receipt` text,
  `site_phone` text NOT NULL,
  `site_address` text,
  `site_email` text NOT NULL,
  `site_facebok` text NOT NULL,
  `site_twitter` text NOT NULL,
  `site_youtube` text NOT NULL,
  `site_whatsapp` text NOT NULL,
  `site_instagram` text NOT NULL,
  `css` text NOT NULL,
  `js` text NOT NULL,
  `copy_right` text NOT NULL,
  `type` int(11) NOT NULL,
  `commission` float NOT NULL,
  `tax` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `site_title`, `receipt_start`, `client_point_notification`, `product_store_notification`, `receipt_sales_start`, `receipt_start_masrofat`, `site_desc`, `site_key`, `site_logo`, `site_icon`, `receipt_logo`, `site_lang`, `occasion_notes_receipt`, `site_phone`, `site_address`, `site_email`, `site_facebok`, `site_twitter`, `site_youtube`, `site_whatsapp`, `site_instagram`, `css`, `js`, `copy_right`, `type`, `commission`, `tax`) VALUES
(1, 'قصر سلطانة', 'Sultana', 55, 15, 'sales', 'masrofat', 'موقع تصميم وبرمجة مواقع', 'تصميم وبرمجة مواقع, تطبيقات الجوال', '36bce4fe5d44bfd411ca15882c63d03f.png', 'fav.png', 'http://demo.f4h.com.sa/sultana/public/assets/images/logo.jpg', 'ar', 'الأضحى', '0532888896', 'الرياض', 'info@arabiacode.com', 'qaser_sultana', 'twitter', 'facebook', '0532888896', 'qaser_sultana', 'fghgfh', 'fghgfh', 'جميع الحقوق محفوظه لشركة ارابيا كود', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `social`
--

CREATE TABLE `social` (
  `id` int(11) NOT NULL,
  `facebook` text NOT NULL,
  `twitter` text NOT NULL,
  `youtube` text NOT NULL,
  `instagram` text NOT NULL,
  `linkedin` text NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `social`
--

INSERT INTO `social` (`id`, `facebook`, `twitter`, `youtube`, `instagram`, `linkedin`, `store_id`) VALUES
(1, 'Facebook', 'Twitter', 'Youtube', 'Instagram', 'Linkedin', 0),
(2, 'Facebook 2', 'Twitter 2', 'Youtube 2', 'Instagram 2', 'Linkedin 2', 8),
(3, 'Facebook', '', '', '', '', 9);

-- --------------------------------------------------------

--
-- Table structure for table `storequantity`
--

CREATE TABLE `storequantity` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` text,
  `3_quantity` float DEFAULT '0',
  `3_samples` float DEFAULT '0',
  `3_notification` int(11) NOT NULL DEFAULT '0',
  `3_notification_state` int(11) NOT NULL DEFAULT '1',
  `6_quantity` float DEFAULT '0',
  `6_samples` float DEFAULT '0',
  `6_notification` float DEFAULT '0',
  `6_notification_state` float DEFAULT '1',
  `7_quantity` float DEFAULT '0',
  `7_samples` float DEFAULT '0',
  `7_notification` float DEFAULT '0',
  `7_notification_state` float DEFAULT '1',
  `8_quantity` float DEFAULT '0',
  `8_samples` float DEFAULT '0',
  `8_notification` float DEFAULT '0',
  `8_notification_state` float DEFAULT '1',
  `9_quantity` float DEFAULT '0',
  `9_samples` float DEFAULT '0',
  `9_notification` float DEFAULT '0',
  `9_notification_state` float DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `storequantity`
--

INSERT INTO `storequantity` (`id`, `product_id`, `product_name`, `3_quantity`, `3_samples`, `3_notification`, `3_notification_state`, `6_quantity`, `6_samples`, `6_notification`, `6_notification_state`, `7_quantity`, `7_samples`, `7_notification`, `7_notification_state`, `8_quantity`, `8_samples`, `8_notification`, `8_notification_state`, `9_quantity`, `9_samples`, `9_notification`, `9_notification_state`) VALUES
(9, 1, 'لوحة مفاتيح', 1000, 150, 4, 0, 71, 0, 0, 1, 60, 20, 0, 1, 0, 0, 0, 1, 248, 0, 0, 1),
(10, 2, 'ماوس', 619, 101, 9, 1, 73, 32, 15, 1, 35, 5, 0, 1, 5, 4, 0, 1, 0, 0, 0, 0),
(11, 3, 'عطر Joy Dior', 1970, 2, 10, 1, 39, 110, 4, 1, 15, 10, 0, 1, 0, 0, 0, 1, 28, 0, 0, 0),
(12, 4, 'عطر الورود', 1761, 3, 18, 0, 13, 15, 0, 1, 140, 20, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(13, 5, 'عطر الذهبي', 11, 5, 8, 0, 40, 0, 0, 1, 20, 3, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(14, 6, 'كريك', 73, 0, 60, 1, 30, 30, 0, 1, 8, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(15, 7, 'برويطة', 71, 0, 23, 0, 74, 0, 0, 1, 7, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(16, 8, 'ماء اكوافينا', 106, 0, 5, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(17, 9, 'ماء اكوافينا 1', 64, 10, 90, 0, 50, 10, 0, 1, 10, 10, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(18, 10, 'تجربة 5', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(19, 11, 'مربع', 7, 0, 10, 0, 10, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(20, 12, 'عطر حسن', 45, 5, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(21, 13, 'محمد', 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(22, 14, 'امنتي', 19, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(23, 15, 'سيارة', 9, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(24, 16, 'sajed', 30, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(25, 17, 'dggdfhfg', 39, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(26, 18, 'e', 49, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(27, 19, 'تجربة', 50, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(28, 20, '5تي اش', 197, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(29, 21, 'هاي كلاس نوير', 1194, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(30, 22, 'عرض افتتاح القارة', 9999, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(31, 23, 'فلورز', 986, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(32, 24, 'تجربة 60', 345345, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(33, 25, 'ee', 975, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(34, 26, 'ايس', 99918, 10, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `phone` text NOT NULL,
  `address` text NOT NULL,
  `email` varchar(350) NOT NULL,
  `password` varchar(50) NOT NULL,
  `gendar` text NOT NULL,
  `birth_date` text NOT NULL,
  `city` text NOT NULL,
  `state` text NOT NULL,
  `profile` text NOT NULL,
  `ref_code` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `user_group` int(11) NOT NULL,
  `created_date` text NOT NULL,
  `tax_number` varchar(1000) DEFAULT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `phone`, `address`, `email`, `password`, `gendar`, `birth_date`, `city`, `state`, `profile`, `ref_code`, `type`, `user_group`, `created_date`, `tax_number`, `is_active`) VALUES
(1, 'فانتاستك لتقنية المعلومات', '123456789', 'مكة', 'ert@jhgff.dgh', '', '', '', '', '', '', 0, 0, 0, '2020-09-08 09:21:28', '565656', 1),
(2, 'فانتاستك للنظم الإدارية', '54321', 'الرياض', 'sf@dt.tyjj', '', '', '', '', '', '', 0, 0, 0, '2020-09-08 09:22:32', NULL, 1),
(3, 'فانتاستك للإقتصاد', '989898', 'الرياض', 'dfg@jhgfg.dfg', '', '', '', '', '', '', 0, 0, 0, '2020-09-08 09:23:17', NULL, 1),
(4, 'فانتاستك للعلوم والإحصاء', '765765', 'الرياض', 'sd@dg.fghgh', '', '', '', '', '', '', 0, 0, 0, '2020-09-08 09:24:39', NULL, 1),
(5, 'حسن السلطان ', '050000000', 'الفيصل', 'hasssan112233642@gmail.com', '', '', '', '', '', '', 0, 0, 0, '2021-02-21 13:41:29', '200359736664664', 1),
(6, 'فرنسا', '05000000000', 'الفيصل', 'a_elhaj@outlook.com', '', '', '', '', '', '', 0, 0, 0, '2021-03-01 14:17:55', NULL, 1),
(7, 'جديد رقم ضريبي', '0568924004', 'bb', 'eng_ahmedereazzar_fci@yahoo.com', '', '', '', '', '', '', 0, 0, 0, '2021-03-24 19:34:43', 'رقم الضريب', 1),
(8, 'حسن السلطان ', '0531888198', 'الفيصل', 'hasssan112233642@gmail.com', '', '', '', '', '', '', 0, 0, 0, '2021-05-06 19:02:36', '1235153131313', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tax`
--

CREATE TABLE `tax` (
  `id` int(11) NOT NULL,
  `name` text,
  `value` float DEFAULT NULL,
  `comment` text,
  `is_active` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax`
--

INSERT INTO `tax` (`id`, `name`, `value`, `comment`, `is_active`) VALUES
(1, 'قيمة مضافة', 0.15, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(350) NOT NULL,
  `phone` varchar(125) NOT NULL,
  `email` varchar(255) NOT NULL,
  `user_hash` text NOT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `user_group` int(11) NOT NULL,
  `forgot_password` int(11) NOT NULL,
  `created_date` text NOT NULL,
  `is_active` varchar(3) NOT NULL COMMENT 'yes or no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `phone`, `email`, `user_hash`, `branch_id`, `user_group`, `forgot_password`, `created_date`, `is_active`) VALUES
(9, 'فانتاستك', '01001647527', 'f4h@f4h.com', '8cb2237d0679ca88db6464eac60da96345513964', 3, 3, 129972, '0', '1'),
(10, 'مدير فرع', '123456789', 'branch_manager@j.com', '8cb2237d0679ca88db6464eac60da96345513964', 3, 2, 123456, '2020-07-05 17:28:07', '1'),
(11, 'مدير فرع 1', '333', 'manager1@f4h.com', '8cb2237d0679ca88db6464eac60da96345513964', 6, 2, 123456, '2020-07-08 23:35:27', '1'),
(12, 'مدير فرع 2', '1234', 'manager2@f4h.com', '8cb2237d0679ca88db6464eac60da96345513964', 7, 2, 123456, '2020-07-08 23:36:12', '1'),
(13, 'موظف مبيعات', '1234', 'sal_employee@f4h.com', '8cb2237d0679ca88db6464eac60da96345513964', 6, 1, 0, '2020-09-06 18:00:24', '1'),
(14, 'حسن', '0595012300', 'hassan@gmail.com', '8cb2237d0679ca88db6464eac60da96345513964', 6, 1, 0, '2020-11-16 10:04:15', '1'),
(15, 'احمد الحاج', '0540052287', 'a_elhaj@outlook.com', '3a1c1c47d4210d5e2e0bfd630f967d378f905155', 9, 2, 0, '2021-03-01 16:07:00', '1'),
(16, 'حسن السلطان ', '0595012300', 'hassan1122336@hotmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 9, 1, 0, '2021-06-26 17:07:54', '1');

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `id` int(11) NOT NULL,
  `title` varchar(350) NOT NULL,
  `type` int(11) NOT NULL,
  `is_user` int(11) NOT NULL,
  `is_user_view` int(11) NOT NULL,
  `is_user_add` int(11) NOT NULL,
  `is_user_edit` int(11) NOT NULL,
  `is_user_delete` int(11) NOT NULL,
  `is_client` int(11) NOT NULL,
  `is_client_view` int(11) NOT NULL,
  `is_client_add` int(11) NOT NULL,
  `is_client_edit` int(11) NOT NULL,
  `is_client_delete` int(11) NOT NULL,
  `is_page` int(11) NOT NULL,
  `is_page_view` int(11) NOT NULL,
  `is_page_add` int(11) NOT NULL,
  `is_page_edit` int(11) NOT NULL,
  `is_page_delete` int(11) NOT NULL,
  `is_category` int(11) NOT NULL,
  `is_category_view` int(11) NOT NULL,
  `is_category_add` int(11) NOT NULL,
  `is_category_edit` int(11) NOT NULL,
  `is_category_delete` int(11) NOT NULL,
  `is_shop` int(11) NOT NULL,
  `is_shop_view` int(11) NOT NULL,
  `is_shop_add` int(11) NOT NULL,
  `is_shop_edit` int(11) NOT NULL,
  `is_shop_delete` int(11) NOT NULL,
  `is_product` int(11) NOT NULL,
  `is_product_view` int(11) NOT NULL,
  `is_product_add` int(11) NOT NULL,
  `is_product_edit` int(11) NOT NULL,
  `is_product_delete` int(11) NOT NULL,
  `is_order` int(11) NOT NULL,
  `is_order_view` int(11) NOT NULL,
  `is_order_edit` int(11) NOT NULL,
  `is_order_delete` int(11) NOT NULL,
  `is_report` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`id`, `title`, `type`, `is_user`, `is_user_view`, `is_user_add`, `is_user_edit`, `is_user_delete`, `is_client`, `is_client_view`, `is_client_add`, `is_client_edit`, `is_client_delete`, `is_page`, `is_page_view`, `is_page_add`, `is_page_edit`, `is_page_delete`, `is_category`, `is_category_view`, `is_category_add`, `is_category_edit`, `is_category_delete`, `is_shop`, `is_shop_view`, `is_shop_add`, `is_shop_edit`, `is_shop_delete`, `is_product`, `is_product_view`, `is_product_add`, `is_product_edit`, `is_product_delete`, `is_order`, `is_order_view`, `is_order_edit`, `is_order_delete`, `is_report`) VALUES
(2, 'مدير فرع', 2, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(1, 'موظف مبيعات', 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(3, 'مدير رئيسي', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `complaints`
--
ALTER TABLE `complaints`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fav`
--
ALTER TABLE `fav`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masrofat_receipt`
--
ALTER TABLE `masrofat_receipt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masrofat_receiptdetails`
--
ALTER TABLE `masrofat_receiptdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer`
--
ALTER TABLE `offer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers_pieaceprice`
--
ALTER TABLE `offers_pieaceprice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers_products`
--
ALTER TABLE `offers_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer_groups`
--
ALTER TABLE `offer_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer_types`
--
ALTER TABLE `offer_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_deps`
--
ALTER TABLE `products_deps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchases_receipt`
--
ALTER TABLE `purchases_receipt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchases_receiptdetails`
--
ALTER TABLE `purchases_receiptdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quantity_request`
--
ALTER TABLE `quantity_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quantity_requestdetails`
--
ALTER TABLE `quantity_requestdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quantity_request_state`
--
ALTER TABLE `quantity_request_state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_receipt`
--
ALTER TABLE `sales_receipt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_receiptdetails`
--
ALTER TABLE `sales_receiptdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `samp_quant_movement`
--
ALTER TABLE `samp_quant_movement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social`
--
ALTER TABLE `social`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `storequantity`
--
ALTER TABLE `storequantity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax`
--
ALTER TABLE `tax`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `complaints`
--
ALTER TABLE `complaints`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fav`
--
ALTER TABLE `fav`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `masrofat_receipt`
--
ALTER TABLE `masrofat_receipt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `masrofat_receiptdetails`
--
ALTER TABLE `masrofat_receiptdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=462;

--
-- AUTO_INCREMENT for table `offer`
--
ALTER TABLE `offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `offers_pieaceprice`
--
ALTER TABLE `offers_pieaceprice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `offers_products`
--
ALTER TABLE `offers_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `offer_groups`
--
ALTER TABLE `offer_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `offer_types`
--
ALTER TABLE `offer_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `products_deps`
--
ALTER TABLE `products_deps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `purchases_receipt`
--
ALTER TABLE `purchases_receipt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `purchases_receiptdetails`
--
ALTER TABLE `purchases_receiptdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `quantity_request`
--
ALTER TABLE `quantity_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `quantity_requestdetails`
--
ALTER TABLE `quantity_requestdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `quantity_request_state`
--
ALTER TABLE `quantity_request_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `rate`
--
ALTER TABLE `rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sales_receipt`
--
ALTER TABLE `sales_receipt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=219;

--
-- AUTO_INCREMENT for table `sales_receiptdetails`
--
ALTER TABLE `sales_receiptdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=268;

--
-- AUTO_INCREMENT for table `samp_quant_movement`
--
ALTER TABLE `samp_quant_movement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `social`
--
ALTER TABLE `social`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `storequantity`
--
ALTER TABLE `storequantity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tax`
--
ALTER TABLE `tax`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
