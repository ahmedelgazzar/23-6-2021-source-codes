-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 04, 2021 at 12:15 AM
-- Server version: 5.7.10-log
-- PHP Version: 5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `order`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_categories`
--

CREATE TABLE `activity_categories` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `content` text,
  `content_en` varchar(250) DEFAULT NULL,
  `photo` text NOT NULL,
  `is_active` int(3) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activity_categories`
--

INSERT INTO `activity_categories` (`id`, `title`, `title_en`, `content`, `content_en`, `photo`, `is_active`, `is_deleted`, `date_add`) VALUES
(1, 'المطاعم', NULL, 'المطاعم', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/15772836555800.jpg', 1, 0, '2020-09-08 08:55:18'),
(2, 'المقاهى', NULL, 'المقاهى', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/hqdefault.jpg', 1, 0, '2020-09-08 08:57:07'),
(3, 'محلات الحلويات', NULL, 'محلات الحلويات', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/720181413512197617633.jpg', 1, 0, '2020-09-08 08:58:43'),
(4, 'محلات الأيس كريم', NULL, 'محلات الأيس كريم', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/Picture1.jpg', 1, 0, '2021-02-05 09:12:44'),
(5, 'المخابز', NULL, 'المخابز', NULL, '', 1, 0, '2021-02-28 18:12:29'),
(6, 'المطابخ وتجهيز الحفلات', NULL, 'المطابخ وتجهيز الحفلات', NULL, '', 1, 0, '2021-04-05 18:49:45');

-- --------------------------------------------------------

--
-- Table structure for table `activity_subcategories`
--

CREATE TABLE `activity_subcategories` (
  `id` int(11) NOT NULL,
  `activity_category_id` int(11) DEFAULT '1',
  `title` text NOT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `content` text,
  `content_en` varchar(250) DEFAULT NULL,
  `photo` text NOT NULL,
  `is_active` int(3) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activity_subcategories`
--

INSERT INTO `activity_subcategories` (`id`, `activity_category_id`, `title`, `title_en`, `content`, `content_en`, `photo`, `is_active`, `is_deleted`, `date_add`) VALUES
(1, 1, 'المطاعم', NULL, 'المطاعم', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/15772836555800.jpg', 1, 0, '2020-09-08 08:55:18'),
(2, 1, 'المقاهى', NULL, 'المقاهى', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/hqdefault.jpg', 1, 0, '2020-09-08 08:57:07'),
(3, 1, 'محلات الحلويات', NULL, 'محلات الحلويات', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/720181413512197617633.jpg', 1, 0, '2020-09-08 08:58:43'),
(4, 1, 'محلات الأيس كريم', NULL, 'محلات الأيس كريم', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/Picture1.jpg', 1, 0, '2021-02-05 09:12:44'),
(5, 1, 'المخابز', NULL, 'المخابز', NULL, '', 1, 0, '2021-02-28 18:12:29'),
(6, 1, 'المطابخ وتجهيز الحفلات', NULL, 'المطابخ وتجهيز الحفلات', NULL, '', 1, 0, '2021-04-05 18:49:45');

-- --------------------------------------------------------

--
-- Table structure for table `captains`
--

CREATE TABLE `captains` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `iban` text,
  `nationality` text,
  `national_id` varchar(250) NOT NULL DEFAULT '0',
  `phone` text NOT NULL,
  `password` varchar(50) NOT NULL,
  `birth_date` text,
  `car_panel_number` varchar(250) DEFAULT NULL,
  `car_type` varchar(250) DEFAULT NULL,
  `bank_name` varchar(250) DEFAULT NULL,
  `city` text NOT NULL,
  `invitation_code` text,
  `province` varchar(250) DEFAULT NULL,
  `personal_photo` varchar(250) DEFAULT NULL,
  `national_id_photo` varchar(250) DEFAULT NULL,
  `driver_card_id` varchar(250) DEFAULT NULL,
  `car_card_id` varchar(250) DEFAULT NULL,
  `insurance_card` varchar(250) DEFAULT NULL,
  `car_front_photo` varchar(250) DEFAULT NULL,
  `car_back_photo` varchar(250) DEFAULT NULL,
  `ref_code` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '2',
  `user_group` int(11) NOT NULL,
  `created_date` text NOT NULL,
  `is_active` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `captains`
--

INSERT INTO `captains` (`id`, `name`, `last_name`, `email`, `iban`, `nationality`, `national_id`, `phone`, `password`, `birth_date`, `car_panel_number`, `car_type`, `bank_name`, `city`, `invitation_code`, `province`, `personal_photo`, `national_id_photo`, `driver_card_id`, `car_card_id`, `insurance_card`, `car_front_photo`, `car_back_photo`, `ref_code`, `type`, `user_group`, `created_date`, `is_active`, `is_deleted`) VALUES
(1, 'سائق 1', '9876', 'fcv@hsfg.gh', NULL, NULL, '966555555555', '966555555555', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, 2, 0, '2021-03-22 20:33:35', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `receipt_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `final_item_price` float NOT NULL DEFAULT '0',
  `product_options` text,
  `sub_product_options` text,
  `offer_id` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT '1',
  `comment` text,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `receipt_id`, `client_id`, `company_id`, `product_id`, `amount`, `final_item_price`, `product_options`, `sub_product_options`, `offer_id`, `state`, `comment`, `created_date`) VALUES
(18, NULL, 0, 1, 2, 1, 10, '[]', '[]', NULL, 1, '', '2021-09-02 07:16:20'),
(20, NULL, 11, 1, 2, 1, 10, '', '', NULL, 1, '', '2021-09-03 09:57:26'),
(21, NULL, 11, 1, 3, 1, 300, '', '', NULL, 1, '', '2021-09-03 09:57:37'),
(22, NULL, 11, 1, 7, 1, 300, '', '', NULL, 1, '', '2021-09-03 09:58:07');

-- --------------------------------------------------------

--
-- Table structure for table `car_type`
--

CREATE TABLE `car_type` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `title_en` text,
  `is_active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car_type`
--

INSERT INTO `car_type` (`id`, `title`, `title_en`, `is_active`) VALUES
(1, 'مؤقت', NULL, 1),
(2, 'دبلوماسي', NULL, 1),
(3, 'خاص', NULL, 1),
(4, 'نقل', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `photo` varchar(250) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `title`, `photo`, `is_active`) VALUES
(19, 'الأحساء', NULL, 1),
(20, 'رفحاء', NULL, 1),
(21, 'حفر الباطن', NULL, 1),
(22, 'حائل', NULL, 1),
(23, 'القريات', NULL, 1),
(24, 'عرعر', NULL, 1),
(25, 'الجوف (سكاكا)', NULL, 1),
(26, 'تبوك', 'tabuk.png', 1),
(27, 'شرق الرياض', NULL, 1),
(28, 'غرب الرياض', NULL, 1),
(29, 'وسط الرياض', NULL, 1),
(30, 'شمال الرياض', NULL, 1),
(31, 'جنوب الرياض', NULL, 1),
(32, 'محافظة الخرج والدلم', NULL, 1),
(33, 'محافظات منطقة القصيم', NULL, 1),
(34, 'محافظات منطقة الرياض', NULL, 1),
(35, 'رابغ', NULL, 1),
(36, 'ينبع', NULL, 1),
(37, 'مكة المكرمة', NULL, 1),
(38, 'جدة', NULL, 1),
(39, 'القنفدة', NULL, 1),
(40, 'الطائف', NULL, 1),
(41, 'المدينة المنورة', NULL, 1),
(42, 'بقيق', NULL, 1),
(43, 'النعيرية', NULL, 1),
(44, 'الخفجي', NULL, 1),
(45, 'الجبيل', NULL, 1),
(46, 'رأس تنورة', NULL, 1),
(47, 'الخبر', NULL, 1),
(48, 'القطيف', NULL, 1),
(49, 'حفر الباطن', NULL, 1),
(50, 'سيهات', NULL, 1),
(51, 'الدمام', NULL, 1),
(52, 'الظهران', NULL, 1),
(53, 'طريف', NULL, 1),
(54, 'صفوى', NULL, 1),
(55, 'محايل عسير', NULL, 1),
(56, 'خميس مشيط', NULL, 1),
(57, 'الباحة', NULL, 1),
(58, 'شرورة', NULL, 1),
(59, 'جازان', NULL, 1),
(60, 'أبها', NULL, 1),
(61, 'نجران', NULL, 1),
(62, 'بيشة', NULL, 1),
(65, 'املج', 'amlag.png', 1),
(66, 'ضبا', 'daba.png', 1),
(67, 'الوجه', 'elogah.png', 1),
(68, 'حقل', 'haqle.png', 1),
(71, 'بئر ما', 'sharma.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `name` text,
  `name_en` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `phone` text NOT NULL,
  `address` text NOT NULL,
  `email` varchar(350) NOT NULL,
  `password` varchar(50) NOT NULL,
  `ref_code` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `created_date` text NOT NULL,
  `is_active` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `name`, `name_en`, `last_name`, `phone`, `address`, `email`, `password`, `ref_code`, `type`, `created_date`, `is_active`, `is_deleted`) VALUES
(2, 'aa', 'Ahmed Elgazzar', 'k', '966548270331', '', 'cc', '7c4a8d09ca3762af61e59520943dc26494f8941b', 3373, 1, '2021-07-08 23:39:11', 1, 0),
(3, 'Sajed', 'Elgazzar', NULL, '0123456789v', '', '', '7c4a8d09ca3762af61e59520943dc26494f8941b', 4622, 1, '2021-07-08 23:41:08', 1, 0),
(5, 'Sajed', NULL, 'Elgazzar', '44', '', 'ccc', '7c4a8d09ca3762af61e59520943dc26494f8941b', 5279, 1, '2021-08-14 22:13:46', 1, 0),
(6, 'Sajed', NULL, 'Elgazzar', '4455', '', 'cccc', '8cb2237d0679ca88db6464eac60da96345513964', 7875, 1, '2021-08-16 12:53:39', 1, 0),
(7, 'iOS', NULL, 'App', '123', '', 'test@test.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 9464, 1, '2021-08-17 15:41:18', 1, 0),
(8, 'Abdallah', NULL, 'badawy ', '966562021500', '', 'tset@test.com', '20eabe5d64b0e216796e834f52d61fd0b70332fc', 8510, 1, '2021-08-17 16:11:18', 1, 0),
(9, 'Sajed', NULL, 'Elgazzar', '445566', '', 'cccjgkyc', '7c4a8d09ca3762af61e59520943dc26494f8941b', 5066, 1, '2021-08-19 08:55:22', 1, 0),
(10, 'Sajed', NULL, '', '44566566', '', 'cccj66gkyc', '7c4a8d09ca3762af61e59520943dc26494f8941b', 4647, 1, '2021-08-19 08:55:51', 1, 0),
(11, 'Mohamad Jaad ', NULL, ' ', '966555555556', '', 'jaadmohamad@yahoo.com', '3d4f2bf07dc1be38b20cd6e46949a1071f9d0e3d', 1473, 1, '2021-08-19 08:58:20', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `client_address`
--

CREATE TABLE `client_address` (
  `id` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `descrption` text,
  `client_id` int(11) DEFAULT NULL,
  `latitude` text,
  `longitude` text,
  `is_default` int(11) DEFAULT '0',
  `is_deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_address`
--

INSERT INTO `client_address` (`id`, `title`, `descrption`, `client_id`, `latitude`, `longitude`, `is_default`, `is_deleted`) VALUES
(10, '7t', '6d', 2, '31.23527980044717', '31.23527980044717', 1, 0),
(8, '7t', '6d', 6, 'la', 'lo', 66, 1),
(7, '7', '6', 2, 'ffgghh', 'ffgghh', 0, 0),
(11, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(12, '7', '6', 2, 'ffgghh', 'ffgghh', 1, 0),
(13, '7', '6', 2, 'ffgghh', 'ffgghh', 0, 1),
(14, '7', '6', 2, 'ffgghh', 'ffgghh', 0, 0),
(15, '7', '6', 2, 'ffgghh', 'ffgghh', 0, 0),
(16, 'كفر أشليم', 'G46C+MHW، كفر أشليم، مركز قويسنا، المنوفية، مصر', 11, '30.5117951', '31.1217323', 1, 0),
(17, 'قسم بنها', 'النساج، قسم بنها، بنها، القليوبية، مصر', 11, '30.46175543805246', '31.183209456503388', 0, 1),
(18, 'الرملة', 'الرملة، بنها،، الرملة، بنها، القليوبية،، الرملة، بنها، القليوبية، مصر', 11, '30.442835266111317', '31.16414397954941', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `client_payment_cards`
--

CREATE TABLE `client_payment_cards` (
  `id` int(11) NOT NULL,
  `card_number` varchar(250) DEFAULT NULL,
  `cvv` varchar(250) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `expire_date` varchar(250) DEFAULT NULL,
  `is_remember` int(11) DEFAULT '0',
  `is_deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_payment_cards`
--

INSERT INTO `client_payment_cards` (`id`, `card_number`, `cvv`, `client_id`, `name`, `expire_date`, `is_remember`, `is_deleted`) VALUES
(10, '7t', '6d', 2, 'la', 'lo', 1, 0),
(8, '7t', '6d', 6, 'la', 'lo', 66, 0),
(7, '7', '6', 2, 'ffgghh', 'ffgghh', 0, 1),
(12, '3453645654', '666', 88, 'شاةثي', '5-9', 1, 0),
(13, '3453645654', '666', 88, 'شاةثي', '5-9', 1, 0),
(14, '3453645654', '666', 2, 'شاةثي', '5-9', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `message` longtext NOT NULL,
  `msg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `phone`, `message`, `msg_date`) VALUES
(1, '24543534', 'branch_manager@jnm.com', 'branch_manager@jnm.c', 'لبابلا', '2020-07-21 04:58:45'),
(11, '7', '6', '2', 'ffgghh', '2021-08-06 12:39:21'),
(12, '7', '6', '2', 'ffgghh', '2021-08-18 20:32:27'),
(13, '7', '6', '2', 'ffgghh', '2021-09-03 16:54:20');

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE `coupon` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `num` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `coupon`
--

INSERT INTO `coupon` (`id`, `title`, `num`) VALUES
(4, '78910', 0.2),
(5, 'rrtret', 0.5);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(11) NOT NULL,
  `media` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `media`) VALUES
(2, 'images.png'),
(4, 'company_owner.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `offer`
--

CREATE TABLE `offer` (
  `id` int(11) NOT NULL,
  `order_company_id` int(11) NOT NULL,
  `name` text,
  `offer_type` varchar(150) DEFAULT NULL,
  `price_topercentage_offer` float DEFAULT '1' COMMENT 'السعر الذى اذا تم حسابه فى السلة يتم تطبيق عرض خصم بنسبة',
  `percentage` float DEFAULT '0' COMMENT 'النسبة فى عرض خصم بنسبة',
  `items_number` float DEFAULT NULL COMMENT 'عدد المنتجات فى عرض اشترى كذا منتج والأخر مجانا',
  `shipping_discount_percentage` int(11) NOT NULL DEFAULT '0' COMMENT 'عرض التوصيل المدعوم بنسبة كم من قيمة التوصيل',
  `shipping_discount_price` int(11) NOT NULL DEFAULT '0' COMMENT 'عرض التوصيل المدعوم قيمة ثابتة',
  `itemnumber_inproductfinalcart` int(11) NOT NULL DEFAULT '0' COMMENT 'عدد المنتجات فى عرض اشترى كذا منتج والأخر مجانا على السلة النهائية',
  `comment` text,
  `created_date` text,
  `is_active` int(11) DEFAULT NULL,
  `in_offerslide` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offer`
--

INSERT INTO `offer` (`id`, `order_company_id`, `name`, `offer_type`, `price_topercentage_offer`, `percentage`, `items_number`, `shipping_discount_percentage`, `shipping_discount_price`, `itemnumber_inproductfinalcart`, `comment`, `created_date`, `is_active`, `in_offerslide`) VALUES
(1, 1, 'عرض خصم بنسبة', '1', 0, 0.25, 0, 0, 0, 0, '', '2020-08-23 16:00:42', 1, 1),
(2, 1, 'منتجين والثالث مجانا', '2', 0, 0, 2, 0, 0, 0, NULL, NULL, 1, 1),
(3, 1, 'منتج ا مع منتج ب', '3', 0, 0, 0, 0, 0, 0, NULL, NULL, 1, 1),
(4, 1, 'توصيل مدعوم', '4', 0, 0, 0, 40, 0, 0, NULL, NULL, 1, 1),
(5, 1, 'منتجين والثالث مجانا فى السلة النهائية', '5', 0, 0, 2, 0, 0, 12, NULL, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `offer_groups`
--

CREATE TABLE `offer_groups` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `products_in_group` text,
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offer_groups`
--

INSERT INTO `offer_groups` (`id`, `title`, `products_in_group`, `date_add`) VALUES
(3, 'الحديقة 3', '12,11,10,8,7,4,3,2,1', '2020-09-08 08:58:43'),
(4, 'المجموعة 2', '18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1', '2021-01-08 09:37:36'),
(6, 'الكل', '26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1', '2021-03-08 11:31:24'),
(9, 'أهل الرياض', '21,20,19', '2021-03-08 11:43:33'),
(10, 'مجموعة المدرسين', '15,14', '2021-03-08 11:46:56'),
(11, 'مجموعة العطور', '15,14,13,12', '2021-03-08 11:49:18'),
(12, 'العطور', '16,15,14', '2021-03-08 11:53:46'),
(13, 'العطور الشرقية', '5,4,3', '2021-03-08 22:04:00');

-- --------------------------------------------------------

--
-- Table structure for table `offer_types`
--

CREATE TABLE `offer_types` (
  `id` int(11) NOT NULL,
  `name` text,
  `comment` text,
  `img` varchar(250) DEFAULT NULL,
  `created_date` text,
  `is_active` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offer_types`
--

INSERT INTO `offer_types` (`id`, `name`, `comment`, `img`, `created_date`, `is_active`) VALUES
(1, 'نسبة معينة', 'مثال خصم 50 % أو 25 % على كل المنتجات فى القائمة', NULL, NULL, 1),
(2, 'product and product', 'اشتر منتجين والثالث مجانا او الثالث بنصف\nالسعر او سعر يحدده التاجر , قابل للتحديد مثل ان يشترط التاجر\nان تكون جميعها من نفس الصنف', NULL, NULL, 1),
(3, 'extra_product', 'شراء منتج من الصنف أ عندها تحصل على منتج من\nالصنف ب', NULL, NULL, 1),
(4, 'discountshipping', 'التوصيل المدعوم ) يتحمل التاجر تكلفة التوصيل كاملة او جزء\nمنها ) مثال خصم 51 % على التوصيل او التوصيل ب 5 ريال فقط', NULL, NULL, 1),
(5, 'product and product_infinalcart', 'اشتر منتجين والثالث مجانا او الثالث بنصف\nالسعر او سعر يحدده التاجر , قابل للتحديد مثل ان يشترط التاجر\nان تكون على السلة كاملة\nويكون العرض على الأقل سعرا', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ordertime_open_close`
--

CREATE TABLE `ordertime_open_close` (
  `id` int(11) NOT NULL,
  `order_company_id` int(11) DEFAULT NULL,
  `day` varchar(250) DEFAULT NULL,
  `open` time DEFAULT NULL,
  `close` time DEFAULT NULL,
  `state_now` varchar(250) DEFAULT NULL,
  `is_active` int(3) NOT NULL DEFAULT '1',
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ordertime_open_close`
--

INSERT INTO `ordertime_open_close` (`id`, `order_company_id`, `day`, `open`, `close`, `state_now`, `is_active`, `date_add`) VALUES
(7, 1, 'saturday', '08:00:00', '23:00:00', NULL, 1, '2021-07-11 15:46:53'),
(8, 1, 'sunday', '08:00:00', '23:00:00', NULL, 1, '2021-07-11 15:46:53'),
(9, 1, 'monday', '07:57:00', '23:00:00', NULL, 1, '2021-07-11 15:46:53'),
(10, 1, 'tuesday', '08:00:00', '23:00:00', NULL, 1, '2021-07-11 15:46:53'),
(11, 1, 'wednesday', '08:00:00', '23:00:00', NULL, 1, '2021-07-11 15:46:53'),
(12, 1, 'thursday', '08:00:00', '23:00:00', NULL, 1, '2021-07-11 15:46:53'),
(13, 1, 'friday', '08:00:00', '23:00:00', NULL, 1, '2021-07-11 15:46:53');

-- --------------------------------------------------------

--
-- Table structure for table `order_company`
--

CREATE TABLE `order_company` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `name_en` varchar(250) DEFAULT NULL,
  `logo` text,
  `logo_back` varchar(500) DEFAULT NULL,
  `haweia` varchar(250) DEFAULT NULL,
  `address` text,
  `description` varchar(250) DEFAULT NULL,
  `latitude` varchar(250) DEFAULT NULL,
  `longitude` varchar(250) DEFAULT NULL,
  `phone` text,
  `email` varchar(350) DEFAULT NULL,
  `province` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `activity_category_id` varchar(250) DEFAULT NULL,
  `activity_subcategory_id` varchar(250) DEFAULT NULL,
  `work_time` text,
  `preparing_time_from` varchar(250) DEFAULT NULL,
  `preparing_time_to` varchar(250) DEFAULT NULL,
  `owner_name` varchar(250) DEFAULT NULL,
  `owner_phone` varchar(250) DEFAULT NULL,
  `manager_name` varchar(250) DEFAULT NULL,
  `manager_phone` varchar(250) DEFAULT NULL,
  `segel_togary_number` varchar(250) DEFAULT NULL,
  `segel_togary_photo` varchar(250) DEFAULT NULL,
  `branch_numbers` varchar(250) DEFAULT NULL,
  `detected_number` varchar(250) DEFAULT NULL,
  `bank_account` varchar(250) DEFAULT NULL,
  `has_delivery` varchar(250) DEFAULT NULL,
  `has_another_app` varchar(250) DEFAULT NULL,
  `balance` float NOT NULL DEFAULT '0',
  `password` varchar(50) NOT NULL,
  `forgot_password` text,
  `is_active` int(11) DEFAULT '0',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_company`
--

INSERT INTO `order_company` (`id`, `name`, `name_en`, `logo`, `logo_back`, `haweia`, `address`, `description`, `latitude`, `longitude`, `phone`, `email`, `province`, `city`, `activity_category_id`, `activity_subcategory_id`, `work_time`, `preparing_time_from`, `preparing_time_to`, `owner_name`, `owner_phone`, `manager_name`, `manager_phone`, `segel_togary_number`, `segel_togary_photo`, `branch_numbers`, `detected_number`, `bank_account`, `has_delivery`, `has_another_app`, `balance`, `password`, `forgot_password`, `is_active`, `is_deleted`, `created_date`) VALUES
(1, 'ماكدونالدز', 'Mackdonald', 'http://demo.f4h.com.sa/order/images/companies/1.jpg', 'http://demo.f4h.com.sa/order/images/companies/2.jpg', NULL, 'بنها قليوبية', 'برجر - زجبات سريعة', '30.459673297707113', '31.23527980044717', '0548270331', 'f4hf4hf4h@gmail.com', NULL, NULL, ',1,', '2', '', '30', '60', 'صاحب الشركة', '14033951', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2026, '7c4a8d09ca3762af61e59520943dc26494f8941b', '195297883', 1, 0, '2021-03-21 11:44:11'),
(2, 'أسماك بحرية', 'Fishs', 'http://demo.f4h.com.sa/order/images/companies/2.jpg', 'http://demo.f4h.com.sa/order/images/companies/3.jpg', NULL, 'الرياض', 'أسماك - جمبري', '30.459673297707113', '31.23527980044717', '0548270332', 'station2@gmail.com', NULL, NULL, ',1,', ',2,', '', '20', '40', 'صاحب الشركة', '1403395s2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '7c4a8d09ca3762af61e59520943dc26494f8941b', '545716364', 1, 0, '2021-03-21 11:44:11'),
(3, 'فطائر بلدى', 'backer', 'http://demo.f4h.com.sa/order/images/companies/3.jpg', 'http://demo.f4h.com.sa/order/images/companies/1.jpg', NULL, 'القاهرة', 'فطائر - بيتزا', '30.459673297707113', '30.459673297707113', '0548270333', 'station3@gmail.com', NULL, NULL, '555b46452dd40949416fe4473a4e199b.jpg', ',6,2,7,', '', '40', '80', 'صاحب الشركة', '1403395s3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '7c4a8d09ca3762af61e59520943dc26494f8941b', '545716364', 1, 0, '2021-03-21 11:44:11'),
(4, 'محطة 4', 'ddff', NULL, NULL, NULL, 'عنوان محطة 4', NULL, NULL, NULL, '0548270334', 'station4@gmail.com', NULL, NULL, '555b46452dd40949416fe4473a4e199b.jpg', '2', '', NULL, NULL, 'مسؤل  محطة 4', '1403395s4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '7c4a8d09ca3762af61e59520943dc26494f8941b', '545716364', 1, 0, '2021-03-21 11:44:11'),
(5, 'حطة trial22244', NULL, NULL, NULL, NULL, 'عنوان محطة tri44al', NULL, NULL, NULL, '056789344844', 'f4hss44dff@f4h.com', NULL, NULL, 'e7fbfc05fdc966fd618109d9b9c6fbf4.jpg', NULL, '22334444', NULL, NULL, 'gazzar station44', '1403395strial44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '601f1889667efaebb33b8c12572835da3f027f78', NULL, 1, 0, '2021-05-10 10:29:23'),
(6, 'محطة مهيمن للبنزين الأخضر 91', NULL, NULL, NULL, NULL, '123456', NULL, NULL, NULL, '053سسسسسسس', 'email_test@gmail.com', NULL, NULL, '', NULL, '123', NULL, NULL, '123', 'رقمالهوية', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10700, '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 1, 0, '2021-06-21 03:36:17'),
(9, 'مهيمن للديزل', 'mohaymen', NULL, NULL, NULL, 'الأحساء', NULL, NULL, NULL, '0500000000', 'email_2test@gmail.com', NULL, NULL, 'SNAG-0004.mp4', NULL, '0000', NULL, NULL, 'مهيمن', '12345', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11500, '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 1, 0, '2021-06-21 08:10:18');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `title` varchar(350) NOT NULL,
  `slug` varchar(350) NOT NULL,
  `content` text NOT NULL,
  `status` varchar(100) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `title`, `slug`, `content`, `status`, `meta_keywords`, `meta_description`) VALUES
(10, 'انضم لنا', 'انضم لنا', 'انضم لنا انضم لناانضم لنا انضم لنا انضم لنا', '1', '', ''),
(70, 'سياسة الخصوصية', 'سياسة الخصوصية', 'سياسة الخصوصية سياسة الخصوصية سياسة الخصوصية سياسة الخصوصية سياسة الخصوصية سياسة الخصوصية ', '1', '', ''),
(71, 'الشروط والأحكام', 'الشروط والأحكام', 'الشروط والأحكام الشروط والأحكام الشروط والأحكام الشروط والأحكام الشروط والأحكام الشروط والأحكام الشروط والأحكام الشروط والأحكام الشروط والأحكام الشروط والأحكام ', '1', '', ''),
(72, 'من نحن', 'من_نحن', 'من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن ', '1', 'koko , jpjp', ''),
(73, 'اتصل بنا', 'اتصل_بنا', 'اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا ', '1', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `name_ar` varchar(250) NOT NULL,
  `name_en` varchar(250) NOT NULL,
  `img` varchar(100) NOT NULL,
  `state` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `name_ar`, `name_en`, `img`, `state`) VALUES
(1, 'شريك 1', 'partner 1', 'http://demo.f4h.com.sa/fullpay/images/home/partners/1.jpg', '1'),
(2, 'شريك 2', 'partner 2', 'http://demo.f4h.com.sa/fullpay/images/home/partners/2.jpg', '1'),
(3, 'شريك 3', 'partner 3', 'http://demo.f4h.com.sa/fullpay/images/home/partners/3.jpg', '1');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `product_category_id` int(11) NOT NULL,
  `order_company_id` int(11) NOT NULL,
  `title` varchar(350) NOT NULL,
  `title_en` varchar(100) DEFAULT NULL,
  `barcode_num` text,
  `price` float DEFAULT NULL,
  `photo` text,
  `comment` text,
  `offer_percentage_id` int(11) DEFAULT '0' COMMENT 'اى دى عرض خصم بنسبة',
  `offer_product_andproduct_id` int(11) NOT NULL DEFAULT '0' COMMENT 'اى دى عرض منتج ا ومنتج ب',
  `ab` int(11) NOT NULL DEFAULT '0' COMMENT 'يتم وضع اى دى المنتج الأخر الرئيسي المستخدم فى عرض منتج ا ومنتج ب',
  `ba` int(11) NOT NULL DEFAULT '0' COMMENT 'يتم وضع اى دى المنتج الأخر الفرعىالمستخدم فى عرض منتج ا ومنتج ب',
  `offer_extraproduct_id` int(11) NOT NULL DEFAULT '0' COMMENT 'اى دى عرض منتج وأكثر والخر مجانا (عرض صنف مجانا)',
  `extraproduct_price` int(11) NOT NULL DEFAULT '0' COMMENT 'سعر المنتج فى حالة تطبيق عرض صنف مجانا',
  `offer_extraproductincart_id` int(11) NOT NULL DEFAULT '0' COMMENT 'اى دى عرض منتج وأكثر والخر مجانا (عرض صنف مجانا) فقط فى السلة',
  `offer_discountshipping_id` int(11) NOT NULL COMMENT 'اى دى عرض التوصيل المدعوم',
  `number_of_sells` int(11) NOT NULL DEFAULT '0' COMMENT 'عدد مبيعات المنتج',
  `created_date` text NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_category_id`, `order_company_id`, `title`, `title_en`, `barcode_num`, `price`, `photo`, `comment`, `offer_percentage_id`, `offer_product_andproduct_id`, `ab`, `ba`, `offer_extraproduct_id`, `extraproduct_price`, `offer_extraproductincart_id`, `offer_discountshipping_id`, `number_of_sells`, `created_date`, `is_deleted`, `is_active`) VALUES
(1, 1, 1, 'لوحة مفاتيح', 'KeyBoard', '826876480002', 50, 'http://demo.f4h.com.sa/sultana/images/cp/products/funct.jpg', '', 0, 0, 0, 0, 2, 5, 0, 0, 1, '2020-09-08 09:00:32', 0, 1),
(2, 3, 1, 'ماوس', 'Mouse', '860366564006', 40, 'http://demo.f4h.com.sa/sultana/images/cp/products/HTB1adWlaLfsK1RjSszgq6yXzpXaX.jpg_350x350_.jpg', '', 1, 0, 0, 0, 0, 0, 0, 0, 5, '2020-09-08 09:01:12', 0, 1),
(3, 1, 1, 'عطر Joy Dior', 'عطر Joy Dior', '149301951890', 300, 'http://demo.f4h.com.sa/sultana/images/cp/products/4460791-1230744024.jpg', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-09-08 09:02:15', 0, 1),
(4, 1, 1, 'عطر الورود', 'Rose perfumes', '521558286705', 500, 'http://demo.f4h.com.sa/sultana/images/cp/products/9999026109.jpg', '', 0, 3, 1, 0, 0, 0, 0, 0, 2, '2020-09-08 09:03:13', 0, 1),
(5, 0, 0, 'عطر الذهبي', 'Golden Perfuim', '945767728896', 10, 'http://demo.f4h.com.sa/sultana/images/cp/products/emirates-voiceghgfdf.gif', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-09-08 09:05:03', 0, 1),
(6, 3, 1, 'كريك', 'كريك', '544485425584', 10, 'http://demo.f4h.com.sa/sultana/images/cp/products/YT-8865--1_776x591.jpg', '', NULL, 0, 0, 0, 0, 0, 0, 4, 0, '2020-09-08 09:06:03', 0, 1),
(7, 3, 1, 'برويطة', 'برويطة', '215665963906', 300, 'http://demo.f4h.com.sa/sultana/images/cp/products/bdvth3.jpg', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-09-08 09:07:37', 0, 1),
(8, 0, 1, 'ماء اكوافينا', 'water1', '012000014383', 0, '', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-10-28 20:41:00', 0, 1),
(9, 0, 0, 'ماء اكوافينا 1', 'water2', '6287008660014', 20, '', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-10-28 20:46:23', 0, 1),
(10, 0, 0, 'تجربة 5', 'test5', '102263932p', 200, '', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-10-28 20:48:17', 0, 1),
(11, 5, 1, 'مربع', 'Murabe3', '3850102323036', 10, '', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-10-28 21:06:54', 0, 1),
(12, 5, 1, 'عطر حسن', 'hasssan', '024300044168', 40, '', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-01 15:07:29', 0, 1),
(13, 0, 0, 'محمد', 'hasssan5', '6287008590090', 8, '', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-01 15:40:07', 0, 1),
(14, 0, 0, 'امنتي', 'AIMANTE', '6390902022861', 0, '', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 17:45:36', 0, 1),
(15, 0, 0, 'سيارة', 'car', 'jghdghkjdhgdhg', 345, '', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-24 14:43:16', 0, 1),
(16, 0, 0, 'sajed', '', '411458313199', 30, '', '', NULL, 0, 0, 0, 0, 0, 0, 4, 0, '2021-01-21 17:41:48', 0, 1),
(17, 0, 0, 'dggdfhfg', '', '986386337208', 8, '', '', NULL, 0, 0, 0, 0, 0, 0, 4, 0, '2021-01-21 17:52:33', 0, 1),
(18, 0, 0, 'e', 'we', '600806512854', 10, '', '', NULL, 0, 0, 0, 0, 0, 5, 0, 0, '2021-01-21 17:53:14', 0, 1),
(19, 0, 0, 'تجربة', 'test', '897396663941', 120, 'http://demo.f4h.com.sa/sultana/images/cp/products/س.png', 'vc', NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2021-02-05 08:55:44', 0, 1),
(20, 0, 0, '5تي اش', '5th avenue', '6290845151577', 150, '', 'تكيز80% تاريخ الانتاج2019/05/1', NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2021-02-21 13:40:32', 0, 1),
(21, 0, 0, 'هاي كلاس نوير', 'High Class noir', '6281074713957', 130.43, '', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2021-02-21 13:48:14', 0, 1),
(22, 0, 0, 'عرض افتتاح القارة', '', '564609899590', 52.17, '', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2021-02-21 13:55:46', 0, 1),
(23, 0, 0, 'فلورز', 'flowers', '6287006662294', 134.78, '', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2021-03-01 14:16:14', 0, 1),
(24, 0, 0, 'تجربة 60', 'test60', '339644344406', 52.17, '', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2021-03-03 11:10:15', 0, 1),
(25, 0, 0, 'ee', 'ee', '510750131949', 90, '', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2021-03-03 11:13:59', 0, 1),
(26, 0, 0, 'ايس', 'ice', '6287006662218', 10, '', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2021-04-05 18:50:29', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products_categories`
--

CREATE TABLE `products_categories` (
  `id` int(11) NOT NULL,
  `order_company_id` int(11) DEFAULT NULL,
  `title` text NOT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `content` text,
  `content_en` varchar(250) DEFAULT NULL,
  `photo` text NOT NULL,
  `is_active` int(3) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products_categories`
--

INSERT INTO `products_categories` (`id`, `order_company_id`, `title`, `title_en`, `content`, `content_en`, `photo`, `is_active`, `is_deleted`, `date_add`) VALUES
(1, 1, 'برجر', NULL, 'برجر', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/15772836555800.jpg', 1, 0, '2020-09-08 08:55:18'),
(2, 1, 'المقاهى', NULL, 'المقاهى', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/hqdefault.jpg', 1, 0, '2020-09-08 08:57:07'),
(3, 1, 'مشويات', NULL, 'مشويات', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/720181413512197617633.jpg', 1, 0, '2020-09-08 08:58:43'),
(4, 2, 'محلات الأيس كريم', NULL, 'محلات الأيس كريم', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/Picture1.jpg', 1, 0, '2021-02-05 09:12:44'),
(5, 1, 'حلويات', NULL, 'حلويات', NULL, '', 1, 0, '2021-02-28 18:12:29'),
(6, 3, 'المطابخ وتجهيز الحفلات', NULL, 'المطابخ وتجهيز الحفلات', NULL, '', 1, 0, '2021-04-05 18:49:45');

-- --------------------------------------------------------

--
-- Table structure for table `product_options`
--

CREATE TABLE `product_options` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_company_id` int(11) NOT NULL,
  `option_categ_id` int(11) DEFAULT NULL,
  `option_categ_title` varchar(250) DEFAULT NULL,
  `option_subcatg_id` int(11) DEFAULT NULL,
  `option_subcatg_title` varchar(250) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `created_date` text NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_options`
--

INSERT INTO `product_options` (`id`, `product_id`, `order_company_id`, `option_categ_id`, `option_categ_title`, `option_subcatg_id`, `option_subcatg_title`, `price`, `created_date`, `is_deleted`, `is_active`) VALUES
(1, 1, 1, 1, 'الحجم', 1, 'صغير', 100, '', 0, 1),
(2, 1, 1, 1, 'الحجم', 2, 'وسط', 200, '', 0, 1),
(3, 1, 1, 1, 'الحجم', 3, 'كبير', 300, '', 0, 1),
(4, 1, 1, 3, 'المشروب', 6, 'بيبسي', 20, '', 0, 1),
(5, 1, 1, 3, 'المشروب', 7, 'كوكا', 40, '', 0, 1),
(6, 4, 1, 2, 'الإضافات', 4, 'جبنة', 20, '', 0, 1),
(7, 1, 1, 2, 'الإضافات', 5, 'كاتشاب', 40, '', 0, 1),
(8, 1, 1, 2, 'الإضافات', 4, 'جبنة', 20, '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `prod_opti_categ`
--

CREATE TABLE `prod_opti_categ` (
  `id` int(11) NOT NULL,
  `order_company_id` int(11) NOT NULL,
  `optional` int(11) NOT NULL DEFAULT '0',
  `title` text NOT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `content` text,
  `content_en` varchar(250) DEFAULT NULL,
  `photo` text NOT NULL,
  `is_active` int(3) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prod_opti_categ`
--

INSERT INTO `prod_opti_categ` (`id`, `order_company_id`, `optional`, `title`, `title_en`, `content`, `content_en`, `photo`, `is_active`, `is_deleted`, `date_add`) VALUES
(1, 1, 0, 'الحجم', NULL, 'الحجم', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/15772836555800.jpg', 1, 0, '2020-09-08 08:55:18'),
(2, 1, 1, 'الإضافات', NULL, 'الإضافات', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/hqdefault.jpg', 1, 0, '2020-09-08 08:57:07'),
(3, 1, 1, 'المشروبات', NULL, 'المشروبات', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/720181413512197617633.jpg', 1, 0, '2020-09-08 08:58:43');

-- --------------------------------------------------------

--
-- Table structure for table `prod_opti_subcat`
--

CREATE TABLE `prod_opti_subcat` (
  `id` int(11) NOT NULL,
  `order_company_id` int(11) NOT NULL,
  `opti_categ_id` int(11) NOT NULL,
  `opti_categ_title` varchar(250) NOT NULL,
  `title` text NOT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `photo` text NOT NULL,
  `is_active` int(3) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prod_opti_subcat`
--

INSERT INTO `prod_opti_subcat` (`id`, `order_company_id`, `opti_categ_id`, `opti_categ_title`, `title`, `title_en`, `photo`, `is_active`, `is_deleted`, `date_add`) VALUES
(1, 1, 1, 'الحجم', 'صغير', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/15772836555800.jpg', 1, 0, '2020-09-08 08:55:18'),
(2, 1, 1, 'الحجم', 'كبير', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/hqdefault.jpg', 1, 0, '2020-09-08 08:57:07'),
(3, 1, 1, 'الحجم', 'وسط', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/720181413512197617633.jpg', 1, 0, '2020-09-08 08:58:43'),
(4, 1, 2, 'الإضافات', 'جبنة', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/15772836555800.jpg', 1, 0, '2020-09-08 08:55:18'),
(5, 1, 2, 'الإضافات', 'كاتشاب', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/hqdefault.jpg', 1, 0, '2020-09-08 08:57:07'),
(6, 1, 3, 'المشروبات', 'بيبسي', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/15772836555800.jpg', 1, 0, '2020-09-08 08:55:18'),
(7, 1, 2, 'المشروبات', 'كوكا', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/hqdefault.jpg', 1, 0, '2020-09-08 08:57:07');

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `rate` float NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `comment` text,
  `date_now` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`id`, `company_id`, `rate`, `client_id`, `comment`, `date_now`) VALUES
(1, 1, 5, 99, 'السلام عليكم ', '2021-03-21 15:37:23'),
(2, 2, 3.5, 6, 'Test', '2021-04-06 15:22:28'),
(3, 3, 4, 6, 'T', '2021-04-06 15:23:32'),
(4, 1, 3, 3, 'T', '2021-04-06 15:28:11'),
(6, 7, 6, 2, 'ffgghh', '2021-08-06 13:45:33'),
(7, 7, 6, 2, 'ffgghh', '2021-08-18 16:15:55'),
(8, 7, 6, 2, 'ffgghh', '2021-08-25 18:47:01'),
(9, 7, 6, 2, 'ffgghh', '2021-08-25 18:47:03'),
(10, 7, 6, 2, 'ffgghh', '2021-08-25 18:47:14');

-- --------------------------------------------------------

--
-- Table structure for table `receipt`
--

CREATE TABLE `receipt` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `tax` float DEFAULT NULL,
  `sum_productsprice` float DEFAULT NULL,
  `receipt_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `receipt_date_back` datetime DEFAULT NULL,
  `client_address_id` int(11) DEFAULT NULL,
  `payment_method` varchar(250) DEFAULT NULL,
  `payment_method_cardnumber` varchar(250) DEFAULT NULL,
  `expected_time_torecive` time DEFAULT NULL,
  `receiving_method` varchar(250) DEFAULT NULL,
  `shipping_cost` float NOT NULL DEFAULT '0',
  `car_type` varchar(250) DEFAULT NULL,
  `car_color` varchar(250) DEFAULT NULL,
  `car_cpanel_number` varchar(250) DEFAULT NULL,
  `car_comment` varchar(250) DEFAULT NULL,
  `cupon_title` varchar(250) DEFAULT NULL,
  `cupon_id` int(11) DEFAULT NULL,
  `cupon_num` float DEFAULT NULL,
  `total_before_cupon` float DEFAULT NULL,
  `total_after_cupon` float DEFAULT NULL,
  `final_receipt_value` float DEFAULT NULL,
  `captain_id` int(11) DEFAULT NULL,
  `state` varchar(150) DEFAULT '1',
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `receipt`
--

INSERT INTO `receipt` (`id`, `client_id`, `company_id`, `tax`, `sum_productsprice`, `receipt_date`, `receipt_date_back`, `client_address_id`, `payment_method`, `payment_method_cardnumber`, `expected_time_torecive`, `receiving_method`, `shipping_cost`, `car_type`, `car_color`, `car_cpanel_number`, `car_comment`, `cupon_title`, `cupon_id`, `cupon_num`, `total_before_cupon`, `total_after_cupon`, `final_receipt_value`, `captain_id`, `state`, `comment`) VALUES
(106, 1, 1, 0, 2730, '2021-08-13 18:20:21', NULL, 10, '01234567', NULL, NULL, 'توصيل', 215.598, '01234567', '01234567', '01234567', '01234567', 'rrtret', 5, 0.5, 2945.6, 1472.8, 1472.8, NULL, '1', '01234567'),
(107, 1, 1, 0, 2730, '2021-08-13 18:21:11', NULL, 10, '01234567', NULL, NULL, 'توصيل', 215.598, '01234567', '01234567', '01234567', '01234567', 'rrtret', 5, 0.5, 2945.6, 1472.8, 1472.8, NULL, '7', '01234567'),
(108, 1, 1, 0, 2730, '2021-08-13 18:21:36', NULL, 10, '01234567', NULL, NULL, 'توصيل', 215.598, '01234567', '01234567', '01234567', '01234567', 'rrtret', 5, 0.5, 2945.6, 1472.8, 1472.8, 2, '1', '01234567'),
(109, 1, 1, 0, 3080, '2021-08-18 15:07:49', NULL, 10, '01234567', NULL, NULL, 'توصيل', 215.598, '01234567', '01234567', '01234567', '01234567', 'rrtret', 5, 0.5, 3295.6, 1647.8, 1647.8, 2, '1', '01234567'),
(110, 2, 1, 0, 2000, '2021-08-18 15:10:11', NULL, 10, '01234567', NULL, NULL, 'توصيل', 215.598, '01234567', '01234567', '01234567', '01234567', 'rrtret', 5, 0.5, 2215.6, 1107.8, 1107.8, NULL, '1', '01234567'),
(111, 2, 1, 0, 2000, '2021-08-18 15:14:52', NULL, 10, '01234567', NULL, NULL, 'توصيل', 215.598, '01234567', '01234567', '01234567', '01234567', 'rrtret', 5, 0.5, 2215.6, 1107.8, 1107.8, NULL, '1', '01234567'),
(112, 1, 1, 0, 1950, '2021-09-02 14:40:43', NULL, 10, '01234567', NULL, NULL, 'توصيل', 215.598, '01234567', '01234567', '01234567', '01234567', 'rrtret', 5, 0.5, 2165.6, 1082.8, 1082.8, NULL, '1', '01234567'),
(113, 11, 1, 0, 650, '2021-09-03 08:48:39', NULL, 16, '', NULL, NULL, 'استلام من الفرع', 0, '', '', '', '', '', 0, 0, 650, 650, 650, NULL, '1', '');

-- --------------------------------------------------------

--
-- Table structure for table `receipt_details`
--

CREATE TABLE `receipt_details` (
  `id` int(11) NOT NULL,
  `receipt_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `final_item_price` float NOT NULL DEFAULT '0',
  `product_options` text,
  `sub_product_options` text,
  `offer_id` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT '1',
  `comment` text,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `receipt_details`
--

INSERT INTO `receipt_details` (`id`, `receipt_id`, `client_id`, `product_id`, `company_id`, `amount`, `final_item_price`, `product_options`, `sub_product_options`, `offer_id`, `state`, `comment`, `created_date`) VALUES
(21, 106, 1, 1, 1, 6, 210, 'tt', 'ww', NULL, 1, 'nn', '2021-08-13 15:04:48'),
(22, 106, 1, 6, 1, 4, 20, NULL, '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(23, 106, 1, 3, 1, 5, 1500, NULL, '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(24, 106, 1, 2, 1, 2, 500, '1,5,7', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(25, 106, 1, 4, 1, 1, 500, '2,4,7', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(26, 107, 1, 1, 1, 6, 210, 'tt', 'ww', NULL, 1, 'nn', '2021-08-13 15:04:48'),
(27, 107, 1, 6, 1, 4, 20, NULL, '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(28, 107, 1, 3, 1, 5, 1500, NULL, '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(29, 107, 1, 2, 1, 2, 500, '1,5,7', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(30, 107, 1, 4, 1, 1, 500, '2,4,7', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(31, 108, 1, 1, 1, 6, 210, 'tt', 'ww', NULL, 1, 'nn', '2021-08-13 15:04:48'),
(32, 108, 1, 6, 1, 4, 20, NULL, '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(33, 108, 1, 3, 1, 5, 1500, NULL, '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(34, 108, 1, 2, 1, 2, 500, '1,5,7', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(35, 108, 1, 4, 1, 1, 500, '2,4,7', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(36, 109, 1, 1, 1, 6, 210, '', 'ww', NULL, 1, 'nn', '2021-08-13 15:04:48'),
(37, 109, 1, 2, 1, 2, 20, '', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(38, 109, 1, 3, 1, 5, 1500, '', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(39, 109, 1, 4, 1, 1, 500, '', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(40, 109, 1, 1, 1, 6, 850, '1,5,8', 'ffgghh', NULL, 1, 'ffgghh', '2021-08-13 21:59:50'),
(41, 110, 2, 3, 1, 5, 1500, '', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(42, 110, 2, 4, 1, 1, 500, '', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(43, 111, 2, 3, 1, 5, 1500, '', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(44, 111, 2, 4, 1, 1, 500, '', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(45, 112, 1, 1, 1, 6, 1260, '1,5,8', 'ffgghh', NULL, 1, 'ffgghh', '2021-08-13 21:59:50'),
(46, 113, 11, 1, 1, 1, 650, '1, 2, 3', '1, 5, 6', NULL, 1, '', '2021-09-02 07:16:30');

-- --------------------------------------------------------

--
-- Table structure for table `receipt_state`
--

CREATE TABLE `receipt_state` (
  `id` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `receipt_state`
--

INSERT INTO `receipt_state` (`id`, `title`) VALUES
(1, 'قيد الإنتظار'),
(2, 'تم استلام الطلب'),
(3, 'طلبك قيد التحضير'),
(4, 'الطلب جاهز'),
(5, 'طلبك فى الطريق'),
(6, 'الكابتن وصل'),
(7, 'الغاء الطلب'),
(8, 'الغاء الطلب من الكابتن'),
(9, 'تم');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `title_ar` varchar(350) NOT NULL,
  `title_en` varchar(350) NOT NULL,
  `content_ar` text NOT NULL,
  `content_en` text NOT NULL,
  `img` text NOT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `state` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL,
  `dep_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `title_ar`, `title_en`, `content_ar`, `content_en`, `img`, `icon`, `state`, `date`, `dep_id`) VALUES
(1, 'بنزين 91', 'Petrol 91', 'بنزين 91', 'Petrol 91', '', NULL, '1', '', 1),
(2, 'بنزين 95', 'Petrol 95', 'بنزين 95', 'Petrol 95', '', NULL, '1', '', 1),
(3, 'ديزل', 'Diesel', 'ديزل', 'Diesel', '', NULL, '1', '', 1),
(4, 'غسيل سيارة', 'Car Clean', 'غسيل سيارة', 'Car Clean', '', NULL, '1', '', 2),
(5, 'تغيير زيت', 'Change Oil', 'تغيير زيت', 'Change Oil', '', NULL, '1', '', 3),
(41, 'توصيل وقود', 'delivery oil', 'توصيل وقود', 'delivery oil', '', NULL, '1', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `services_deps`
--

CREATE TABLE `services_deps` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `photo` text,
  `content` text,
  `conte_en` text,
  `post_icon` varchar(250) DEFAULT NULL,
  `date_add` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services_deps`
--

INSERT INTO `services_deps` (`id`, `title`, `title_en`, `photo`, `content`, `conte_en`, `post_icon`, `date_add`) VALUES
(1, 'وقود', NULL, 'https://demo.f4h.com.sa/fullpay/images/home/services_deps/fuel.png', ' وقود وقود وقود وقود وقود وقود وقود وقود  وقود وقود وقود وقود وقود وقود وقود وقود وقود وقود وقود', NULL, 'fa fa-comments-o', '2020-09-08 08:55:18'),
(2, 'غسيل سيارات', NULL, 'https://demo.f4h.com.sa/fullpay/images/home/services_deps/car_wash.png', 'غسيل سيارات غسيل سيارات غسيل سيارات غسيل سيارات غسيل سيارات غسيل سيارات ', NULL, 'fa fa-leaf', '2020-09-08 08:57:07'),
(3, 'صيانة', NULL, 'https://demo.f4h.com.sa/fullpay/images/home/services_deps/maintenance.png', 'صيانة صيانة صيانة صيانة صيانة صيانة صيانة صيانة صيانة صيانة صيانة صيانة صيانة ', NULL, 'fa fa-support', '2020-09-08 08:58:43');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(120) COLLATE utf8_bin DEFAULT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('53aa10c8d5885a9c0fadc0ba9867138e', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0', 1630700220, ''),
('64b70e2fb556d2f1a1c9aa616c255dba', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0', 1630704885, 'a:2:{s:9:"user_data";s:0:"";s:3:"lan";s:2:"ar";}'),
('6e89fc5c05a745f11002e57658b3756e', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0', 1630700604, ''),
('c411c1aed3f8b8e9617d8a79f25be016', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0', 1630705954, '');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `site_title` text NOT NULL,
  `application_fees` float DEFAULT NULL,
  `shipping_kilo_cost` float NOT NULL DEFAULT '0',
  `site_desc` text NOT NULL,
  `site_key` text NOT NULL,
  `site_logo` text NOT NULL,
  `site_icon` text NOT NULL,
  `site_lang` varchar(10) NOT NULL,
  `site_phone` text NOT NULL,
  `site_fax` varchar(100) DEFAULT NULL,
  `site_email` text NOT NULL,
  `site_dutytime` text,
  `site_title_en` varchar(100) DEFAULT NULL,
  `site_dutytime_en` text,
  `site_facebok` text NOT NULL,
  `site_twitter` text NOT NULL,
  `site_youtube` text NOT NULL,
  `site_linkedin` text NOT NULL,
  `site_instagram` text NOT NULL,
  `css` text NOT NULL,
  `js` text NOT NULL,
  `copy_right` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `site_title`, `application_fees`, `shipping_kilo_cost`, `site_desc`, `site_key`, `site_logo`, `site_icon`, `site_lang`, `site_phone`, `site_fax`, `site_email`, `site_dutytime`, `site_title_en`, `site_dutytime_en`, `site_facebok`, `site_twitter`, `site_youtube`, `site_linkedin`, `site_instagram`, `css`, `js`, `copy_right`) VALUES
(1, 'Order', 5, 5, 'Order', 'Order', 'deb555bce78f27154799f5c61c68909d.png', 'bac0a0f7251a26fcae41446a4c4baad9.png', 'ar', '0123456789', '01234567891', 'info@FullPay.com', 'الإثنين - الخميس: 08:00 - 18:00                                 \r\n<br>الجمعة - السبت : 08:00 - 12:30                                 \r\n<br>الأحد - مغلق', 'Order', 'Monday- Thursday: 08:00 - 18:00                                 \r\n<br> Saturday: 08:00 - 12:30                                 \r\n<br>Friday- Closed', 'https://www.facebook.com/Order/', 'https://www.twitter.com/Order/', 'https://www.youtube.com/Order/', 'https://www.linkedin.com/Order/', 'https://www.instgram.com/Order/', '', '', 'جميع الحقوق محفوظه Order');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(11) NOT NULL,
  `slide_title_ar` varchar(100) NOT NULL,
  `slide_title_en` varchar(100) NOT NULL,
  `slide_subtitle_ar` text NOT NULL,
  `slide_subtitle_en` text NOT NULL,
  `img` varchar(100) NOT NULL,
  `state` varchar(20) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `slide_title_ar`, `slide_title_en`, `slide_subtitle_ar`, `slide_subtitle_en`, `img`, `state`, `is_active`, `is_deleted`) VALUES
(1, 'شريحة 1', 'slide 1', 'نص مبدئي 1', 'Sample Text 1', 'https://demo.f4h.com.sa/fullpay/images/home/slides/3.jpg', '1', 1, 0),
(2, 'شريحة 2', 'slide 2', 'نص مبدئي 2', 'Sample Text  2', 'https://demo.f4h.com.sa/fullpay/images/home/slides/1.jpg', '0', 0, 1),
(3, 'شريحة 3', 'slide 3', 'نص مبدئي  3', 'Sample Text  3', 'https://demo.f4h.com.sa/fullpay/images/home/slides/2.jpg', '0', 0, 1),
(9, '66', '', '', '', 'http://localhost/fullpay/images/home/slides/9e47ec074db8e667f6da055a314d2cf1.GIF', '0', 0, 1),
(10, 'صورة', '', '', '', 'http://demo.f4h.com.sa/fullpay/images/home/slides/ca01501594f3f56f43aea7f1b2ece720.jpg', '', 1, 0),
(11, '...', '', '', '', 'http://demo.f4h.com.sa/fullpay/images/home/slides/bc3aabe32fb1fdafa10c47eb0391487f.png', '0', 0, 1),
(12, 'صورة22', '', '', '', 'http://demo.f4h.com.sa/fullpay/images/home/slides/d76eb35ec9b1c96e2e62e86f778c6a1b.jpg', '', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `social`
--

CREATE TABLE `social` (
  `id` int(11) NOT NULL,
  `facebook` text NOT NULL,
  `twitter` text NOT NULL,
  `youtube` text NOT NULL,
  `instagram` text NOT NULL,
  `linkedin` text NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `social`
--

INSERT INTO `social` (`id`, `facebook`, `twitter`, `youtube`, `instagram`, `linkedin`, `store_id`) VALUES
(1, 'Facebook', 'Twitter', 'Youtube', 'Instagram', 'Linkedin', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tax`
--

CREATE TABLE `tax` (
  `id` int(11) NOT NULL,
  `name` text,
  `value` float DEFAULT NULL,
  `comment` text,
  `is_active` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax`
--

INSERT INTO `tax` (`id`, `name`, `value`, `comment`, `is_active`) VALUES
(1, 'قيمة مضافة', 0.5, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `time_open_close`
--

CREATE TABLE `time_open_close` (
  `id` int(11) NOT NULL,
  `order_company_id` int(11) DEFAULT NULL,
  `saturday_open` time DEFAULT NULL,
  `sunday_open` time DEFAULT NULL,
  `monday_open` time DEFAULT NULL,
  `tuesday_open` time DEFAULT NULL,
  `wednesday_open` time DEFAULT NULL,
  `thursday_open` time DEFAULT NULL,
  `friday_open` time DEFAULT NULL,
  `saturday_close` time DEFAULT NULL,
  `sunday_close` time DEFAULT NULL,
  `monday_close` time DEFAULT NULL,
  `tuesday_close` time DEFAULT NULL,
  `wednesday_close` time DEFAULT NULL,
  `thursday_close` time DEFAULT NULL,
  `friday_close` time DEFAULT NULL,
  `is_active` int(3) NOT NULL DEFAULT '1',
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `time_open_close`
--

INSERT INTO `time_open_close` (`id`, `order_company_id`, `saturday_open`, `sunday_open`, `monday_open`, `tuesday_open`, `wednesday_open`, `thursday_open`, `friday_open`, `saturday_close`, `sunday_close`, `monday_close`, `tuesday_close`, `wednesday_close`, `thursday_close`, `friday_close`, `is_active`, `date_add`) VALUES
(1, 1, '00:00:00', NULL, '00:00:00', NULL, '00:00:00', '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-09-08 08:55:18'),
(2, 2, '00:00:00', NULL, '00:00:00', NULL, '00:00:00', '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-09-08 08:57:07'),
(3, 1, '00:00:00', NULL, '00:00:00', NULL, '00:00:00', '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-09-08 08:58:43'),
(4, 2, '00:00:00', NULL, '00:00:00', NULL, '00:00:00', '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-02-05 09:12:44'),
(5, 1, '00:00:00', NULL, '00:00:00', NULL, '00:00:00', '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-02-28 18:12:29'),
(6, 3, '00:00:00', NULL, '00:00:00', NULL, '00:00:00', '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-04-05 18:49:45');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(350) NOT NULL,
  `phone` varchar(125) NOT NULL,
  `email` varchar(255) NOT NULL,
  `user_hash` text NOT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `user_group` int(11) NOT NULL,
  `forgot_password` int(11) NOT NULL,
  `created_date` text NOT NULL,
  `is_active` varchar(3) NOT NULL COMMENT 'yes or no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `phone`, `email`, `user_hash`, `branch_id`, `user_group`, `forgot_password`, `created_date`, `is_active`) VALUES
(9, 'فانتاستك', '01001647527', 'f4h@f4h.com', '8cb2237d0679ca88db6464eac60da96345513964', 3, 3, 129972, '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `id` int(11) NOT NULL,
  `title` varchar(350) NOT NULL,
  `type` int(11) NOT NULL,
  `is_user` int(11) NOT NULL,
  `is_user_view` int(11) NOT NULL,
  `is_user_add` int(11) NOT NULL,
  `is_user_edit` int(11) NOT NULL,
  `is_user_delete` int(11) NOT NULL,
  `is_client` int(11) NOT NULL,
  `is_client_view` int(11) NOT NULL,
  `is_client_add` int(11) NOT NULL,
  `is_client_edit` int(11) NOT NULL,
  `is_client_delete` int(11) NOT NULL,
  `is_page` int(11) NOT NULL,
  `is_page_view` int(11) NOT NULL,
  `is_page_add` int(11) NOT NULL,
  `is_page_edit` int(11) NOT NULL,
  `is_page_delete` int(11) NOT NULL,
  `is_category` int(11) NOT NULL,
  `is_category_view` int(11) NOT NULL,
  `is_category_add` int(11) NOT NULL,
  `is_category_edit` int(11) NOT NULL,
  `is_category_delete` int(11) NOT NULL,
  `is_shop` int(11) NOT NULL,
  `is_shop_view` int(11) NOT NULL,
  `is_shop_add` int(11) NOT NULL,
  `is_shop_edit` int(11) NOT NULL,
  `is_shop_delete` int(11) NOT NULL,
  `is_product` int(11) NOT NULL,
  `is_product_view` int(11) NOT NULL,
  `is_product_add` int(11) NOT NULL,
  `is_product_edit` int(11) NOT NULL,
  `is_product_delete` int(11) NOT NULL,
  `is_order` int(11) NOT NULL,
  `is_order_view` int(11) NOT NULL,
  `is_order_edit` int(11) NOT NULL,
  `is_order_delete` int(11) NOT NULL,
  `is_report` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`id`, `title`, `type`, `is_user`, `is_user_view`, `is_user_add`, `is_user_edit`, `is_user_delete`, `is_client`, `is_client_view`, `is_client_add`, `is_client_edit`, `is_client_delete`, `is_page`, `is_page_view`, `is_page_add`, `is_page_edit`, `is_page_delete`, `is_category`, `is_category_view`, `is_category_add`, `is_category_edit`, `is_category_delete`, `is_shop`, `is_shop_view`, `is_shop_add`, `is_shop_edit`, `is_shop_delete`, `is_product`, `is_product_view`, `is_product_add`, `is_product_edit`, `is_product_delete`, `is_order`, `is_order_view`, `is_order_edit`, `is_order_delete`, `is_report`) VALUES
(2, 'مدير فرع', 2, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(1, 'موظف مبيعات', 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(3, 'مدير رئيسي', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_categories`
--
ALTER TABLE `activity_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_subcategories`
--
ALTER TABLE `activity_subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `captains`
--
ALTER TABLE `captains`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_type`
--
ALTER TABLE `car_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_address`
--
ALTER TABLE `client_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_payment_cards`
--
ALTER TABLE `client_payment_cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer`
--
ALTER TABLE `offer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer_groups`
--
ALTER TABLE `offer_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer_types`
--
ALTER TABLE `offer_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordertime_open_close`
--
ALTER TABLE `ordertime_open_close`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_company`
--
ALTER TABLE `order_company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_categories`
--
ALTER TABLE `products_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_options`
--
ALTER TABLE `product_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prod_opti_categ`
--
ALTER TABLE `prod_opti_categ`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prod_opti_subcat`
--
ALTER TABLE `prod_opti_subcat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt`
--
ALTER TABLE `receipt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_details`
--
ALTER TABLE `receipt_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_state`
--
ALTER TABLE `receipt_state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services_deps`
--
ALTER TABLE `services_deps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social`
--
ALTER TABLE `social`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax`
--
ALTER TABLE `tax`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_open_close`
--
ALTER TABLE `time_open_close`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_categories`
--
ALTER TABLE `activity_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `activity_subcategories`
--
ALTER TABLE `activity_subcategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `captains`
--
ALTER TABLE `captains`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `car_type`
--
ALTER TABLE `car_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `client_address`
--
ALTER TABLE `client_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `client_payment_cards`
--
ALTER TABLE `client_payment_cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `offer`
--
ALTER TABLE `offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `offer_groups`
--
ALTER TABLE `offer_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `offer_types`
--
ALTER TABLE `offer_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ordertime_open_close`
--
ALTER TABLE `ordertime_open_close`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `order_company`
--
ALTER TABLE `order_company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `products_categories`
--
ALTER TABLE `products_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `product_options`
--
ALTER TABLE `product_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `prod_opti_categ`
--
ALTER TABLE `prod_opti_categ`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `prod_opti_subcat`
--
ALTER TABLE `prod_opti_subcat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `rate`
--
ALTER TABLE `rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `receipt`
--
ALTER TABLE `receipt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;
--
-- AUTO_INCREMENT for table `receipt_details`
--
ALTER TABLE `receipt_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `receipt_state`
--
ALTER TABLE `receipt_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `services_deps`
--
ALTER TABLE `services_deps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `social`
--
ALTER TABLE `social`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tax`
--
ALTER TABLE `tax`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `time_open_close`
--
ALTER TABLE `time_open_close`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
