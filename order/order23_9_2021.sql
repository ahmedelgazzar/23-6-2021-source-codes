-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 24, 2021 at 12:58 AM
-- Server version: 5.7.35-log
-- PHP Version: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demof4hc_order`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_categories`
--

CREATE TABLE `activity_categories` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `content` text,
  `content_en` varchar(250) DEFAULT NULL,
  `photo` text NOT NULL,
  `is_active` int(3) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activity_categories`
--

INSERT INTO `activity_categories` (`id`, `title`, `title_en`, `content`, `content_en`, `photo`, `is_active`, `is_deleted`, `date_add`) VALUES
(1, 'المطاعم', NULL, 'المطاعم', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/15772836555800.jpg', 1, 0, '2020-09-08 08:55:18'),
(2, 'المقاهى', NULL, 'المقاهى', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/hqdefault.jpg', 1, 0, '2020-09-08 08:57:07'),
(3, 'محلات الحلويات', NULL, 'محلات الحلويات', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/720181413512197617633.jpg', 1, 0, '2020-09-08 08:58:43'),
(4, 'محلات الأيس كريم', NULL, 'محلات الأيس كريم', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/Picture1.jpg', 1, 0, '2021-02-05 09:12:44'),
(5, 'المخابز', NULL, 'المخابز', NULL, '', 1, 0, '2021-02-28 18:12:29'),
(6, 'المطابخ وتجهيز الحفلات', NULL, 'المطابخ وتجهيز الحفلات', NULL, '', 1, 0, '2021-04-05 18:49:45');

-- --------------------------------------------------------

--
-- Table structure for table `activity_subcategories`
--

CREATE TABLE `activity_subcategories` (
  `id` int(11) NOT NULL,
  `activity_category_id` int(11) DEFAULT '1',
  `title` text NOT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `content` text,
  `content_en` varchar(250) DEFAULT NULL,
  `photo` text NOT NULL,
  `is_active` int(3) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activity_subcategories`
--

INSERT INTO `activity_subcategories` (`id`, `activity_category_id`, `title`, `title_en`, `content`, `content_en`, `photo`, `is_active`, `is_deleted`, `date_add`) VALUES
(1, 1, 'المطاعم', NULL, 'المطاعم', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/15772836555800.jpg', 1, 0, '2020-09-08 08:55:18'),
(2, 1, 'المقاهى', NULL, 'المقاهى', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/hqdefault.jpg', 1, 0, '2020-09-08 08:57:07'),
(3, 1, 'محلات الحلويات', NULL, 'محلات الحلويات', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/720181413512197617633.jpg', 1, 0, '2020-09-08 08:58:43'),
(4, 1, 'محلات الأيس كريم', NULL, 'محلات الأيس كريم', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/Picture1.jpg', 1, 0, '2021-02-05 09:12:44'),
(5, 1, 'المخابز', NULL, 'المخابز', NULL, '', 1, 0, '2021-02-28 18:12:29'),
(6, 1, 'المطابخ وتجهيز الحفلات', NULL, 'المطابخ وتجهيز الحفلات', NULL, '', 1, 0, '2021-04-05 18:49:45');

-- --------------------------------------------------------

--
-- Table structure for table `captains`
--

CREATE TABLE `captains` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `iban` text,
  `nationality` text,
  `national_id` varchar(250) NOT NULL DEFAULT '0',
  `phone` text NOT NULL,
  `password` varchar(50) NOT NULL,
  `birth_date` text,
  `car_panel_number` varchar(250) DEFAULT NULL,
  `car_type` varchar(250) DEFAULT NULL,
  `bank_name` varchar(250) DEFAULT NULL,
  `city` text NOT NULL,
  `invitation_code` text,
  `province` varchar(250) DEFAULT NULL,
  `personal_photo` varchar(250) DEFAULT NULL,
  `national_id_photo` varchar(250) DEFAULT NULL,
  `driver_card_id` varchar(250) DEFAULT NULL,
  `car_card_id` varchar(250) DEFAULT NULL,
  `insurance_card` varchar(250) DEFAULT NULL,
  `car_front_photo` varchar(250) DEFAULT NULL,
  `car_back_photo` varchar(250) DEFAULT NULL,
  `ref_code` int(11) NOT NULL,
  `remember_token` text,
  `type` int(11) NOT NULL DEFAULT '2',
  `user_group` int(11) NOT NULL,
  `created_date` text NOT NULL,
  `can_accept_request` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `captains`
--

INSERT INTO `captains` (`id`, `name`, `last_name`, `email`, `iban`, `nationality`, `national_id`, `phone`, `password`, `birth_date`, `car_panel_number`, `car_type`, `bank_name`, `city`, `invitation_code`, `province`, `personal_photo`, `national_id_photo`, `driver_card_id`, `car_card_id`, `insurance_card`, `car_front_photo`, `car_back_photo`, `ref_code`, `remember_token`, `type`, `user_group`, `created_date`, `can_accept_request`, `is_active`, `is_deleted`) VALUES
(1, 'سائق 1', '9876', 'fcv@hsfg.gh', NULL, NULL, '966555555555', '966555555555', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, 'd8zNvO5eS8S6lANUging23:APA91bFiYeH6zTud5XiYLDCJ93Xgs32vPZErZFjJyerAQdcOIisIeXrR6La7ek7leOpITfPKmfBWt75IyUpKMTCb33lDf2vyqBbRfp6oO_gJltZugnCL73gFokFmwmGwhGV3m_hFmf3i', 2, 0, '2021-03-22 20:33:35', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `receipt_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `final_item_price` float NOT NULL DEFAULT '0',
  `product_options` text,
  `sub_product_options` text,
  `offer_id` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT '1',
  `comment` text,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `receipt_id`, `client_id`, `company_id`, `product_id`, `amount`, `final_item_price`, `product_options`, `sub_product_options`, `offer_id`, `state`, `comment`, `created_date`) VALUES
(64, NULL, 3, 1, 1, 2, 95, '1,5', '', NULL, 1, 'ffgghh', '2021-09-18 16:36:23'),
(65, NULL, 3, 1, 3, 2, 880, '1,5', '', NULL, 1, 'ffgghh', '2021-09-18 16:36:23'),
(66, NULL, 3, 1, 4, 2, 1280, '1,5', '', NULL, 1, 'ffgghh', '2021-09-18 16:36:23'),
(69, NULL, 1, 0, 15, 2, 1290, '1,2', '4,3', NULL, 1, 'ffgghh', '2021-09-21 07:20:19'),
(70, NULL, 1, 0, 15, 4, 1700, '5,7', '4,3', NULL, 1, 'ffgghh', '2021-09-21 07:21:39'),
(71, NULL, 1, 0, 15, 1, 345, '', '', NULL, 1, 'ffgghh', '2021-09-21 15:22:35'),
(73, NULL, 1, 1, 4, 2, 1040, '6', '1,7', NULL, 1, 'ffgghh', '2021-09-21 16:58:23'),
(74, NULL, 8, 1, 1, 1, 47.5, '1,5', '', NULL, 1, 'ffgghh', '2021-09-21 16:58:23'),
(75, NULL, 8, 1, 2, 3, 410, '1,5', '', NULL, 1, 'ffgghh', '2021-09-21 16:58:23'),
(76, NULL, 8, 1, 3, 4, 1760, '1,5', '2,3', NULL, 1, 'ffgghh', '2021-09-21 16:58:23'),
(77, NULL, 8, 1, 4, 4, 2560, '1,5', '2,3', NULL, 1, 'ffgghh', '2021-09-21 16:58:23'),
(78, NULL, 8, 1, 23, 4, 1099.12, '1,5', '2,3', NULL, 1, 'ffgghh', '2021-09-21 16:58:23'),
(98, NULL, 12, 1, 2, 1, 40, '', '', NULL, 1, '', '2021-09-23 15:13:32'),
(108, NULL, 7, 1, 3, 2, 600, '', '', NULL, 1, '', '2021-09-23 21:45:43'),
(109, NULL, 7, 1, 6, 1, 10, '', '', NULL, 1, '', '2021-09-23 21:46:20'),
(110, NULL, 7, 1, 2, 1, 40, '', '', NULL, 1, '', '2021-09-23 21:47:07'),
(111, NULL, 7, 1, 1, 1, 37.5, '1', '', NULL, 1, '', '2021-09-23 21:51:34');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `photo` varchar(250) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `title`, `photo`, `is_active`) VALUES
(19, 'الأحساء', NULL, 0),
(20, 'رفحاء', NULL, 0),
(21, 'حفر الباطن', NULL, 0),
(22, 'حائل', NULL, 0),
(23, 'القريات', NULL, 0),
(24, 'عرعر', NULL, 0),
(25, 'الجوف (سكاكا)', NULL, 0),
(26, 'تبوك', 'tabuk.png', 1),
(27, 'شرق الرياض', NULL, 0),
(28, 'غرب الرياض', NULL, 0),
(29, 'وسط الرياض', NULL, 0),
(30, 'شمال الرياض', NULL, 0),
(31, 'جنوب الرياض', NULL, 0),
(32, 'محافظة الخرج والدلم', NULL, 0),
(33, 'محافظات منطقة القصيم', NULL, 0),
(34, 'محافظات منطقة الرياض', NULL, 0),
(35, 'رابغ', NULL, 0),
(36, 'ينبع', NULL, 0),
(37, 'مكة المكرمة', NULL, 0),
(38, 'جدة', NULL, 0),
(39, 'القنفدة', NULL, 0),
(40, 'الطائف', NULL, 0),
(41, 'المدينة المنورة', NULL, 0),
(42, 'بقيق', NULL, 0),
(43, 'النعيرية', NULL, 0),
(44, 'الخفجي', NULL, 0),
(45, 'الجبيل', NULL, 0),
(46, 'رأس تنورة', NULL, 0),
(47, 'الخبر', NULL, 0),
(48, 'القطيف', NULL, 0),
(49, 'حفر الباطن', NULL, 0),
(50, 'سيهات', NULL, 0),
(51, 'الدمام', NULL, 0),
(52, 'الظهران', NULL, 0),
(53, 'طريف', NULL, 0),
(54, 'صفوى', NULL, 0),
(55, 'محايل عسير', NULL, 0),
(56, 'خميس مشيط', NULL, 0),
(57, 'الباحة', NULL, 0),
(58, 'شرورة', NULL, 0),
(59, 'جازان', NULL, 0),
(60, 'أبها', NULL, 0),
(61, 'نجران', NULL, 0),
(62, 'بيشة', NULL, 0),
(65, 'املج', 'amlag.png', 1),
(66, 'ضبا', 'daba.png', 1),
(67, 'الوجه', 'elogah.png', 1),
(68, 'حقل', 'haqle.png', 1),
(71, 'بئر ما', 'sharma.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `name` text,
  `name_en` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `phone` text NOT NULL,
  `address` text NOT NULL,
  `email` varchar(350) NOT NULL,
  `password` varchar(50) NOT NULL,
  `remember_token` text,
  `ref_code` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `created_date` text NOT NULL,
  `is_active` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `name`, `name_en`, `last_name`, `phone`, `address`, `email`, `password`, `remember_token`, `ref_code`, `type`, `created_date`, `is_active`, `is_deleted`) VALUES
(2, 'aa', 'Ahmed Elgazzar', 'k', '966548270331', '', 'cc', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ttuu', 3373, 1, '2021-07-08 23:39:11', 1, 0),
(3, 'Sajed', 'Elgazzar', NULL, '0123456789v', '', '', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 4622, 1, '2021-07-08 23:41:08', 1, 0),
(5, 'Sajed', NULL, 'Elgazzar', '44', '', 'ccc', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 5279, 1, '2021-08-14 22:13:46', 1, 0),
(6, 'Sajed', NULL, 'Elgazzar', '4455', '', 'cccc', '8cb2237d0679ca88db6464eac60da96345513964', NULL, 7875, 1, '2021-08-16 12:53:39', 1, 0),
(7, 'iOS', NULL, 'App', '123', '', 'test@test.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'csK9oGU3QcO6a3BxLNef8v:APA91bF8qp_sNW77AThXR5I8v8wuvIpmSWyErLUQkhVXRvSZ-uMbDSYwb4Aogx8YKjanD0QsUO3gY34Vd5g_SufeMUmjz0c2VxWBkU_YvQjXBjzuvogRCfTX5FCh7aNTiXD4pVfiKcNi', 9464, 1, '2021-08-17 15:41:18', 1, 0),
(8, 'Abdallah', NULL, 'badawy ', '966562021500', '', 'tset@test.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 8510, 1, '2021-08-17 16:11:18', 1, 0),
(9, 'Sajed', NULL, 'Elgazzar', '445566', '', 'cccjgkyc', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 5066, 1, '2021-08-19 08:55:22', 1, 0),
(10, 'Sajed', NULL, '', '44566566', '', 'cccj66gkyc', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 4647, 1, '2021-08-19 08:55:51', 1, 0),
(11, 'Mohamad Jaad ', NULL, ' ', '966555555556', '', 'jaadmohamad@yahoo.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'd8zNvO5eS8S6lANUging23:APA91bFiYeH6zTud5XiYLDCJ93Xgs32vPZErZFjJyerAQdcOIisIeXrR6La7ek7leOpITfPKmfBWt75IyUpKMTCb33lDf2vyqBbRfp6oO_gJltZugnCL73gFokFmwmGwhGV3m_hFmf3i', 1473, 1, '2021-08-19 08:58:20', 1, 0),
(12, 'Mohamad Jaad ', NULL, ' ', '966555555558', '', 'eng.mohamadjaad1@gmail.com', '3d4f2bf07dc1be38b20cd6e46949a1071f9d0e3d', 'd8zNvO5eS8S6lANUging23:APA91bFiYeH6zTud5XiYLDCJ93Xgs32vPZErZFjJyerAQdcOIisIeXrR6La7ek7leOpITfPKmfBWt75IyUpKMTCb33lDf2vyqBbRfp6oO_gJltZugnCL73gFokFmwmGwhGV3m_hFmf3i', 2687, 1, '2021-09-23 19:12:49', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `client_address`
--

CREATE TABLE `client_address` (
  `id` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `descrption` text,
  `client_id` int(11) DEFAULT NULL,
  `latitude` text,
  `longitude` text,
  `is_default` int(11) DEFAULT '0',
  `is_deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_address`
--

INSERT INTO `client_address` (`id`, `title`, `descrption`, `client_id`, `latitude`, `longitude`, `is_default`, `is_deleted`) VALUES
(10, '7t', '6d', 2, '31.23527980044717', '31.23527980044717', 0, 1),
(8, '7t', '6d', 6, 'la', 'lo', 66, 1),
(7, '7', '6', 2, 'ffgghh', 'ffgghh', 0, 0),
(11, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(12, '7', '6', 2, 'ffgghh', 'ffgghh', 0, 0),
(13, '7', '6', 2, 'ffgghh', 'ffgghh', 0, 1),
(14, '7', '6', 2, 'ffgghh', 'ffgghh', 0, 0),
(15, '7', '6', 2, 'ffgghh', 'ffgghh', 0, 0),
(16, 'كفر أشليم', 'G46C+MHW، كفر أشليم، مركز قويسنا، المنوفية، مصر', 11, '30.5117951', '31.1217323', 0, 1),
(17, 'قسم بنها', 'النساج، قسم بنها، بنها، القليوبية، مصر', 11, '30.46175543805246', '31.183209456503388', 0, 1),
(18, 'الرملة', 'الرملة، بنها،، الرملة، بنها، القليوبية،، الرملة، بنها، القليوبية، مصر', 11, '30.442835266111317', '31.16414397954941', 0, 1),
(19, 'كفر أشليم', 'Unnamed Road, أشليم، مركز قويسنا، المنوفية،، كفر أشليم، مركز قويسنا، المنوفية، مصر', 11, '30.511865', '31.1218081', 0, 1),
(20, 'كفر أشليم', 'Unnamed Road, أشليم، مركز قويسنا، المنوفية،، كفر أشليم، مركز قويسنا، المنوفية، مصر', 11, '30.511865', '31.1218081', 0, 1),
(21, 'ميت العطار', 'ميت العطار، بنها،، ميت العطار، بنها، القليوبية، مصر', 11, '30.440156861255755', '31.14298403263092', 0, 1),
(22, 'كفر أشليم', 'Unnamed Road, أشليم، مركز قويسنا، المنوفية،، كفر أشليم، مركز قويسنا، المنوفية، مصر', 11, '30.511865', '31.1218081', 0, 1),
(23, NULL, 'Unnamed Road, مركز قويسنا، المنوفية، مصر', 11, '30.477655055576278', '31.13649174571037', 0, 1),
(24, NULL, 'Unnamed Road, مركز قويسنا، المنوفية، مصر', 11, '30.477655055576278', '31.13649174571037', 0, 1),
(25, 'شبرا قبالة وخلوة نور الدين', 'Unnamed Road, شبرا قبالة وخلوة نور الدين، مركز قويسنا، المنوفية، مصر', 11, '30.47670296057507', '31.136584952473637', 0, 1),
(26, 'كفر أشليم', 'G46C+MHW، كفر أشليم، مركز قويسنا، المنوفية، مصر', 11, '30.511863', '31.1217961', 0, 1),
(27, 'منشأة مسجد الخضر', 'إسطنها، أسطنها، الباجور،، منشأة مسجد الخضر، الباجور، المنوفية، مصر', 11, '30.458869180184575', '31.12355142831802', 0, 1),
(28, 'كفر أشليم', 'G46C+MHW، كفر أشليم، مركز قويسنا، المنوفية، مصر', 11, '30.5118596', '31.1218137', 0, 1),
(29, 'كفر أشليم', 'G46C+MHW، كفر أشليم، مركز قويسنا، المنوفية، مصر', 11, '30.5118596', '31.1218137', 1, 0),
(30, 'بطا', 'وروره - كفر بطا، بطا، بنها، القليوبية، مصر', 0, '30.45478688816942', '31.16664815694094', 1, 0),
(31, '7', '6', 2, 'ffgghh', 'ffgghh', 0, 0),
(32, 'King Abdullah, Eastern Ring Road, Riyadh, Saudi Arabia, 12451 ', 'test', 7, '24.759846805239693', '46.73892941325903', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `client_payment_cards`
--

CREATE TABLE `client_payment_cards` (
  `id` int(11) NOT NULL,
  `card_number` varchar(250) DEFAULT NULL,
  `cvv` varchar(250) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `expire_date` varchar(250) DEFAULT NULL,
  `is_remember` int(11) DEFAULT '0',
  `is_deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_payment_cards`
--

INSERT INTO `client_payment_cards` (`id`, `card_number`, `cvv`, `client_id`, `name`, `expire_date`, `is_remember`, `is_deleted`) VALUES
(10, '7t', '6d', 2, 'la', 'lo', 1, 0),
(8, '7t', '6d', 6, 'la', 'lo', 66, 0),
(7, '7', '6', 2, 'ffgghh', 'ffgghh', 0, 1),
(12, '3453645654', '666', 88, 'شاةثي', '5-9', 1, 0),
(13, '3453645654', '666', 88, 'شاةثي', '5-9', 1, 0),
(14, '3453645654', '666', 2, 'شاةثي', '5-9', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `message` longtext NOT NULL,
  `msg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `phone`, `message`, `msg_date`) VALUES
(1, '24543534', 'branch_manager@jnm.com', 'branch_manager@jnm.c', 'لبابلا', '2020-07-21 04:58:45'),
(11, '7', '6', '2', 'ffgghh', '2021-08-06 12:39:21'),
(12, '7', '6', '2', 'ffgghh', '2021-08-18 20:32:27'),
(13, '7', '6', '2', 'ffgghh', '2021-09-03 16:54:20');

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE `coupon` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `num` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `coupon`
--

INSERT INTO `coupon` (`id`, `title`, `num`) VALUES
(4, '78910', 0.2),
(5, 'rrtret', 0.5);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(11) NOT NULL,
  `media` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `media`) VALUES
(2, 'images.png'),
(4, 'company_owner.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `receipt_id` int(11) DEFAULT NULL,
  `text_body` varchar(500) DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `client_id`, `receipt_id`, `text_body`, `created_date`) VALUES
(1, 1, 108, 'حالة الطلب : الكابتن وصل', '2021-09-17 12:02:33'),
(2, 1, 106, 'حالة الطلب : طلبك فى الطريق', '2021-09-18 09:09:08'),
(3, 1, 106, 'حالة الطلب : طلبك فى الطريق', '2021-09-18 09:09:55'),
(4, 1, 108, 'حالة الطلب : الكابتن وصل', '2021-09-18 09:10:10'),
(5, 1, 108, 'حالة الطلب : الكابتن وصل', '2021-09-18 09:19:43'),
(6, 1, 108, 'حالة الطلب : الكابتن وصل', '2021-09-18 09:22:26'),
(7, 11, 123, 'حالة الطلب : الكابتن وصل', '2021-09-18 09:25:54'),
(8, 11, 123, 'حالة الطلب : الكابتن وصل', '2021-09-18 09:27:41'),
(9, 11, 123, 'حالة الطلب : الكابتن وصل', '2021-09-18 09:28:31'),
(10, 1, 106, 'حالة الطلب : طلبك فى الطريق', '2021-09-18 09:50:11'),
(11, 1, 106, 'حالة الطلب : تم', '2021-09-18 09:51:16'),
(12, 11, 130, 'حالة الطلب : تم استلام الطلب', '2021-09-18 10:20:35'),
(13, 11, 131, 'حالة الطلب : طلبك فى الطريق', '2021-09-18 11:45:42'),
(14, 11, 131, 'حالة الطلب : طلبك فى الطريق', '2021-09-18 11:46:01'),
(15, 11, 131, 'حالة الطلب : طلبك فى الطريق', '2021-09-18 11:56:43'),
(16, 11, 131, 'حالة الطلب : طلبك فى الطريق', '2021-09-18 11:57:43'),
(17, 11, 131, 'حالة الطلب : طلبك فى الطريق', '2021-09-18 11:58:06'),
(18, 11, 131, 'حالة الطلب : طلبك فى الطريق', '2021-09-18 11:58:10'),
(19, 11, 131, 'حالة الطلب : طلبك فى الطريق', '2021-09-18 11:59:39'),
(20, 11, 131, 'حالة الطلب : طلبك فى الطريق', '2021-09-18 12:44:50'),
(21, 11, 131, 'حالة الطلب : الكابتن وصل', '2021-09-19 02:07:58');

-- --------------------------------------------------------

--
-- Table structure for table `offer`
--

CREATE TABLE `offer` (
  `id` int(11) NOT NULL,
  `order_company_id` int(11) NOT NULL,
  `name` text,
  `offer_type` varchar(150) DEFAULT NULL,
  `name_en` varchar(300) DEFAULT NULL,
  `price_topercentage_offer` float DEFAULT '1' COMMENT 'السعر الذى اذا تم حسابه فى السلة يتم تطبيق عرض خصم بنسبة',
  `percentage` float DEFAULT '0' COMMENT 'النسبة فى عرض خصم بنسبة',
  `items_number` float DEFAULT NULL COMMENT 'عدد المنتجات فى عرض اشترى كذا منتج والأخر مجانا',
  `shipping_discount_percentage` int(11) NOT NULL DEFAULT '0' COMMENT 'عرض التوصيل المدعوم بنسبة كم من قيمة التوصيل',
  `shipping_discount_price` int(11) NOT NULL DEFAULT '0' COMMENT 'عرض التوصيل المدعوم قيمة ثابتة',
  `itemnumber_inproductfinalcart` int(11) NOT NULL DEFAULT '0' COMMENT 'عدد المنتجات فى عرض اشترى كذا منتج والأخر مجانا على السلة النهائية',
  `comment` text,
  `created_date` text,
  `is_active` int(11) DEFAULT NULL,
  `in_offerslide` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offer`
--

INSERT INTO `offer` (`id`, `order_company_id`, `name`, `offer_type`, `name_en`, `price_topercentage_offer`, `percentage`, `items_number`, `shipping_discount_percentage`, `shipping_discount_price`, `itemnumber_inproductfinalcart`, `comment`, `created_date`, `is_active`, `in_offerslide`) VALUES
(1, 1, 'عرض خصم بنسبة', '1', 'discount percentage', 0, 0.25, 0, 0, 0, 0, '', '2020-08-23 16:00:42', 1, 1),
(2, 1, 'منتجين والثالث مجانا', '2', 'two products and the third free', 0, 0, 2, 0, 0, 0, NULL, NULL, 1, 1),
(3, 1, 'منتج ا مع منتج ب', '3', 'product a with product b free', 0, 0, 0, 0, 0, 0, NULL, NULL, 1, 1),
(4, 1, 'توصيل مدعوم', '4', 'shipping discount', 0, 0, 0, 50, 0, 0, NULL, NULL, 1, 1),
(5, 1, 'منتجين والثالث مجانا فى السلة النهائية', '5', 'two products and the third free in cart', 0, 0, 2, 0, 0, 12, NULL, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `offer_groups`
--

CREATE TABLE `offer_groups` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `products_in_group` text,
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offer_groups`
--

INSERT INTO `offer_groups` (`id`, `title`, `products_in_group`, `date_add`) VALUES
(3, 'الحديقة 3', '12,11,10,8,7,4,3,2,1', '2020-09-08 08:58:43'),
(4, 'المجموعة 2', '18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1', '2021-01-08 09:37:36'),
(6, 'الكل', '26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1', '2021-03-08 11:31:24'),
(9, 'أهل الرياض', '21,20,19', '2021-03-08 11:43:33'),
(10, 'مجموعة المدرسين', '15,14', '2021-03-08 11:46:56'),
(11, 'مجموعة العطور', '15,14,13,12', '2021-03-08 11:49:18'),
(12, 'العطور', '16,15,14', '2021-03-08 11:53:46'),
(13, 'العطور الشرقية', '5,4,3', '2021-03-08 22:04:00');

-- --------------------------------------------------------

--
-- Table structure for table `offer_types`
--

CREATE TABLE `offer_types` (
  `id` int(11) NOT NULL,
  `name` text,
  `name_en` varchar(300) DEFAULT NULL,
  `comment` text,
  `img` varchar(250) DEFAULT NULL,
  `created_date` text,
  `is_active` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offer_types`
--

INSERT INTO `offer_types` (`id`, `name`, `name_en`, `comment`, `img`, `created_date`, `is_active`) VALUES
(1, 'نسبة معينة', 'percentage discount', 'مثال خصم 50 % أو 25 % على كل المنتجات فى القائمة', NULL, NULL, 1),
(2, 'منتج ومنتج', 'product and product', 'اشتر منتجين والثالث مجانا او الثالث بنصف\nالسعر او سعر يحدده التاجر , قابل للتحديد مثل ان يشترط التاجر\nان تكون جميعها من نفس الصنف', NULL, NULL, 1),
(3, 'منتج زيادة', 'extra_product', 'شراء منتج من الصنف أ عندها تحصل على منتج من\nالصنف ب', NULL, NULL, 1),
(4, 'خصم فى الشحن', 'discountshipping', 'التوصيل المدعوم ) يتحمل التاجر تكلفة التوصيل كاملة او جزء\nمنها ) مثال خصم 51 % على التوصيل او التوصيل ب 5 ريال فقط', NULL, NULL, 1),
(5, 'product and product_infinalcart', 'product and product_infinalcart', 'اشتر منتجين والثالث مجانا او الثالث بنصف\nالسعر او سعر يحدده التاجر , قابل للتحديد مثل ان يشترط التاجر\nان تكون على السلة كاملة\nويكون العرض على الأقل سعرا', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ordertime_open_close`
--

CREATE TABLE `ordertime_open_close` (
  `id` int(11) NOT NULL,
  `order_company_id` int(11) DEFAULT NULL,
  `day` varchar(250) DEFAULT NULL,
  `open` time DEFAULT NULL,
  `close` time DEFAULT NULL,
  `state_now` varchar(250) DEFAULT NULL,
  `is_active` int(3) NOT NULL DEFAULT '1',
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ordertime_open_close`
--

INSERT INTO `ordertime_open_close` (`id`, `order_company_id`, `day`, `open`, `close`, `state_now`, `is_active`, `date_add`) VALUES
(7, 1, 'saturday', '08:00:00', '23:00:00', NULL, 1, '2021-07-11 15:46:53'),
(8, 1, 'sunday', '08:00:00', '23:00:00', NULL, 1, '2021-07-11 15:46:53'),
(9, 1, 'monday', '07:57:00', '23:00:00', NULL, 1, '2021-07-11 15:46:53'),
(10, 1, 'tuesday', '08:00:00', '23:00:00', NULL, 1, '2021-07-11 15:46:53'),
(11, 1, 'wednesday', '08:00:00', '23:00:00', NULL, 1, '2021-07-11 15:46:53'),
(12, 1, 'thursday', '08:00:00', '23:00:00', NULL, 1, '2021-07-11 15:46:53'),
(13, 1, 'friday', '08:00:00', '23:00:00', NULL, 1, '2021-07-11 15:46:53');

-- --------------------------------------------------------

--
-- Table structure for table `order_company`
--

CREATE TABLE `order_company` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `name_en` varchar(250) DEFAULT NULL,
  `logo` text,
  `logo_back` varchar(500) DEFAULT NULL,
  `haweia` varchar(250) DEFAULT NULL,
  `address` text,
  `description` varchar(250) DEFAULT NULL,
  `description_en` text,
  `latitude` varchar(250) DEFAULT NULL,
  `longitude` varchar(250) DEFAULT NULL,
  `phone` text,
  `email` varchar(350) DEFAULT NULL,
  `province` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `activity_category_id` varchar(250) DEFAULT NULL,
  `activity_subcategory_id` varchar(250) DEFAULT NULL,
  `work_time` text,
  `preparing_time_from` varchar(250) DEFAULT NULL,
  `preparing_time_to` varchar(250) DEFAULT NULL,
  `owner_name` varchar(250) DEFAULT NULL,
  `owner_phone` varchar(250) DEFAULT NULL,
  `manager_name` varchar(250) DEFAULT NULL,
  `manager_phone` varchar(250) DEFAULT NULL,
  `segel_togary_number` varchar(250) DEFAULT NULL,
  `segel_togary_photo` varchar(250) DEFAULT NULL,
  `branch_numbers` varchar(250) DEFAULT NULL,
  `detected_number` varchar(250) DEFAULT NULL,
  `bank_account` varchar(250) DEFAULT NULL,
  `has_delivery` varchar(250) DEFAULT NULL,
  `has_another_app` varchar(250) DEFAULT NULL,
  `balance` float NOT NULL DEFAULT '0',
  `password` varchar(50) NOT NULL,
  `forgot_password` text,
  `is_active` int(11) DEFAULT '0',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_company`
--

INSERT INTO `order_company` (`id`, `name`, `name_en`, `logo`, `logo_back`, `haweia`, `address`, `description`, `description_en`, `latitude`, `longitude`, `phone`, `email`, `province`, `city`, `activity_category_id`, `activity_subcategory_id`, `work_time`, `preparing_time_from`, `preparing_time_to`, `owner_name`, `owner_phone`, `manager_name`, `manager_phone`, `segel_togary_number`, `segel_togary_photo`, `branch_numbers`, `detected_number`, `bank_account`, `has_delivery`, `has_another_app`, `balance`, `password`, `forgot_password`, `is_active`, `is_deleted`, `created_date`) VALUES
(1, 'ماكدونالدز', 'Mackdonald', 'http://demo.f4h.com.sa/order/images/companies/1.jpg', 'http://demo.f4h.com.sa/order/images/companies/2.jpg', NULL, 'بنها قليوبية', 'برجر - زجبات سريعة', NULL, '30.459673297707113', '31.23527980044717', '0548270331', 'f4hf4hf4h@gmail.com', NULL, NULL, ',1,', '2', '', '30', '60', 'صاحب الشركة', '14033951', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2026, '7c4a8d09ca3762af61e59520943dc26494f8941b', '195297883', 1, 0, '2021-03-21 11:44:11'),
(2, 'أسماك بحرية', 'Fishs', 'http://demo.f4h.com.sa/order/images/companies/2.jpg', 'http://demo.f4h.com.sa/order/images/companies/3.jpg', NULL, 'الرياض', 'أسماك - جمبري', NULL, '30.459673297707113', '31.23527980044717', '0548270332', 'station2@gmail.com', NULL, NULL, ',1,', ',2,', '', '20', '40', 'صاحب الشركة', '1403395s2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '7c4a8d09ca3762af61e59520943dc26494f8941b', '545716364', 1, 0, '2021-03-21 11:44:11'),
(3, 'فطائر بلدى', 'backer', 'http://demo.f4h.com.sa/order/images/companies/3.jpg', 'http://demo.f4h.com.sa/order/images/companies/1.jpg', NULL, 'القاهرة', 'فطائر - بيتزا', NULL, '30.459673297707113', '30.459673297707113', '0548270333', 'station3@gmail.com', NULL, NULL, '555b46452dd40949416fe4473a4e199b.jpg', ',6,2,7,', '', '40', '80', 'صاحب الشركة', '1403395s3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '7c4a8d09ca3762af61e59520943dc26494f8941b', '545716364', 1, 0, '2021-03-21 11:44:11'),
(4, 'محطة 4', 'ddff', NULL, NULL, NULL, 'عنوان محطة 4', NULL, NULL, NULL, NULL, '0548270334', 'station4@gmail.com', NULL, NULL, '555b46452dd40949416fe4473a4e199b.jpg', '2', '', NULL, NULL, 'مسؤل  محطة 4', '1403395s4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '7c4a8d09ca3762af61e59520943dc26494f8941b', '545716364', 1, 0, '2021-03-21 11:44:11'),
(5, 'حطة trial22244', NULL, NULL, NULL, NULL, 'عنوان محطة tri44al', NULL, NULL, NULL, NULL, '056789344844', 'f4hss44dff@f4h.com', NULL, NULL, 'e7fbfc05fdc966fd618109d9b9c6fbf4.jpg', NULL, '22334444', NULL, NULL, 'gazzar station44', '1403395strial44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '601f1889667efaebb33b8c12572835da3f027f78', NULL, 1, 0, '2021-05-10 10:29:23'),
(6, 'محطة مهيمن للبنزين الأخضر 91', NULL, NULL, NULL, NULL, '123456', NULL, NULL, NULL, NULL, '053سسسسسسس', 'email_test@gmail.com', NULL, NULL, '', NULL, '123', NULL, NULL, '123', 'رقمالهوية', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10700, '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 1, 0, '2021-06-21 03:36:17'),
(9, 'مهيمن للديزل', 'mohaymen', NULL, NULL, NULL, 'الأحساء', NULL, NULL, NULL, NULL, '0500000000', 'email_2test@gmail.com', NULL, NULL, 'SNAG-0004.mp4', NULL, '0000', NULL, NULL, 'مهيمن', '12345', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11500, '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 1, 0, '2021-06-21 08:10:18');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `title` varchar(350) NOT NULL,
  `slug` varchar(350) NOT NULL,
  `content` text NOT NULL,
  `status` varchar(100) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `title`, `slug`, `content`, `status`, `meta_keywords`, `meta_description`) VALUES
(10, 'انضم لنا', 'انضم لنا', 'انضم لنا انضم لناانضم لنا انضم لنا انضم لنا', '1', '', ''),
(70, 'سياسة الخصوصية', 'سياسة الخصوصية', 'سياسة الخصوصية سياسة الخصوصية سياسة الخصوصية سياسة الخصوصية سياسة الخصوصية سياسة الخصوصية ', '1', '', ''),
(71, 'الشروط والأحكام', 'الشروط والأحكام', 'الشروط والأحكام الشروط والأحكام الشروط والأحكام الشروط والأحكام الشروط والأحكام الشروط والأحكام الشروط والأحكام الشروط والأحكام الشروط والأحكام الشروط والأحكام ', '1', '', ''),
(72, 'من نحن', 'من_نحن', 'من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن ', '1', 'koko , jpjp', ''),
(73, 'اتصل بنا', 'اتصل_بنا', 'اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا اتصل بنا ', '1', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `name_ar` varchar(250) NOT NULL,
  `name_en` varchar(250) NOT NULL,
  `img` varchar(100) NOT NULL,
  `state` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `name_ar`, `name_en`, `img`, `state`) VALUES
(1, 'شريك 1', 'partner 1', 'http://demo.f4h.com.sa/fullpay/images/home/partners/1.jpg', '1'),
(2, 'شريك 2', 'partner 2', 'http://demo.f4h.com.sa/fullpay/images/home/partners/2.jpg', '1'),
(3, 'شريك 3', 'partner 3', 'http://demo.f4h.com.sa/fullpay/images/home/partners/3.jpg', '1');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `product_category_id` int(11) NOT NULL,
  `order_company_id` int(11) NOT NULL,
  `title` varchar(350) NOT NULL,
  `title_en` varchar(100) DEFAULT NULL,
  `barcode_num` text,
  `price` float DEFAULT NULL,
  `photo` text,
  `comment` text,
  `comment_en` text,
  `offer_percentage_id` int(11) DEFAULT '0' COMMENT 'اى دى عرض خصم بنسبة',
  `offer_product_andproduct_id` int(11) NOT NULL DEFAULT '0' COMMENT 'اى دى عرض منتج ا ومنتج ب',
  `ab` int(11) NOT NULL DEFAULT '0' COMMENT 'يتم وضع اى دى المنتج الأخر الرئيسي المستخدم فى عرض منتج ا ومنتج ب',
  `ba` int(11) NOT NULL DEFAULT '0' COMMENT 'يتم وضع اى دى المنتج الأخر الفرعىالمستخدم فى عرض منتج ا ومنتج ب',
  `offer_extraproduct_id` int(11) NOT NULL DEFAULT '0' COMMENT 'اى دى عرض منتج وأكثر والخر مجانا (عرض صنف مجانا)',
  `extraproduct_price` int(11) NOT NULL DEFAULT '0' COMMENT 'سعر المنتج فى حالة تطبيق عرض صنف مجانا',
  `offer_extraproductincart_id` int(11) NOT NULL DEFAULT '0' COMMENT 'اى دى عرض منتج وأكثر والخر مجانا (عرض صنف مجانا) فقط فى السلة',
  `offer_discountshipping_id` int(11) NOT NULL COMMENT 'اى دى عرض التوصيل المدعوم',
  `number_of_sells` int(11) NOT NULL DEFAULT '0' COMMENT 'عدد مبيعات المنتج',
  `created_date` text NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_category_id`, `order_company_id`, `title`, `title_en`, `barcode_num`, `price`, `photo`, `comment`, `comment_en`, `offer_percentage_id`, `offer_product_andproduct_id`, `ab`, `ba`, `offer_extraproduct_id`, `extraproduct_price`, `offer_extraproductincart_id`, `offer_discountshipping_id`, `number_of_sells`, `created_date`, `is_deleted`, `is_active`) VALUES
(1, 1, 1, 'لوحة مفاتيح', 'KeyBoard', '826876480002', 50, 'http://demo.f4h.com.sa/sultana/images/cp/products/funct.jpg', '', NULL, 1, 0, 0, 0, 0, 0, 0, 0, 1, '2020-09-08 09:00:32', 0, 1),
(2, 3, 1, 'ماوس', 'Mouse', '860366564006', 40, 'http://demo.f4h.com.sa/sultana/images/cp/products/HTB1adWlaLfsK1RjSszgq6yXzpXaX.jpg_350x350_.jpg', '', NULL, 0, 0, 0, 0, 2, 50, 0, 0, 5, '2020-09-08 09:01:12', 0, 1),
(3, 1, 1, 'عطر Joy Dior', 'عطر Joy Dior', '149301951890', 300, 'http://demo.f4h.com.sa/sultana/images/cp/products/4460791-1230744024.jpg', '', NULL, NULL, 3, 2, 0, 0, 0, 0, 0, 0, '2020-09-08 09:02:15', 0, 1),
(4, 1, 1, 'عطر الورود', 'Rose perfumes', '521558286705', 500, 'http://demo.f4h.com.sa/sultana/images/cp/products/9999026109.jpg', '', NULL, 0, 0, 0, 0, 0, 0, 0, 4, 2, '2020-09-08 09:03:13', 0, 1),
(5, 0, 0, 'عطر الذهبي', 'Golden Perfuim', '945767728896', 10, 'http://demo.f4h.com.sa/sultana/images/cp/products/emirates-voiceghgfdf.gif', '', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-09-08 09:05:03', 0, 1),
(6, 3, 1, 'كريك', 'كريك', '544485425584', 10, 'http://demo.f4h.com.sa/sultana/images/cp/products/YT-8865--1_776x591.jpg', '', NULL, NULL, 0, 0, 0, 0, 0, 0, 4, 0, '2020-09-08 09:06:03', 0, 1),
(7, 3, 1, 'برويطة', 'برويطة', '215665963906', 300, 'http://demo.f4h.com.sa/sultana/images/cp/products/bdvth3.jpg', '', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-09-08 09:07:37', 0, 1),
(8, 0, 1, 'ماء اكوافينا', 'water1', '012000014383', 0, '', '', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-10-28 20:41:00', 0, 1),
(9, 0, 0, 'ماء اكوافينا 1', 'water2', '6287008660014', 20, '', '', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-10-28 20:46:23', 0, 1),
(10, 0, 0, 'تجربة 5', 'test5', '102263932p', 200, '', '', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-10-28 20:48:17', 0, 1),
(11, 5, 1, 'مربع', 'Murabe3', '3850102323036', 10, '', '', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-10-28 21:06:54', 0, 1),
(12, 5, 1, 'عطر حسن', 'hasssan', '024300044168', 40, '', '', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-01 15:07:29', 0, 1),
(13, 0, 0, 'محمد', 'hasssan5', '6287008590090', 8, '', '', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-01 15:40:07', 0, 1),
(14, 0, 0, 'امنتي', 'AIMANTE', '6390902022861', 0, '', '', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 17:45:36', 0, 1),
(15, 0, 0, 'سيارة', 'car', 'jghdghkjdhgdhg', 345, '', '', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-24 14:43:16', 0, 1),
(16, 0, 0, 'sajed', '', '411458313199', 30, '', '', NULL, NULL, 0, 0, 0, 0, 0, 0, 4, 0, '2021-01-21 17:41:48', 0, 1),
(17, 0, 0, 'dggdfhfg', '', '986386337208', 8, '', '', NULL, NULL, 0, 0, 0, 0, 0, 0, 4, 0, '2021-01-21 17:52:33', 0, 1),
(18, 0, 0, 'e', 'we', '600806512854', 10, '', '', NULL, NULL, 0, 0, 0, 0, 0, 5, 0, 0, '2021-01-21 17:53:14', 0, 1),
(19, 0, 0, 'تجربة', 'test', '897396663941', 120, 'http://demo.f4h.com.sa/sultana/images/cp/products/س.png', 'vc', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2021-02-05 08:55:44', 0, 1),
(20, 0, 0, '5تي اش', '5th avenue', '6290845151577', 150, '', 'تكيز80% تاريخ الانتاج2019/05/1', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2021-02-21 13:40:32', 0, 1),
(21, 0, 0, 'هاي كلاس نوير', 'High Class noir', '6281074713957', 130.43, '', '', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2021-02-21 13:48:14', 0, 1),
(22, 0, 0, 'عرض افتتاح القارة', '', '564609899590', 52.17, '', '', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2021-02-21 13:55:46', 0, 1),
(23, 0, 0, 'فلورز', 'flowers', '6287006662294', 134.78, '', '', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2021-03-01 14:16:14', 0, 1),
(24, 0, 0, 'تجربة 60', 'test60', '339644344406', 52.17, '', '', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2021-03-03 11:10:15', 0, 1),
(25, 0, 0, 'ee', 'ee', '510750131949', 90, '', '', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2021-03-03 11:13:59', 0, 1),
(26, 0, 0, 'ايس', 'ice', '6287006662218', 10, '', '', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2021-04-05 18:50:29', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products_categories`
--

CREATE TABLE `products_categories` (
  `id` int(11) NOT NULL,
  `order_company_id` int(11) DEFAULT NULL,
  `title` text NOT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `content` text,
  `content_en` varchar(250) DEFAULT NULL,
  `photo` text NOT NULL,
  `for_menue` int(11) NOT NULL DEFAULT '0',
  `is_active` int(3) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products_categories`
--

INSERT INTO `products_categories` (`id`, `order_company_id`, `title`, `title_en`, `content`, `content_en`, `photo`, `for_menue`, `is_active`, `is_deleted`, `date_add`) VALUES
(1, 1000000, 'الأكثر مبيعا', 'Most Sales', 'الأكثر مبيعا', 'Most Sales', '', 1, 1, 0, '2021-04-05 18:49:45'),
(2, 1000000, 'العروض', 'Offers', 'العروض', 'Offers', '', 1, 1, 0, '2021-04-05 18:49:45'),
(3, 1, 'مشويات', NULL, 'مشويات', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/720181413512197617633.jpg', 0, 1, 0, '2020-09-08 08:58:43'),
(4, 2, 'محلات الأيس كريم', NULL, 'محلات الأيس كريم', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/Picture1.jpg', 0, 1, 0, '2021-02-05 09:12:44'),
(5, 1, 'حلويات', NULL, 'حلويات', NULL, '', 0, 1, 0, '2021-02-28 18:12:29'),
(6, 3, 'المطابخ وتجهيز الحفلات', NULL, 'المطابخ وتجهيز الحفلات', NULL, '', 0, 1, 0, '2021-04-05 18:49:45'),
(9, 1, 'برجر', NULL, 'برجر', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/15772836555800.jpg', 0, 1, 0, '2020-09-08 08:55:18'),
(10, 1, 'المقاهى', NULL, 'المقاهى', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/hqdefault.jpg', 0, 1, 0, '2020-09-08 08:57:07');

-- --------------------------------------------------------

--
-- Table structure for table `product_options`
--

CREATE TABLE `product_options` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_company_id` int(11) NOT NULL,
  `option_categ_id` int(11) DEFAULT NULL,
  `option_categ_title` varchar(250) DEFAULT NULL,
  `option_categ_title_en` varchar(500) DEFAULT NULL,
  `option_subcatg_id` int(11) DEFAULT NULL,
  `option_subcatg_title` varchar(250) DEFAULT NULL,
  `option_subcatg_title_en` varchar(500) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `created_date` text NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_options`
--

INSERT INTO `product_options` (`id`, `product_id`, `order_company_id`, `option_categ_id`, `option_categ_title`, `option_categ_title_en`, `option_subcatg_id`, `option_subcatg_title`, `option_subcatg_title_en`, `price`, `created_date`, `is_deleted`, `is_active`) VALUES
(1, 1, 1, 1, 'الحجم', NULL, 1, 'صغير', NULL, 100, '', 0, 1),
(2, 1, 1, 1, 'الحجم', NULL, 2, 'وسط', NULL, 200, '', 0, 1),
(3, 1, 1, 1, 'الحجم', NULL, 3, 'كبير', NULL, 300, '', 0, 1),
(4, 1, 1, 3, 'المشروب', NULL, 6, 'بيبسي', NULL, 20, '', 0, 1),
(5, 1, 1, 3, 'المشروب', NULL, 7, 'كوكا', NULL, 40, '', 0, 1),
(6, 4, 1, 2, 'الإضافات', NULL, 4, 'جبنة', NULL, 20, '', 0, 1),
(7, 1, 1, 2, 'الإضافات', NULL, 5, 'كاتشاب', NULL, 40, '', 0, 1),
(8, 1, 1, 2, 'الإضافات', NULL, 4, 'جبنة', NULL, 20, '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `prod_opti_categ`
--

CREATE TABLE `prod_opti_categ` (
  `id` int(11) NOT NULL,
  `order_company_id` int(11) NOT NULL,
  `optional` int(11) NOT NULL DEFAULT '0',
  `add_to_product_price` int(11) NOT NULL DEFAULT '0' COMMENT 'عند عرض الاختيار فى التطبيق هل يتم اضافة سعر المنتج اليه',
  `title` text NOT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `content` text,
  `content_en` varchar(250) DEFAULT NULL,
  `photo` text NOT NULL,
  `is_active` int(3) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prod_opti_categ`
--

INSERT INTO `prod_opti_categ` (`id`, `order_company_id`, `optional`, `add_to_product_price`, `title`, `title_en`, `content`, `content_en`, `photo`, `is_active`, `is_deleted`, `date_add`) VALUES
(1, 1, 0, 1, 'الحجم', NULL, 'الحجم', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/15772836555800.jpg', 1, 0, '2020-09-08 08:55:18'),
(2, 1, 1, 0, 'الإضافات', NULL, 'الإضافات', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/hqdefault.jpg', 1, 0, '2020-09-08 08:57:07'),
(3, 1, 1, 0, 'المشروبات', NULL, 'المشروبات', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/720181413512197617633.jpg', 1, 0, '2020-09-08 08:58:43');

-- --------------------------------------------------------

--
-- Table structure for table `prod_opti_subcat`
--

CREATE TABLE `prod_opti_subcat` (
  `id` int(11) NOT NULL,
  `order_company_id` int(11) NOT NULL,
  `opti_categ_id` int(11) NOT NULL,
  `opti_categ_title` varchar(250) NOT NULL,
  `title` text NOT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `photo` text NOT NULL,
  `is_active` int(3) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prod_opti_subcat`
--

INSERT INTO `prod_opti_subcat` (`id`, `order_company_id`, `opti_categ_id`, `opti_categ_title`, `title`, `title_en`, `photo`, `is_active`, `is_deleted`, `date_add`) VALUES
(1, 1, 1, 'الحجم', 'صغير', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/15772836555800.jpg', 1, 0, '2020-09-08 08:55:18'),
(2, 1, 1, 'الحجم', 'كبير', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/hqdefault.jpg', 1, 0, '2020-09-08 08:57:07'),
(3, 1, 1, 'الحجم', 'وسط', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/720181413512197617633.jpg', 1, 0, '2020-09-08 08:58:43'),
(4, 1, 2, 'الإضافات', 'جبنة', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/15772836555800.jpg', 1, 0, '2020-09-08 08:55:18'),
(5, 1, 2, 'الإضافات', 'كاتشاب', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/hqdefault.jpg', 1, 0, '2020-09-08 08:57:07'),
(6, 1, 3, 'المشروبات', 'بيبسي', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/15772836555800.jpg', 1, 0, '2020-09-08 08:55:18'),
(7, 1, 2, 'المشروبات', 'كوكا', NULL, 'http://demo.f4h.com.sa/sultana/images/cp/productsDeps/hqdefault.jpg', 1, 0, '2020-09-08 08:57:07');

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `rate` float NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `comment` text,
  `date_now` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`id`, `company_id`, `rate`, `client_id`, `comment`, `date_now`) VALUES
(1, 1, 5, 99, 'السلام عليكم ', '2021-03-21 15:37:23'),
(2, 2, 3.5, 6, 'Test', '2021-04-06 15:22:28'),
(3, 3, 4, 6, 'T', '2021-04-06 15:23:32'),
(4, 1, 3, 3, 'T', '2021-04-06 15:28:11'),
(6, 7, 6, 2, 'ffgghh', '2021-08-06 13:45:33'),
(7, 7, 6, 2, 'ffgghh', '2021-08-18 16:15:55'),
(8, 7, 6, 2, 'ffgghh', '2021-08-25 18:47:01'),
(9, 7, 6, 2, 'ffgghh', '2021-08-25 18:47:03'),
(10, 7, 6, 2, 'ffgghh', '2021-08-25 18:47:14'),
(11, 11, 11, 11, '11', '2021-09-05 07:55:40');

-- --------------------------------------------------------

--
-- Table structure for table `receipt`
--

CREATE TABLE `receipt` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `tax` float DEFAULT NULL,
  `sum_productsprice` float DEFAULT NULL,
  `receipt_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `receipt_date_back` datetime DEFAULT NULL,
  `client_address_id` int(11) DEFAULT NULL,
  `payment_method` varchar(250) DEFAULT NULL,
  `payment_method_cardnumber` varchar(250) DEFAULT NULL,
  `expected_time_torecive` time DEFAULT NULL,
  `receiving_method` varchar(250) DEFAULT NULL,
  `shipping_cost` float NOT NULL DEFAULT '0',
  `car_type` varchar(250) DEFAULT NULL,
  `car_color` varchar(250) DEFAULT NULL,
  `car_cpanel_number` varchar(250) DEFAULT NULL,
  `car_comment` varchar(250) DEFAULT NULL,
  `cupon_title` varchar(250) DEFAULT NULL,
  `cupon_id` int(11) DEFAULT NULL,
  `cupon_num` float DEFAULT NULL,
  `total_before_cupon` float DEFAULT NULL,
  `total_after_cupon` float DEFAULT NULL,
  `final_receipt_value` float DEFAULT NULL,
  `captain_id` int(11) DEFAULT NULL,
  `state` varchar(150) DEFAULT '1',
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `receipt`
--

INSERT INTO `receipt` (`id`, `client_id`, `company_id`, `tax`, `sum_productsprice`, `receipt_date`, `receipt_date_back`, `client_address_id`, `payment_method`, `payment_method_cardnumber`, `expected_time_torecive`, `receiving_method`, `shipping_cost`, `car_type`, `car_color`, `car_cpanel_number`, `car_comment`, `cupon_title`, `cupon_id`, `cupon_num`, `total_before_cupon`, `total_after_cupon`, `final_receipt_value`, `captain_id`, `state`, `comment`) VALUES
(106, 1, 1, 0, 2730, '2021-08-13 18:20:21', NULL, 10, '01234567', NULL, NULL, 'توصيل', 215.598, '01234567', '01234567', '01234567', '01234567', 'rrtret', 5, 0.5, 2945.6, 1472.8, 1472.8, 1, '9', '01234567'),
(107, 1, 1, 0, 2730, '2021-08-13 18:21:11', NULL, 10, '01234567', NULL, NULL, 'توصيل', 215.598, '01234567', '01234567', '01234567', '01234567', 'rrtret', 5, 0.5, 2945.6, 1472.8, 1472.8, 1, '9', '01234567'),
(108, 1, 1, 0, 2730, '2021-08-13 18:21:36', NULL, 10, '01234567', NULL, NULL, 'توصيل', 215.598, '01234567', '01234567', '01234567', '01234567', 'rrtret', 5, 0.5, 2945.6, 1472.8, 1472.8, 8, '6', '01234567'),
(109, 1, 1, 0, 3080, '2021-08-18 15:07:49', NULL, 10, '01234567', NULL, NULL, 'توصيل', 215.598, '01234567', '01234567', '01234567', '01234567', 'rrtret', 5, 0.5, 3295.6, 1647.8, 1647.8, 2, '1', '01234567'),
(110, 2, 1, 0, 2000, '2021-08-18 15:10:11', NULL, 10, '01234567', NULL, NULL, 'توصيل', 215.598, '01234567', '01234567', '01234567', '01234567', 'rrtret', 5, 0.5, 2215.6, 1107.8, 1107.8, 1, '9', '01234567'),
(111, 2, 1, 0, 2000, '2021-08-18 15:14:52', NULL, 10, '01234567', NULL, NULL, 'توصيل', 215.598, '01234567', '01234567', '01234567', '01234567', 'rrtret', 5, 0.5, 2215.6, 1107.8, 1107.8, NULL, '1', '01234567'),
(112, 1, 1, 0, 1950, '2021-09-02 14:40:43', NULL, 10, '01234567', NULL, NULL, 'توصيل', 215.598, '01234567', '01234567', '01234567', '01234567', 'rrtret', 5, 0.5, 2165.6, 1082.8, 1082.8, NULL, '7', '01234567'),
(113, 11, 1, 0, 650, '2021-09-03 08:48:39', NULL, 16, '', NULL, NULL, 'استلام من الفرع', 0, '', '', '', '', '', 0, 0, 650, 650, 650, NULL, '7', ''),
(114, 11, 1, 0, 650, '2021-09-04 05:26:07', NULL, 16, '', NULL, NULL, 'استلام من الفرع', 0, '', '', '', '', '', 0, 0, 650, 650, 650, NULL, '7', ''),
(115, 11, 1, 0, 20, '2021-09-04 05:27:16', NULL, 16, '', NULL, NULL, NULL, 0, '', '', '', '', '', 0, 0, 20, 20, 20, NULL, '7', ''),
(117, 11, 1, 0, 10, '2021-09-04 05:37:04', NULL, 16, '', NULL, NULL, 'استلام من الفرع', 0, '', '', '', '', '', 0, 0, 10, 10, 10, NULL, '7', ''),
(118, 1, 1, 0, 2730, '2021-08-13 18:21:36', NULL, 10, '01234567', NULL, NULL, 'توصيل', 215.598, '01234567', '01234567', '01234567', '01234567', 'rrtret', 5, 0.5, 2945.6, 1472.8, 1472.8, 2, '9', '01234567'),
(119, 1, 1, 0, 3080, '2021-08-18 15:07:49', NULL, 10, '01234567', NULL, NULL, 'توصيل', 215.598, '01234567', '01234567', '01234567', '01234567', 'rrtret', 5, 0.5, 3295.6, 1647.8, 1647.8, 2, '9', '01234567'),
(120, 11, 1, 0, 1310, '2021-09-04 14:51:00', NULL, 16, '', NULL, NULL, 'استلام من الفرع', 0, '', '', '', '', '', 0, 0, 1310, 1310, 1310, NULL, '7', ''),
(121, 11, 1, 0, 620, '2021-09-05 07:53:18', NULL, 16, '', NULL, NULL, 'استلام من الفرع', 0, '', '', '', '', '', 0, 0, 620, 620, 620, NULL, '7', ''),
(122, 11, 1, 0, 390, '2021-09-06 04:56:13', NULL, 16, '', NULL, NULL, 'استلام من الفرع', 0, '', '', '', '', '', 0, 0, 390, 390, 390, NULL, '7', ''),
(123, 11, 1, 0, 620, '2021-09-15 02:54:25', NULL, 16, '', NULL, NULL, 'استلام من السيارة', 0, 'تويوتا ', 'ابيض', '533836', '', '', 0, 0, 620, 620, 620, NULL, '6', ''),
(124, 11, 1, 0, 430, '2021-09-15 02:57:15', NULL, 16, '', NULL, NULL, 'استلام من الفرع', 0, '', '', '', '', '', 0, 0, 430, 430, 430, NULL, '2', ''),
(125, 11, 1, 0, 460, '2021-09-18 10:10:01', NULL, 0, '', NULL, NULL, 'استلام من الفرع', 0, '', '', '', '', '', 0, 0, 460, 460, 460, NULL, '1', ''),
(127, 1, 1, 0, 10, '2021-09-18 10:11:37', NULL, 10, '01234567', NULL, NULL, 'توصيل', 215.598, '01234567', '01234567', '01234567', '01234567', 'rrtret', 5, 0.5, 225.598, 112.799, 112.799, NULL, '1', '01234567'),
(130, 11, 1, 0, 390, '2021-09-18 10:17:27', NULL, 26, '', NULL, NULL, 'استلام من الفرع', 0, '', '', '', '', '', 0, 0, 390, 390, 390, 1, '2', ''),
(131, 11, 1, 0, 580, '2021-09-18 11:37:21', NULL, 26, '', NULL, NULL, 'استلام من الفرع', 0, '', '', '', '', '', 0, 0, 580, 580, 580, 1, '6', ''),
(132, 11, 1, 0, 127.5, '2021-09-20 05:34:02', NULL, 29, '', NULL, NULL, 'استلام من الفرع', 0, '', '', '', '', '', 0, 0, 127.5, 127.5, 127.5, NULL, '1', ''),
(133, 11, 1, 0, 52.5, '2021-09-23 05:23:53', NULL, 29, '', NULL, NULL, 'استلام من الفرع', 0, '', '', '', '', '', 0, 0, 52.5, 52.5, 52.5, NULL, '1', ''),
(134, 11, 1, 0, 77.5, '2021-09-23 12:23:03', NULL, 0, '', NULL, NULL, 'استلام من السيارة', 0, '', '', '', '', '', 0, 0, 77.5, 77.5, 77.5, NULL, '1', ''),
(137, 11, 1, 0, 72.5, '2021-09-23 12:23:46', NULL, 29, '', NULL, NULL, 'استلام من الفرع', 0, '', '', '', '', '', 0, 0, 72.5, 72.5, 72.5, NULL, '1', ''),
(138, 11, 1, 0, 92.5, '2021-09-23 14:15:12', NULL, 29, '', NULL, NULL, 'استلام من الفرع', 0, '', '', '', '', '', 0, 0, 92.5, 92.5, 92.5, NULL, '1', ''),
(139, 11, 1, 0, 62.5, '2021-09-23 14:16:45', NULL, 29, '', NULL, NULL, 'استلام من الفرع', 0, '', '', '', '', '', 0, 0, 62.5, 62.5, 62.5, NULL, '1', ''),
(140, 11, 1, 0, 1000, '2021-09-23 15:23:18', NULL, 29, '', NULL, NULL, 'استلام من الفرع', 0, '', '', '', '', '', 0, 0, 1000, 1000, 1000, NULL, '1', ''),
(141, 0, 1, 0, 155, '2021-09-23 16:09:05', NULL, 0, '', NULL, NULL, 'استلام من السيارة', 0, '', '', '', '', '', 0, 0, 155, 155, 155, NULL, '1', ''),
(145, 7, 1, 0, 42.5, '2021-09-23 16:12:38', NULL, 30, '', NULL, NULL, 'استلام من الفرع', 0, '', '', '', '', '', 0, 0, 42.5, 42.5, 42.5, NULL, '7', ''),
(146, 7, 1, 0, 402.5, '2021-09-23 21:44:53', NULL, 0, '', NULL, NULL, 'استلام من الفرع', 0, '', '', '', '', '', 0, 0, 402.5, 402.5, 402.5, NULL, '1', '');

-- --------------------------------------------------------

--
-- Table structure for table `receipt_details`
--

CREATE TABLE `receipt_details` (
  `id` int(11) NOT NULL,
  `receipt_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `final_item_price` float NOT NULL DEFAULT '0',
  `product_options` text,
  `sub_product_options` text,
  `offer_id` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT '1',
  `comment` text,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `receipt_details`
--

INSERT INTO `receipt_details` (`id`, `receipt_id`, `client_id`, `product_id`, `company_id`, `amount`, `final_item_price`, `product_options`, `sub_product_options`, `offer_id`, `state`, `comment`, `created_date`) VALUES
(21, 106, 1, 1, 1, 6, 210, 'tt', 'ww', NULL, 1, 'nn', '2021-08-13 15:04:48'),
(22, 106, 1, 6, 1, 4, 20, NULL, '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(23, 106, 1, 3, 1, 5, 1500, NULL, '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(24, 106, 1, 2, 1, 2, 500, '1,5,7', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(25, 106, 1, 4, 1, 1, 500, '2,4,7', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(26, 107, 1, 1, 1, 6, 210, 'tt', 'ww', NULL, 1, 'nn', '2021-08-13 15:04:48'),
(27, 107, 1, 6, 1, 4, 20, NULL, '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(28, 107, 1, 3, 1, 5, 1500, NULL, '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(29, 107, 1, 2, 1, 2, 500, '1,5,7', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(30, 107, 1, 4, 1, 1, 500, '2,4,7', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(31, 108, 1, 1, 1, 6, 210, 'tt', 'ww', NULL, 1, 'nn', '2021-08-13 15:04:48'),
(32, 108, 1, 6, 1, 4, 20, NULL, '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(33, 108, 1, 3, 1, 5, 1500, NULL, '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(34, 108, 1, 2, 1, 2, 500, '1,5,7', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(35, 108, 1, 4, 1, 1, 500, '2,4,7', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(36, 109, 1, 1, 1, 6, 210, '', 'ww', NULL, 1, 'nn', '2021-08-13 15:04:48'),
(37, 109, 1, 2, 1, 2, 20, '', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(38, 109, 1, 3, 1, 5, 1500, '', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(39, 109, 1, 4, 1, 1, 500, '', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(40, 109, 1, 1, 1, 6, 850, '1,5,8', 'ffgghh', NULL, 1, 'ffgghh', '2021-08-13 21:59:50'),
(41, 110, 2, 3, 1, 5, 1500, '', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(42, 110, 2, 4, 1, 1, 500, '', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(43, 111, 2, 3, 1, 5, 1500, '', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(44, 111, 2, 4, 1, 1, 500, '', '', NULL, NULL, NULL, '2021-07-21 13:01:03'),
(45, 112, 1, 1, 1, 6, 1260, '1,5,8', 'ffgghh', NULL, 1, 'ffgghh', '2021-08-13 21:59:50'),
(46, 113, 11, 1, 1, 1, 650, '1, 2, 3', '1, 5, 6', NULL, 1, '', '2021-09-02 07:16:30'),
(47, 114, 11, 1, 1, 1, 650, '1,2,3', '2,4,7', NULL, 1, '', '2021-09-03 11:53:42'),
(48, 115, 11, 2, 1, 2, 20, '', '', NULL, 1, '', '2021-09-04 05:27:04'),
(49, 117, 11, 2, 1, 1, 10, '', '', NULL, 1, '', '2021-09-04 05:34:34'),
(50, 120, 11, 2, 1, 1, 10, '', '', NULL, 1, '', '2021-09-04 05:52:26'),
(51, 120, 11, 1, 1, 2, 1300, '1,2,3', '1,5,6', NULL, 1, '', '2021-09-04 05:53:58'),
(52, 121, 11, 1, 1, 2, 620, '2,5,6', '', NULL, 1, '', '2021-09-05 07:52:52'),
(53, 122, 11, 1, 1, 1, 390, '3,4,6', '', NULL, 1, '', '2021-09-06 04:55:41'),
(54, 123, 11, 1, 1, 2, 620, '2,5,6', '', NULL, 1, '', '2021-09-15 02:53:24'),
(55, 124, 11, 1, 1, 1, 430, '3,5,7', '', NULL, 1, '', '2021-09-15 02:57:01'),
(56, 125, 11, 1, 1, 2, 460, '1,5,7', '', NULL, 1, '', '2021-09-18 10:08:58'),
(57, 127, 1, 2, 1, 1, 10, '', '', NULL, 1, '', '2021-09-02 07:16:20'),
(58, 130, 11, 1, 1, 1, 390, '3,4,6', '', NULL, 1, '', '2021-09-18 10:17:18'),
(59, 131, 11, 1, 1, 2, 580, '2,4,6', '', NULL, 1, '', '2021-09-18 11:37:12'),
(60, 132, 11, 1, 1, 2, 55, '5,6', '', NULL, 1, '', '2021-09-20 05:28:30'),
(61, 132, 11, 1, 1, 1, 72.5, '2,4,6', '', NULL, 1, '', '2021-09-20 05:33:15'),
(62, 133, 11, 1, 1, 1, 52.5, '1,4,7', '', NULL, 1, '', '2021-09-23 05:23:39'),
(63, 134, 11, 1, 1, 1, 77.5, '2,4,7', '', NULL, 1, '', '2021-09-23 12:22:37'),
(64, 137, 11, 1, 1, 1, 72.5, '2,4,6', '', NULL, 1, '', '2021-09-23 12:23:37'),
(65, 138, 11, 1, 1, 1, 92.5, '2,4,5,6,7', '', NULL, 1, '', '2021-09-23 14:15:00'),
(66, 139, 11, 1, 1, 1, 62.5, '2', '', NULL, 1, '', '2021-09-23 14:16:38'),
(67, 140, 11, 4, 1, 2, 1000, '', '', NULL, 1, '', '2021-09-23 15:22:50'),
(68, 141, 0, 1, 1, 1, 117.5, '1,5,1,5,1,5', '', NULL, 1, '', '2021-09-23 15:03:10'),
(69, 141, 0, 1, 1, 1, 37.5, '1', '', NULL, 1, '', '2021-09-23 16:08:00'),
(70, 145, 7, 1, 1, 1, 42.5, '4,1', '', NULL, 1, '', '2021-09-23 16:12:21'),
(71, 146, 7, 1, 1, 7, 402.5, '7,5,1', '', NULL, 1, '', '2021-09-23 17:07:06');

-- --------------------------------------------------------

--
-- Table structure for table `receipt_state`
--

CREATE TABLE `receipt_state` (
  `id` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `receipt_state`
--

INSERT INTO `receipt_state` (`id`, `title`) VALUES
(1, 'قيد الإنتظار'),
(2, 'تم استلام الطلب'),
(3, 'طلبك قيد التحضير'),
(4, 'الطلب جاهز'),
(5, 'طلبك فى الطريق'),
(6, 'الكابتن وصل'),
(7, 'الغاء الطلب'),
(8, 'الغاء الطلب من الكابتن'),
(9, 'تم'),
(44, 'الطلب جاهز فى انتظار موافقة الكابتن');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `title_ar` varchar(350) NOT NULL,
  `title_en` varchar(350) NOT NULL,
  `content_ar` text NOT NULL,
  `content_en` text NOT NULL,
  `img` text NOT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `state` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL,
  `dep_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `title_ar`, `title_en`, `content_ar`, `content_en`, `img`, `icon`, `state`, `date`, `dep_id`) VALUES
(1, 'بنزين 91', 'Petrol 91', 'بنزين 91', 'Petrol 91', '', NULL, '1', '', 1),
(2, 'بنزين 95', 'Petrol 95', 'بنزين 95', 'Petrol 95', '', NULL, '1', '', 1),
(3, 'ديزل', 'Diesel', 'ديزل', 'Diesel', '', NULL, '1', '', 1),
(4, 'غسيل سيارة', 'Car Clean', 'غسيل سيارة', 'Car Clean', '', NULL, '1', '', 2),
(5, 'تغيير زيت', 'Change Oil', 'تغيير زيت', 'Change Oil', '', NULL, '1', '', 3),
(41, 'توصيل وقود', 'delivery oil', 'توصيل وقود', 'delivery oil', '', NULL, '1', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `services_deps`
--

CREATE TABLE `services_deps` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `photo` text,
  `content` text,
  `conte_en` text,
  `post_icon` varchar(250) DEFAULT NULL,
  `date_add` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services_deps`
--

INSERT INTO `services_deps` (`id`, `title`, `title_en`, `photo`, `content`, `conte_en`, `post_icon`, `date_add`) VALUES
(1, 'وقود', NULL, 'https://demo.f4h.com.sa/fullpay/images/home/services_deps/fuel.png', ' وقود وقود وقود وقود وقود وقود وقود وقود  وقود وقود وقود وقود وقود وقود وقود وقود وقود وقود وقود', NULL, 'fa fa-comments-o', '2020-09-08 08:55:18'),
(2, 'غسيل سيارات', NULL, 'https://demo.f4h.com.sa/fullpay/images/home/services_deps/car_wash.png', 'غسيل سيارات غسيل سيارات غسيل سيارات غسيل سيارات غسيل سيارات غسيل سيارات ', NULL, 'fa fa-leaf', '2020-09-08 08:57:07'),
(3, 'صيانة', NULL, 'https://demo.f4h.com.sa/fullpay/images/home/services_deps/maintenance.png', 'صيانة صيانة صيانة صيانة صيانة صيانة صيانة صيانة صيانة صيانة صيانة صيانة صيانة ', NULL, 'fa fa-support', '2020-09-08 08:58:43');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(120) COLLATE utf8_bin DEFAULT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('01ea6dec8935fcb878d1bc674bb9021e', '102.185.38.165', 'okhttp/4.9.0', 1632448123, ''),
('06b27ceab5889226458152c481db6b4a', '102.185.38.165', 'okhttp/4.9.0', 1632448245, ''),
('0b0a311b64a3e483b9ea6eddadb304bb', '102.185.38.165', 'okhttp/4.9.0', 1632442456, ''),
('0c119d9ce322553b47fed45042148870', '102.185.38.165', 'okhttp/4.9.0', 1632448185, ''),
('0dd6e8d35a00b2eadea63e86c3269927', '102.185.38.165', 'okhttp/4.9.0', 1632448165, ''),
('0fe5fa85d34b781853eaaf02b4685fef', '102.185.38.165', 'okhttp/4.9.0', 1632448057, ''),
('131a207bf8954b611793f738181e5b6c', '102.185.38.165', 'okhttp/4.9.0', 1632449802, ''),
('13feaa97f60e2af846cded25cf8d2c12', '102.185.38.165', 'okhttp/4.9.0', 1632448295, ''),
('155e9a27f52422fbb09299acba9b2ff2', '102.185.38.165', 'okhttp/4.9.0', 1632448030, ''),
('16c573c582841ce6fb8c5415cb9db827', '102.185.38.165', 'okhttp/4.9.0', 1632448262, ''),
('16f44aa8dddb47f921e8c1cbeb6c607e', '102.185.38.165', 'okhttp/4.9.0', 1632449843, ''),
('1816cbcaccc8bd6d5801f325c6fdc0ce', '102.185.38.165', 'okhttp/4.9.0', 1632448123, ''),
('1abf8d3330c2dcd01f986451e6aab593', '102.185.38.165', 'okhttp/4.9.0', 1632448088, ''),
('1c630ad0321f56c7ec9b48ff43c00c04', '102.185.38.165', 'okhttp/4.9.0', 1632448088, ''),
('1d6f0ae45557321e41be1dd2c2537209', '102.185.38.165', 'okhttp/4.9.0', 1632448253, ''),
('20d93a65ee5fa1b18e6d6dcc235bf8be', '102.185.38.165', 'okhttp/4.9.0', 1632448144, ''),
('22a9f1f4411ec394fc9ac8c9058ffb87', '102.185.38.165', 'okhttp/4.9.0', 1632448202, ''),
('232bc7b09b53c58a2a45e118ef779b5e', '102.185.38.165', 'okhttp/4.9.0', 1632448110, ''),
('237eb6e381c44bc5b30b8c370509fc0d', '102.185.38.165', 'okhttp/4.9.0', 1632448260, ''),
('24767c2dba37cd4e2ac775e1a2a548d4', '102.185.38.165', 'okhttp/4.9.0', 1632448215, ''),
('2580c5023abb5add42b7b183ceb38e27', '102.185.38.165', 'okhttp/4.9.0', 1632448030, ''),
('264aba42c2690a47cc70aa28f0906eea', '102.185.38.165', 'okhttp/4.9.0', 1632447918, ''),
('29f27ea36a9459326d4cabf532928269', '102.185.38.165', 'okhttp/4.9.0', 1632447982, ''),
('2a820d23ca0fcdd4e5e32d6c45ffe507', '102.185.38.165', 'okhttp/4.9.0', 1632448025, ''),
('2aa031b395205c8a8217e5026dca2f0b', '102.185.38.165', 'okhttp/4.9.0', 1632449832, ''),
('2befdaa16f37c267f07cacc2284282fc', '102.185.38.165', 'okhttp/4.9.0', 1632449799, ''),
('2c705559f3209fcfbdcdee17fda13aa1', '102.185.38.165', 'okhttp/4.9.0', 1632447943, ''),
('2d97d07af05343686075ebb7fa6372cb', '102.185.38.165', 'okhttp/4.9.0', 1632448190, ''),
('2e2e4056586e8a53ac67b43b64ec1763', '102.185.38.165', 'okhttp/4.9.0', 1632448190, ''),
('2f50a5c63edaf832f92d37745752447b', '102.185.38.165', 'okhttp/4.9.0', 1632448295, ''),
('2fdc4bddb20908f683993182ee9c2a74', '102.185.38.165', 'okhttp/4.9.0', 1632449828, ''),
('33ee0f89507db0ec88793b8e8dc5e051', '102.185.38.165', 'okhttp/4.9.0', 1632448202, ''),
('34bc56baf8fe822f1a5a4ea13ca0d8a2', '102.185.38.165', 'okhttp/4.9.0', 1632442467, ''),
('34f1af4ed49e58d50d9abb44c9b385a6', '102.185.38.165', 'okhttp/4.9.0', 1632448272, ''),
('3847c38b9b658cd6e51a613be1ae7262', '102.185.38.165', 'okhttp/4.9.0', 1632448245, ''),
('3972993d36b211bec2c6bdd80b095a3a', '102.185.38.165', 'okhttp/4.9.0', 1632448136, ''),
('3a3690fd20c5a65ac6fdb5554d9b9081', '102.185.38.165', 'okhttp/4.9.0', 1632448240, ''),
('3a54a0548bb81f52b7b24bd8043da554', '102.185.38.165', 'okhttp/4.9.0', 1632448030, ''),
('3b20f78d80b1eb8bcd537554e352e767', '102.185.38.165', 'okhttp/4.9.0', 1632448100, ''),
('3be12ade0b02ec8319f07b5fb59927db', '102.185.38.165', 'okhttp/4.9.0', 1632448220, ''),
('3c8a4f89b9e208077525820217e9fab3', '102.185.38.165', 'okhttp/4.9.0', 1632448267, ''),
('3dfa16fda9a3e02bc6a4ec2b5c8d05ad', '102.185.38.165', 'okhttp/4.9.0', 1632448271, ''),
('3ffd7f5c8f5547e20b864b0322245076', '102.185.38.165', 'okhttp/4.9.0', 1632448144, ''),
('404b3c02eedc216b5f28bdc3d22cdc36', '102.185.38.165', 'okhttp/4.9.0', 1632448123, ''),
('41f6e2c681eb1af102a92121a9906286', '102.185.38.165', 'okhttp/4.9.0', 1632448220, ''),
('4200adb4543670365117d350b5995829', '102.185.38.165', 'okhttp/4.9.0', 1632448249, ''),
('43757aba326a0b0ba6a65ef1a8747d84', '102.185.38.165', 'okhttp/4.9.0', 1632448037, ''),
('46d22101d564def1236b0bd822fc05f7', '102.185.38.165', 'okhttp/4.9.0', 1632442415, ''),
('481eaf2057f2ba005f09cf6653ca999e', '102.185.38.165', 'okhttp/4.9.0', 1632448240, ''),
('48651e8f708605b6682f2274a143aba6', '102.185.38.165', 'okhttp/4.9.0', 1632448157, ''),
('499ce680c60412f8b81f9d1a16785126', '102.185.38.165', 'okhttp/4.9.0', 1632448190, ''),
('49f97867002f2159a06125a806a89853', '102.185.38.165', 'okhttp/4.9.0', 1632447932, ''),
('4a277c3cec13d38456a44a7b1c3a453e', '102.185.38.165', 'okhttp/4.9.0', 1632448272, ''),
('4b45af80e86fc736ff315ac6256aa8f8', '102.185.38.165', 'okhttp/4.9.0', 1632447980, ''),
('4b627293a04fe4e601e2c9f6796387e4', '102.185.38.165', 'okhttp/4.9.0', 1632447989, ''),
('4bea0949457cbd3807ad8f39be44db09', '102.185.38.165', 'okhttp/4.9.0', 1632449802, ''),
('4cfac90232252dc169f05388e017fdb7', '102.185.38.165', 'okhttp/4.9.0', 1632448249, ''),
('4db0000ae4d6dcf6ad58e05b7e47237d', '102.185.38.165', 'okhttp/4.9.0', 1632448057, ''),
('4db5e7564ed1f874dffb8d3a9295bf03', '102.185.38.165', 'okhttp/4.9.0', 1632449832, ''),
('4ed4bbca4d100edb7a7c65f7b9252f91', '102.185.38.165', 'okhttp/4.9.0', 1632447999, ''),
('51bcc67c2bf59b9c5ae68393b509c5fb', '102.185.38.165', 'okhttp/4.9.0', 1632448220, ''),
('526d667af5c173bff3bc4befb37a2d1c', '102.185.38.165', 'okhttp/4.9.0', 1632448225, ''),
('5287ac6e9a4bcb6642999f15bbe0c422', '102.185.38.165', 'PostmanRuntime/7.28.4', 1632443071, ''),
('542a0d2a67d979bd25c770eb8aaae569', '102.185.38.165', 'okhttp/4.9.0', 1632448185, ''),
('54a6a344ffa9eacc0c2f63c6c3708ed9', '102.185.38.165', 'okhttp/4.9.0', 1632448267, ''),
('54eaa97ab381c8ed7d9729693e1fb3a7', '102.185.38.165', 'okhttp/4.9.0', 1632448113, ''),
('55c41b77679dac1bf02d277eab585015', '102.185.38.165', 'okhttp/4.9.0', 1632448253, ''),
('567d3f68c8092b4cf65c4fb1ae121d9e', '102.185.38.165', 'okhttp/4.9.0', 1632448072, ''),
('5a7b7b969e96d93ced4a8b807f717c3e', '102.185.38.165', 'okhttp/4.9.0', 1632442447, ''),
('5aa40487fd9a3d293c577b361c3f1036', '102.185.38.165', 'okhttp/4.9.0', 1632448295, ''),
('5b2db119490736a1f983591d71cbcd27', '102.185.38.165', 'okhttp/4.9.0', 1632448120, ''),
('5cbcdf0294b35b248d07976e21d5277b', '102.185.38.165', 'okhttp/4.9.0', 1632448262, ''),
('608af7b869b02d4132982b2f690cecd5', '102.185.38.165', 'okhttp/4.9.0', 1632448245, ''),
('618b1cf5edea6017888d062be8165011', '102.185.38.165', 'okhttp/4.9.0', 1632448245, ''),
('6295c3f416a1a947d786cff2ad528299', '102.185.38.165', 'okhttp/4.9.0', 1632448043, ''),
('63e6d9fd0b415fd7da03825dad358443', '102.185.38.165', 'okhttp/4.9.0', 1632448305, ''),
('63fad5b6d6f756231c58c48df7cf06e6', '102.185.38.165', 'okhttp/4.9.0', 1632442415, ''),
('6513e541e185ad8d9491cd43c4b52bb8', '102.185.38.165', 'okhttp/4.9.0', 1632442447, ''),
('6550621c84905965922af03e32af86f7', '102.185.38.165', 'okhttp/4.9.0', 1632447997, ''),
('65662310e24f9ea55166f7262c672c0b', '102.185.38.165', 'okhttp/4.9.0', 1632448165, ''),
('65ed4f8b862dbc10accbb99c2f117129', '102.185.38.165', 'okhttp/4.9.0', 1632448096, ''),
('6671f33506b51546bd5e39b8ccc2542b', '102.185.38.165', 'okhttp/4.9.0', 1632449832, ''),
('677421f38c6861aa88f22c76f9740ff5', '102.185.38.165', 'okhttp/4.9.0', 1632442442, ''),
('67d0714b819aca1265ba4e271f31406f', '102.185.38.165', 'okhttp/4.9.0', 1632448190, ''),
('67eb08780c98945386818a364006a9e0', '102.185.38.165', 'okhttp/4.9.0', 1632447944, ''),
('69876a62e5bb2100d89aa3cc3dcd129d', '102.185.38.165', 'okhttp/4.9.0', 1632448287, ''),
('6bddec18f7f13f5466236cdf381f7704', '102.185.38.165', 'okhttp/4.9.0', 1632448223, ''),
('6ed065e76fdbe7aaaca9f9a4f8edda2e', '102.185.38.165', 'okhttp/4.9.0', 1632448272, ''),
('70a9bf69ff21095149b03b621dce9b22', '102.185.38.165', 'okhttp/4.9.0', 1632447930, ''),
('70fefe13e446b15566d300c221831272', '102.185.38.165', 'okhttp/4.9.0', 1632442415, ''),
('714da10b7d203795e46ef75a321ce385', '102.185.38.165', 'okhttp/4.9.0', 1632448168, ''),
('715e87db5034da01d84720f8d77d57ec', '102.185.38.165', 'okhttp/4.9.0', 1632449799, ''),
('7356f4f30a935e554232f6c37232f8bc', '102.185.38.165', 'okhttp/4.9.0', 1632448123, ''),
('746de0839f272981a05c963ee8adb108', '102.185.38.165', 'okhttp/4.9.0', 1632447982, ''),
('760e14e0b9f97f60e9ff4ee48a9ad80c', '102.185.38.165', 'okhttp/4.9.0', 1632448097, ''),
('7618fe665c0d8323c064b987f35a1bc6', '102.185.38.165', 'okhttp/4.9.0', 1632442456, ''),
('782ac6a24c3eb72436ddc88773c97c50', '102.185.38.165', 'okhttp/4.9.0', 1632448230, ''),
('7a72da00362e2f9c44776c04adec81ee', '102.185.38.165', 'okhttp/4.9.0', 1632447897, ''),
('7a81dbf0572b4497e69ee1304965638a', '102.185.38.165', 'okhttp/4.9.0', 1632447932, ''),
('7affa67ea2939e3e78b0fd428ae3ad89', '102.185.38.165', 'okhttp/4.9.0', 1632448105, ''),
('7c55a74499b47a280d8eaf75e1f92496', '102.185.38.165', 'okhttp/4.9.0', 1632449802, ''),
('7cc7177b7e3e8e5afbaa5e5c0f412ff1', '102.185.38.165', 'okhttp/4.9.0', 1632448171, ''),
('7dd9e87da1afc7a2cfd8c32de90082de', '102.185.38.165', 'okhttp/4.9.0', 1632448215, ''),
('7e5248813d536376cea87da54fbfb5d4', '102.185.38.165', 'okhttp/4.9.0', 1632448202, ''),
('7edaa55e0c76d7686b1c3c0065fed0bb', '102.185.38.165', 'okhttp/4.9.0', 1632448305, ''),
('807799d09f33ad47c5c2ef3e927706cc', '102.185.38.165', 'okhttp/4.9.0', 1632442429, ''),
('815fe15702247bb416b49d6acb1acee8', '102.185.38.165', 'okhttp/4.9.0', 1632448260, ''),
('8559d315e3a1a03fbaac88af287cfc82', '102.185.38.165', 'okhttp/4.9.0', 1632447989, ''),
('858f033531b2b30145be93eb9b86efa0', '102.185.38.165', 'okhttp/4.9.0', 1632447982, ''),
('8636a6ba190692b384464b978ff57612', '102.185.38.165', 'okhttp/4.9.0', 1632442429, ''),
('875824f1a82682dfbadfd590ebc93971', '102.185.38.165', 'okhttp/4.9.0', 1632448043, ''),
('876e259dfcf8defec15e8e1ae664104a', '102.185.38.165', 'okhttp/4.9.0', 1632448110, ''),
('87c2ff66adb3c5c6ec303d3251a34f71', '102.185.38.165', 'okhttp/4.9.0', 1632442415, ''),
('89437fdfaa203425996febedc2c4dd76', '102.185.38.165', 'okhttp/4.9.0', 1632449831, ''),
('8955e98e275a308b7a2bb8bf6f83803d', '102.185.38.165', 'okhttp/4.9.0', 1632448105, ''),
('8959e9193a69fa2aedb06e13a8ac81ce', '102.185.38.165', 'okhttp/4.9.0', 1632448043, ''),
('8bac7fd8a3fb80a54aacf1677f119bcc', '102.185.38.165', 'okhttp/4.9.0', 1632447904, ''),
('8c5ce25a235a031ba16153f77f257c63', '102.185.38.165', 'okhttp/4.9.0', 1632448202, ''),
('8c5f9f73b5811c5a36af8a24e6ae139e', '102.185.38.165', 'okhttp/4.9.0', 1632449846, ''),
('8d45c6387f1b57839cfee23622a6365e', '102.185.38.165', 'okhttp/4.9.0', 1632448271, ''),
('8ff78176f5accae2377708c3bd35a151', '102.185.38.165', 'okhttp/4.9.0', 1632448225, ''),
('9066005fdc04d91c6e5bbe570d111acf', '102.185.38.165', 'okhttp/4.9.0', 1632447944, ''),
('907529e75d5fb3629f48b60426beec08', '102.185.38.165', 'okhttp/4.9.0', 1632448096, ''),
('93c1a71d97a2b8e6b576f3645fd4c57a', '102.185.38.165', 'okhttp/4.9.0', 1632442447, ''),
('979e07c0e13ee92a50ab4e87d7c42076', '102.185.38.165', 'okhttp/4.9.0', 1632448096, ''),
('994fc835aca0f4b8ed8b513103b31223', '102.185.38.165', 'okhttp/4.9.0', 1632448085, ''),
('9b10600fceab7dbf04dd9ea1b670fd81', '102.185.38.165', 'okhttp/4.9.0', 1632448120, ''),
('9b2b5ba3d4f32d4997010c447d44406e', '102.185.38.165', 'okhttp/4.9.0', 1632448145, ''),
('9dec01f50f26c0ec41c81b98387a7545', '102.185.38.165', 'okhttp/4.9.0', 1632448249, ''),
('a0c4d09c144117fa7284d4d92a09091c', '102.185.38.165', 'okhttp/4.9.0', 1632449846, ''),
('a116ce7c961b60ff3312afa640600128', '102.185.38.165', 'okhttp/4.9.0', 1632448253, ''),
('a2c682f0eb26ff687a2006593e555520', '102.185.38.165', 'okhttp/4.9.0', 1632448072, ''),
('a7ebcc1683d34a7e2bb73265c6023524', '102.185.38.165', 'okhttp/4.9.0', 1632448157, ''),
('a7fe60610edc527cd056b9c47f387096', '102.185.38.165', 'okhttp/4.9.0', 1632447903, ''),
('aa78f7e58cebe1729d0c7226ba48ec62', '102.185.38.165', 'okhttp/4.9.0', 1632448000, ''),
('ab6a2a71eb7fc4b6a01b425a1ab31a8b', '102.185.38.165', 'okhttp/4.9.0', 1632448085, ''),
('ac6ef2bad4b0379583fd10a4ff0629fc', '102.185.38.165', 'okhttp/4.9.0', 1632448188, ''),
('aca7689475553c63a07aea287bb6258f', '102.185.38.165', 'okhttp/4.9.0', 1632448072, ''),
('acb3fd9186d27ce06c6462028bc9496a', '102.185.38.165', 'okhttp/4.9.0', 1632449846, ''),
('ad6fb533c3317a965044ae14ac97f61d', '102.185.38.165', 'okhttp/4.9.0', 1632447944, ''),
('aec64390b9086f59757788f972fbe133', '102.185.38.165', 'okhttp/4.9.0', 1632449846, ''),
('aed865908d86f520c57482ec9cc13c37', '102.185.38.165', 'okhttp/4.9.0', 1632449802, ''),
('b19000f2f0514c7eced58c46b9836953', '102.185.38.165', 'okhttp/4.9.0', 1632448240, ''),
('b30261ccfc0722760c1c7f0d68421fe2', '102.185.38.165', 'okhttp/4.9.0', 1632448220, ''),
('b48d712e012aef3cafeb76c84c1331b5', '102.185.38.165', 'okhttp/4.9.0', 1632442442, ''),
('b54c01e6adabc391f1e0a7cccdc972f9', '102.185.38.165', 'okhttp/4.9.0', 1632448294, ''),
('b7e5510269c41547109fce5d4bfe2491', '102.185.38.165', 'okhttp/4.9.0', 1632448000, ''),
('b99e117009fbe049f6fe15a65f702408', '102.185.38.165', 'okhttp/4.9.0', 1632448088, ''),
('bab0cf2dc36565177f25418a3ac60ac1', '102.185.38.165', 'okhttp/4.9.0', 1632447932, ''),
('bb8580cdc39d388fac7cd3b40f407184', '102.185.38.165', 'okhttp/4.9.0', 1632447944, ''),
('bba941deb48f0b2d9692e3f4405ca1fa', '102.185.38.165', 'okhttp/4.9.0', 1632448088, ''),
('bd17316e06cbf143c4b12ad267ee784a', '102.185.38.165', 'okhttp/4.9.0', 1632448123, ''),
('be35a9655630e1cf94346e1f6f4e8f7f', '102.185.38.165', 'okhttp/4.9.0', 1632447982, ''),
('c0b9bc439eedc6130c9d4c03f1c06750', '102.185.38.165', 'okhttp/4.9.0', 1632448215, ''),
('c426857694ce1c8606833015f19e1764', '102.185.38.165', 'okhttp/4.9.0', 1632447999, ''),
('c4de5d2df6ab518febebc2eeaecf1602', '102.185.38.165', 'okhttp/4.9.0', 1632448190, ''),
('c611dae260d89a1f041ad6a432505f52', '102.185.38.165', 'okhttp/4.9.0', 1632448272, ''),
('c9a68624f980bfa6ec3eca538d6e651e', '102.185.38.165', 'okhttp/4.9.0', 1632447928, ''),
('cb86f9b6c889e7a0e57d489e5ebd9d6a', '102.185.38.165', 'okhttp/4.9.0', 1632448194, ''),
('cc3d7813a3d77c7d8b34d53364244447', '102.185.38.165', 'okhttp/4.9.0', 1632448272, ''),
('cc82552742865c3f59dc63b0af553951', '102.185.38.165', 'okhttp/4.9.0', 1632447932, ''),
('cd7faed2be8b60bbf6e60d96e6358c02', '102.185.38.165', 'okhttp/4.9.0', 1632448030, ''),
('cfd83e2b642d749ad8dfeeaa76dc36c3', '102.185.38.165', 'okhttp/4.9.0', 1632447997, ''),
('d1497eabe27d4002382246921428a2a0', '102.185.38.165', 'okhttp/4.9.0', 1632448043, ''),
('d35c647532fd72b73bd4730c6a701660', '102.185.38.165', 'okhttp/4.9.0', 1632448188, ''),
('d7b6db2b762032270bedeeec6fa6adaf', '102.185.38.165', 'okhttp/4.9.0', 1632448249, ''),
('d89948f7480b76a4637617bae5bbd9cb', '102.185.38.165', 'okhttp/4.9.0', 1632448072, ''),
('da0ad1ab7b59c0b99eef154ad2e515ef', '102.185.38.165', 'okhttp/4.9.0', 1632448194, ''),
('dbbe88cd8595aa3ff6a1eb1cf0d31e12', '102.185.38.165', 'okhttp/4.9.0', 1632449832, ''),
('dd6ff1f6d1c840c0f79d83ca0b7ef269', '102.185.38.165', 'okhttp/4.9.0', 1632448000, ''),
('de7bc9381671804dcab52f7e0bf80bf0', '102.185.38.165', 'okhttp/4.9.0', 1632448240, ''),
('e033ad58b8d99c64c6cb3f82ab3f1812', '102.185.38.165', 'okhttp/4.9.0', 1632448269, ''),
('e4487240ef0c951f895f1e00fedfd1e9', '102.185.38.165', 'okhttp/4.9.0', 1632448027, ''),
('e4a6a9b3fa3d2b86b195dcabc5636acf', '102.185.38.165', 'okhttp/4.9.0', 1632442447, ''),
('e4c9262ef5f510ae6b313c275fbb68fc', '102.185.38.165', 'okhttp/4.9.0', 1632442447, ''),
('e7384d86c010184e4e20a77934378bd8', '102.185.38.165', 'okhttp/4.9.0', 1632447893, ''),
('e7ed522e985959d1c97183a07028891d', '102.185.38.165', 'okhttp/4.9.0', 1632448295, ''),
('e9b2e05e5ce3228592ae2899d871cc01', '102.185.38.165', 'okhttp/4.9.0', 1632448215, ''),
('ee4c6028d8a763cda3ccc3395bc6438d', '102.185.38.165', 'okhttp/4.9.0', 1632448037, ''),
('ef87629c8187e1e64ebfe8a0b04ff413', '102.185.38.165', 'okhttp/4.9.0', 1632448144, ''),
('f143e916ca8cf5ce1f20e8437cd1f53b', '102.185.38.165', 'okhttp/4.9.0', 1632448113, ''),
('f161aedb3d47ab9554dfaf067dde758d', '102.185.38.165', 'okhttp/4.9.0', 1632447939, ''),
('f2a3168d5b54d3ef3bec26a7f4cd989a', '102.185.38.165', 'okhttp/4.9.0', 1632448253, ''),
('f6be84fabd79abfa7b47afb484d6f12b', '102.185.38.165', 'okhttp/4.9.0', 1632448220, ''),
('f96a218c223fae50cf30692a0740ab26', '102.185.38.165', 'okhttp/4.9.0', 1632448269, ''),
('f99ac2d13f743a0b8003ef61d8c97c3d', '102.185.38.165', 'okhttp/4.9.0', 1632449802, ''),
('fd7180e30788092af1bc543d37332227', '102.185.38.165', 'okhttp/4.9.0', 1632447977, ''),
('fe6c36fe6d1e2f582b04333e3bc7c868', '102.185.38.165', 'okhttp/4.9.0', 1632448171, '');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `site_title` text NOT NULL,
  `application_fees` float DEFAULT NULL,
  `shipping_kilo_cost` float NOT NULL DEFAULT '0',
  `site_desc` text NOT NULL,
  `site_key` text NOT NULL,
  `site_logo` text NOT NULL,
  `site_icon` text NOT NULL,
  `site_lang` varchar(10) NOT NULL,
  `site_phone` text NOT NULL,
  `site_fax` varchar(100) DEFAULT NULL,
  `site_email` text NOT NULL,
  `site_dutytime` text,
  `site_title_en` varchar(100) DEFAULT NULL,
  `site_dutytime_en` text,
  `site_facebok` text NOT NULL,
  `site_twitter` text NOT NULL,
  `site_youtube` text NOT NULL,
  `site_linkedin` text NOT NULL,
  `site_instagram` text NOT NULL,
  `css` text NOT NULL,
  `js` text NOT NULL,
  `copy_right` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `site_title`, `application_fees`, `shipping_kilo_cost`, `site_desc`, `site_key`, `site_logo`, `site_icon`, `site_lang`, `site_phone`, `site_fax`, `site_email`, `site_dutytime`, `site_title_en`, `site_dutytime_en`, `site_facebok`, `site_twitter`, `site_youtube`, `site_linkedin`, `site_instagram`, `css`, `js`, `copy_right`) VALUES
(1, 'FullPay', 5, 5, 'FullPay', 'FullPay', 'deb555bce78f27154799f5c61c68909d.png', 'bac0a0f7251a26fcae41446a4c4baad9.png', 'ar', '0123456789', '01234567891', 'info@FullPay.com', 'الإثنين - الخميس: 08:00 - 18:00                                 \r\n<br>الجمعة - السبت : 08:00 - 12:30                                 \r\n<br>الأحد - مغلق', 'FullPay', 'Monday- Thursday: 08:00 - 18:00                                 \r\n<br> Saturday: 08:00 - 12:30                                 \r\n<br>Friday- Closed', 'https://www.facebook.com/FullPay/', 'https://www.twitter.com/FullPay/', 'https://www.youtube.com/FullPay/', 'https://www.linkedin.com/FullPay/', 'https://www.instgram.com/FullPay/', '', '', 'جميع الحقوق محفوظه FullPay');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(11) NOT NULL,
  `slide_title_ar` varchar(100) NOT NULL,
  `slide_title_en` varchar(100) NOT NULL,
  `slide_subtitle_ar` text NOT NULL,
  `slide_subtitle_en` text NOT NULL,
  `img` varchar(100) NOT NULL,
  `state` varchar(20) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `slide_title_ar`, `slide_title_en`, `slide_subtitle_ar`, `slide_subtitle_en`, `img`, `state`, `is_active`, `is_deleted`) VALUES
(1, 'شريحة 1', 'slide 1', 'نص مبدئي 1', 'Sample Text 1', 'https://demo.f4h.com.sa/fullpay/images/home/slides/3.jpg', '1', 1, 0),
(2, 'شريحة 2', 'slide 2', 'نص مبدئي 2', 'Sample Text  2', 'https://demo.f4h.com.sa/fullpay/images/home/slides/1.jpg', '0', 0, 1),
(3, 'شريحة 3', 'slide 3', 'نص مبدئي  3', 'Sample Text  3', 'https://demo.f4h.com.sa/fullpay/images/home/slides/2.jpg', '0', 0, 1),
(9, '66', '', '', '', 'http://localhost/fullpay/images/home/slides/9e47ec074db8e667f6da055a314d2cf1.GIF', '0', 0, 1),
(10, 'صورة', '', '', '', 'http://demo.f4h.com.sa/fullpay/images/home/slides/ca01501594f3f56f43aea7f1b2ece720.jpg', '', 1, 0),
(11, '...', '', '', '', 'http://demo.f4h.com.sa/fullpay/images/home/slides/bc3aabe32fb1fdafa10c47eb0391487f.png', '0', 0, 1),
(12, 'صورة22', '', '', '', 'http://demo.f4h.com.sa/fullpay/images/home/slides/d76eb35ec9b1c96e2e62e86f778c6a1b.jpg', '', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `social`
--

CREATE TABLE `social` (
  `id` int(11) NOT NULL,
  `facebook` text NOT NULL,
  `twitter` text NOT NULL,
  `youtube` text NOT NULL,
  `instagram` text NOT NULL,
  `linkedin` text NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `social`
--

INSERT INTO `social` (`id`, `facebook`, `twitter`, `youtube`, `instagram`, `linkedin`, `store_id`) VALUES
(1, 'Facebook', 'Twitter', 'Youtube', 'Instagram', 'Linkedin', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tax`
--

CREATE TABLE `tax` (
  `id` int(11) NOT NULL,
  `name` text,
  `value` float DEFAULT NULL,
  `comment` text,
  `is_active` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax`
--

INSERT INTO `tax` (`id`, `name`, `value`, `comment`, `is_active`) VALUES
(1, 'قيمة مضافة', 0.5, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `time_open_close`
--

CREATE TABLE `time_open_close` (
  `id` int(11) NOT NULL,
  `order_company_id` int(11) DEFAULT NULL,
  `saturday_open` time DEFAULT NULL,
  `sunday_open` time DEFAULT NULL,
  `monday_open` time DEFAULT NULL,
  `tuesday_open` time DEFAULT NULL,
  `wednesday_open` time DEFAULT NULL,
  `thursday_open` time DEFAULT NULL,
  `friday_open` time DEFAULT NULL,
  `saturday_close` time DEFAULT NULL,
  `sunday_close` time DEFAULT NULL,
  `monday_close` time DEFAULT NULL,
  `tuesday_close` time DEFAULT NULL,
  `wednesday_close` time DEFAULT NULL,
  `thursday_close` time DEFAULT NULL,
  `friday_close` time DEFAULT NULL,
  `is_active` int(3) NOT NULL DEFAULT '1',
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `time_open_close`
--

INSERT INTO `time_open_close` (`id`, `order_company_id`, `saturday_open`, `sunday_open`, `monday_open`, `tuesday_open`, `wednesday_open`, `thursday_open`, `friday_open`, `saturday_close`, `sunday_close`, `monday_close`, `tuesday_close`, `wednesday_close`, `thursday_close`, `friday_close`, `is_active`, `date_add`) VALUES
(1, 1, '00:00:00', NULL, '00:00:00', NULL, '00:00:00', '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-09-08 08:55:18'),
(2, 2, '00:00:00', NULL, '00:00:00', NULL, '00:00:00', '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-09-08 08:57:07'),
(3, 1, '00:00:00', NULL, '00:00:00', NULL, '00:00:00', '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-09-08 08:58:43'),
(4, 2, '00:00:00', NULL, '00:00:00', NULL, '00:00:00', '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-02-05 09:12:44'),
(5, 1, '00:00:00', NULL, '00:00:00', NULL, '00:00:00', '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-02-28 18:12:29'),
(6, 3, '00:00:00', NULL, '00:00:00', NULL, '00:00:00', '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-04-05 18:49:45');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(350) NOT NULL,
  `phone` varchar(125) NOT NULL,
  `email` varchar(255) NOT NULL,
  `user_hash` text NOT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `user_group` int(11) NOT NULL,
  `forgot_password` int(11) NOT NULL,
  `created_date` text NOT NULL,
  `is_active` varchar(3) NOT NULL COMMENT 'yes or no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `phone`, `email`, `user_hash`, `branch_id`, `user_group`, `forgot_password`, `created_date`, `is_active`) VALUES
(9, 'فانتاستك', '01001647527', 'f4h@f4h.com', '8cb2237d0679ca88db6464eac60da96345513964', 3, 3, 129972, '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `id` int(11) NOT NULL,
  `title` varchar(350) NOT NULL,
  `type` int(11) NOT NULL,
  `is_user` int(11) NOT NULL,
  `is_user_view` int(11) NOT NULL,
  `is_user_add` int(11) NOT NULL,
  `is_user_edit` int(11) NOT NULL,
  `is_user_delete` int(11) NOT NULL,
  `is_client` int(11) NOT NULL,
  `is_client_view` int(11) NOT NULL,
  `is_client_add` int(11) NOT NULL,
  `is_client_edit` int(11) NOT NULL,
  `is_client_delete` int(11) NOT NULL,
  `is_page` int(11) NOT NULL,
  `is_page_view` int(11) NOT NULL,
  `is_page_add` int(11) NOT NULL,
  `is_page_edit` int(11) NOT NULL,
  `is_page_delete` int(11) NOT NULL,
  `is_category` int(11) NOT NULL,
  `is_category_view` int(11) NOT NULL,
  `is_category_add` int(11) NOT NULL,
  `is_category_edit` int(11) NOT NULL,
  `is_category_delete` int(11) NOT NULL,
  `is_shop` int(11) NOT NULL,
  `is_shop_view` int(11) NOT NULL,
  `is_shop_add` int(11) NOT NULL,
  `is_shop_edit` int(11) NOT NULL,
  `is_shop_delete` int(11) NOT NULL,
  `is_product` int(11) NOT NULL,
  `is_product_view` int(11) NOT NULL,
  `is_product_add` int(11) NOT NULL,
  `is_product_edit` int(11) NOT NULL,
  `is_product_delete` int(11) NOT NULL,
  `is_order` int(11) NOT NULL,
  `is_order_view` int(11) NOT NULL,
  `is_order_edit` int(11) NOT NULL,
  `is_order_delete` int(11) NOT NULL,
  `is_report` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`id`, `title`, `type`, `is_user`, `is_user_view`, `is_user_add`, `is_user_edit`, `is_user_delete`, `is_client`, `is_client_view`, `is_client_add`, `is_client_edit`, `is_client_delete`, `is_page`, `is_page_view`, `is_page_add`, `is_page_edit`, `is_page_delete`, `is_category`, `is_category_view`, `is_category_add`, `is_category_edit`, `is_category_delete`, `is_shop`, `is_shop_view`, `is_shop_add`, `is_shop_edit`, `is_shop_delete`, `is_product`, `is_product_view`, `is_product_add`, `is_product_edit`, `is_product_delete`, `is_order`, `is_order_view`, `is_order_edit`, `is_order_delete`, `is_report`) VALUES
(2, 'مدير فرع', 2, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(1, 'موظف مبيعات', 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(3, 'مدير رئيسي', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_categories`
--
ALTER TABLE `activity_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_subcategories`
--
ALTER TABLE `activity_subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `captains`
--
ALTER TABLE `captains`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_address`
--
ALTER TABLE `client_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_payment_cards`
--
ALTER TABLE `client_payment_cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer`
--
ALTER TABLE `offer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer_groups`
--
ALTER TABLE `offer_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer_types`
--
ALTER TABLE `offer_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordertime_open_close`
--
ALTER TABLE `ordertime_open_close`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_company`
--
ALTER TABLE `order_company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_categories`
--
ALTER TABLE `products_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_options`
--
ALTER TABLE `product_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prod_opti_categ`
--
ALTER TABLE `prod_opti_categ`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prod_opti_subcat`
--
ALTER TABLE `prod_opti_subcat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt`
--
ALTER TABLE `receipt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_details`
--
ALTER TABLE `receipt_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_state`
--
ALTER TABLE `receipt_state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services_deps`
--
ALTER TABLE `services_deps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social`
--
ALTER TABLE `social`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax`
--
ALTER TABLE `tax`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_open_close`
--
ALTER TABLE `time_open_close`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_categories`
--
ALTER TABLE `activity_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `activity_subcategories`
--
ALTER TABLE `activity_subcategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `captains`
--
ALTER TABLE `captains`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `client_address`
--
ALTER TABLE `client_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `client_payment_cards`
--
ALTER TABLE `client_payment_cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `offer`
--
ALTER TABLE `offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `offer_groups`
--
ALTER TABLE `offer_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `offer_types`
--
ALTER TABLE `offer_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ordertime_open_close`
--
ALTER TABLE `ordertime_open_close`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `order_company`
--
ALTER TABLE `order_company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `products_categories`
--
ALTER TABLE `products_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product_options`
--
ALTER TABLE `product_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `prod_opti_categ`
--
ALTER TABLE `prod_opti_categ`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `prod_opti_subcat`
--
ALTER TABLE `prod_opti_subcat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `rate`
--
ALTER TABLE `rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `receipt`
--
ALTER TABLE `receipt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- AUTO_INCREMENT for table `receipt_details`
--
ALTER TABLE `receipt_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `receipt_state`
--
ALTER TABLE `receipt_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `services_deps`
--
ALTER TABLE `services_deps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `social`
--
ALTER TABLE `social`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tax`
--
ALTER TABLE `tax`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `time_open_close`
--
ALTER TABLE `time_open_close`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
